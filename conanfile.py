from conan import ConanFile

class SGonPPMACRecipe(ConanFile):
    name = "sgonppmac"
    executable = "ds_SGonPPMAC"
    version = "1.0.6"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Falilou Thiam, Alain Buteau"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/motion/sgonppmac.git"
    description = "None"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        self.requires("sgonpmaclib/[>=1.0]@soleil/stable")
        self.requires("ssh2/1.4.3@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")

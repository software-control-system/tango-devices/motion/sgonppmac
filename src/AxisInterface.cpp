//=============================================================================
// AxisInterface.cpp
//=============================================================================
// class.............Axis Interface (Base classs for all Axis type)
// original author...F. THIAM
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <sstream>
#include "AxisInterface.h"
#include "BoxInitDetector.h"

// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================


namespace sgonppmac_ns {

	//- check controller cmd macro:
#define CHECK_CTRL_CMD() \
	do \
	{ \
	if (!m_cmdInterface || sgonppmac_ns::BoxInitDetector::instance()->is_init_needed(this)) \
	THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
	_CPTC("request aborted - the command controller isn't accessible "), \
  _CPTC("AxisInterface::check_ctrl_cmd")); \
	} while (0)

	//- check controller std macro:
#define CHECK_CTRL_STD() \
	do \
	{ \
	if (!m_stdDataInterface || sgonppmac_ns::BoxInitDetector::instance()->is_init_needed(this)) \
	THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
	_CPTC("request aborted - the standard controller isn't accessible "), \
  _CPTC("AxisInterface::check_ctrl_std")); \
	} while (0)

	//- check controller ctl macro:
#define CHECK_CTRL_CTL() \
	do \
	{ \
	if (!m_ctlDataInterface || sgonppmac_ns::BoxInitDetector::instance()->is_init_needed(this)) \
	THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
	_CPTC("request aborted - the critical controller isn't accessible "), \
  _CPTC("AxisInterface::check_ctrl_ctl")); \
	} while (0)
  
// ============================================================================

// ============================================================================
// AxisInterface::AxisInterface
// ============================================================================
AxisInterface::AxisInterface(Tango::DeviceImpl * hostDevice)
: yat4tango::TangoLogAdapter(hostDevice)
{
  m_hostDevice = hostDevice;
	m_cmdInterface = NULL;
	m_stdDataInterface = NULL;
	m_ctlDataInterface = NULL;
	m_cs_id = 0;
  m_isWritePosInProgress = false;
}

// ============================================================================
// AxisInterface::~AxisInterface
// ============================================================================
AxisInterface::~AxisInterface ()
{
	m_cmdInterface = NULL;
	m_stdDataInterface = NULL;
	m_ctlDataInterface = NULL;
	m_cs_id = 0;
  m_isWritePosInProgress = false;
}

// ============================================================================
// AxisInterface::init_controler_interface
// ============================================================================
void AxisInterface::init_controler_interface()
{
  sgonppmac_ns::BoxInitDetector::instance()->add_listener(this);

	// Get the command controler interface
	//--------------------------------------------
	try
	{
		m_cmdInterface = sgonppmac_ns::ControllerFactory::instance()->get_cmd_interface();
	}
	catch (...)
	{
    //std::cout<<"AxisInterface::init_controler_interface() :		get_cmd_interface exception"<<std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get command controler interface - Caught DevFailed!"), 
							_CPTC("AxisInterface::init_controler_interface")); 
	}

	// check the m_cmdInterface
	if (!m_cmdInterface)
	{
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("The command controler interface is not created - Caught DevFailed!"), 
							_CPTC("AxisInterface::init_controler_interface")); 
	}

	// Get the standard controler interface
	//--------------------------------------------
	try
	{
		m_stdDataInterface = sgonppmac_ns::ControllerFactory::instance()->get_std_data_interface();
	}
	catch (...)
	{
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get standard data controler interface - Caught DevFailed!"), 
							_CPTC("AxisInterface::init_controler_interface")); 
	}

	// check the m_stdDataInterface
	if (!m_stdDataInterface)
	{
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("The standard data controler interface is not created - Caught DevFailed!"), 
							_CPTC("AxisInterface::init_controler_interface")); 
	}
	
	// Get the critical controler interface
	//--------------------------------------------
	try
	{
		m_ctlDataInterface = sgonppmac_ns::ControllerFactory::instance()->get_ctl_data_interface(); 
	}
	catch (...)
	{
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get critical data controler interface - Caught DevFailed!"), 
							_CPTC("AxisInterface::init_controler_interface")); 
	}

	// check the m_ctlDataInterface
	if (!m_ctlDataInterface)
	{
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("The critical data controler interface is not created - Caught DevFailed!"), 
							_CPTC("AxisInterface::init_controler_interface")); 
	}
}

// ============================================================================
// AxisInterface::declare_axis
// ============================================================================
void AxisInterface::declare_axis( std::vector<sgonpmaclib::T_axis_id> axis_list)
{
	CHECK_CTRL_CMD();
	CHECK_CTRL_STD();
	CHECK_CTRL_CTL();

	try
  {
		// Declare the axis in the Command ControlerInterface and send message init_axis to hardware
    this->m_cmdInterface->declare_axis(axis_list);
    INFO_STREAM << "Axis properly declared in Command Controler Interface" << endl;
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::declare_axis Axis declaration NOK in Command ControlerInterface: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to declare axis in Command ControlerInterface - Caught DevFailed!"), 
                     _CPTC("AxisInterface::declare_axis"));
  }
  catch(...)
  {
		ERROR_STREAM << "AxisInterface::declare_axis Axis declaration NOK in Command ControlerInterface" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to declare axis in Command ControlerInterface - Caught unknown exception!"), 
							_CPTC("AxisInterface::declare_axis")); 
  }

	try
  {
		// Declare the axis in the Standard ControlerInterface and send message init_axis to hardware
    this->m_stdDataInterface->declare_axis(axis_list);
    INFO_STREAM << "Axis properly declared in Standard Controler Interface" << endl;
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::declare_axis Axis declaration NOK in Standard ControlerInterface: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to declare axis in Standard ControlerInterface - Caught DevFailed!"), 
                     _CPTC("AxisInterface::declare_axis"));
  }
  catch(...)
  {
		ERROR_STREAM << "AxisInterface::declare_axis Axis declaration NOK in Standard ControlerInterface" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to declare axis in Standard ControlerInterface - Caught unknown exception!"), 
							_CPTC("AxisInterface::declare_axis")); 
  }

	try
  {
		// Declare the axis in the Critical ControlerInterface and send message init_axis to hardware
    this->m_ctlDataInterface->declare_axis(axis_list);
    INFO_STREAM << "Axis properly declared in Critical Controler Interface" << endl;
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::declare_axis Axis declaration NOK in Critical ControlerInterface: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to declare axis in Critical ControlerInterface - Caught DevFailed!"), 
                     _CPTC("AxisInterface::declare_axis"));
  }
  catch(...)
  {
		ERROR_STREAM << "AxisInterface::declare_axis Axis declaration NOK in Critical ControlerInterface" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to declare axis in Critical ControlerInterface - Caught unknown exception!"), 
							_CPTC("AxisInterface::declare_axis")); 
  }
}

// ============================================================================
// AxisInterface::undeclare_axis
// ============================================================================
void AxisInterface::undeclare_axis(std::vector<sgonpmaclib::T_axis_id> axis_list)
{
	CHECK_CTRL_CMD();
	CHECK_CTRL_STD();
	CHECK_CTRL_CTL();

	try
  {
		// Declare the axis in the Command ControlerInterface and send message undeclare_axis to hardware
    this->m_cmdInterface->undeclare_axis(axis_list);
    INFO_STREAM << "Axis properly undeclared in Command Controler Interface" << endl;
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::undeclare_axis Axis declaration NOK in Command ControlerInterface: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to undeclare axis in Command ControlerInterface - Caught DevFailed!"), 
                     _CPTC("SimpleAxis::undeclare_axis"));
  }
  catch(...)
  {
		ERROR_STREAM << "AxisInterface::undeclare_axis Axis declaration NOK in Command ControlerInterface" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to undeclare axis in Command ControlerInterface - Caught unknown exception!"), 
							_CPTC("SimpleAxis::undeclare_axis")); 
  }

	try
  {
		// Declare the axis in the Standard ControlerInterface and send message undeclare_axis to hardware
    this->m_stdDataInterface->undeclare_axis(axis_list);
    INFO_STREAM << "Axis properly undeclared in Standard Controler Interface" << endl;
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::undeclare_axis Axis declaration NOK in Standard ControlerInterface: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to undeclare axis in Standard ControlerInterface - Caught DevFailed!"), 
                     _CPTC("SimpleAxis::undeclare_axis"));
  }
  catch(...)
  {
		ERROR_STREAM << "AxisInterface::undeclare_axis Axis declaration NOK in Standard ControlerInterface" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to undeclare axis in Standard ControlerInterface - Caught unknown exception!"), 
							_CPTC("SimpleAxis::undeclare_axis")); 
  }

	try
  {
		// Declare the axis in the Critical ControlerInterface and send message undeclare_axis to hardware
    this->m_ctlDataInterface->undeclare_axis(axis_list);
    INFO_STREAM << "Axis properly undeclared in Critical Controler Interface" << endl;
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::undeclare_axis Axis declaration NOK in Critical ControlerInterface: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to undeclare axis in Critical ControlerInterface - Caught DevFailed!"), 
                     _CPTC("SimpleAxis::undeclare_axis"));
  }
  catch(...)
  {
		ERROR_STREAM << "AxisInterface::undeclare_axis Axis declaration NOK in Critical ControlerInterface" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to undeclare axis in Critical ControlerInterface - Caught unknown exception!"), 
							_CPTC("SimpleAxis::undeclare_axis")); 
  }
}

// ============================================================================
// AxisInterface::motor_omega_off
// ============================================================================
void AxisInterface::motor_omega_off()
{
	CHECK_CTRL_CMD();

#if defined (_SIMULATION_)
	CHECK_CTRL_CTL();
#endif

  try
  {
		this->m_cmdInterface->motor_omega_off(m_cs_id);

#if defined (_SIMULATION_)
		this->m_ctlDataInterface->motor_omega_off(m_cs_id);
#endif
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::motor_omega_off caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to motor on - Caught DevFailed!"), 
                     _CPTC("AxisInterface::motor_omega_off"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to motor omega off" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to motor on - Caught unknown exception!"), 
							_CPTC("AxisInterface::motor_omega_off")); 
  }
}

// ============================================================================
// AxisInterface::motor_other_off
// ============================================================================
void AxisInterface::motor_other_off()
{
	CHECK_CTRL_CMD();

#if defined (_SIMULATION_)
	CHECK_CTRL_CTL();
#endif

  try
  {
		this->m_cmdInterface->motor_other_off(m_cs_id);

#if defined (_SIMULATION_)
		this->m_ctlDataInterface->motor_other_off(m_cs_id);
#endif
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::motor_other_off caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to motor on - Caught DevFailed!"), 
                     _CPTC("AxisInterface::motor_other_off"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to motor omega off" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to motor on - Caught unknown exception!"), 
							_CPTC("AxisInterface::motor_other_off")); 
  }
}

// ============================================================================
// AxisInterface::motor_on
// ============================================================================
void AxisInterface::motor_on()
{
	CHECK_CTRL_CMD();

#if defined (_SIMULATION_)
	CHECK_CTRL_CTL();
#endif

  try
  {
		this->m_cmdInterface->motor_on(m_cs_id);

#if defined (_SIMULATION_)
		this->m_ctlDataInterface->motor_on(m_cs_id);
#endif
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::motor_on caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to motor on - Caught DevFailed!"), 
                     _CPTC("AxisInterface::motor_on"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to motor on" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to motor on - Caught unknown exception!"), 
							_CPTC("AxisInterface::motor_on")); 
  }
}

// ============================================================================
// AxisInterface::motor_off
// ============================================================================
void AxisInterface::motor_off()
{
	if (this->get_state() == Tango::MOVING)
	{
		THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
			_CPTC("Motor OFF is forbidden when the state is MOVING !"), 
			_CPTC("AxisInterface::motor_off")); 
	}

	CHECK_CTRL_CMD();

#if defined (_SIMULATION_)
	CHECK_CTRL_CTL();
#endif

  try
  {
		this->m_cmdInterface->motor_off(m_cs_id);

#if defined (_SIMULATION_)
		this->m_ctlDataInterface->motor_off(m_cs_id);
#endif
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::motor_off caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to motor off - Caught DevFailed!"), 
                     _CPTC("AxisInterface::motor_off"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to motor off" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to motor off - Caught unknown exception!"), 
							_CPTC("AxisInterface::motor_off")); 
  }
}

// ============================================================================
// AxisInterface::axis_stop
// ============================================================================
void AxisInterface::axis_stop()
{
	CHECK_CTRL_CMD();

#if defined (_SIMULATION_)
	CHECK_CTRL_CTL();
#endif

  // in any case, set "move in progress" flag to false
  m_isWritePosInProgress = false;

  try
  {
		this->m_cmdInterface->axis_stop(m_cs_id);

#if defined (_SIMULATION_)
		this->m_ctlDataInterface->axis_stop(m_cs_id);
#endif
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::axis_stop caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to axis stop - Caught DevFailed!"), 
                     _CPTC("AxisInterface::axis_stop"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to axis stop" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to axis stop - Caught unknown exception!"), 
							_CPTC("AxisInterface::axis_stop")); 
  }
}

// ============================================================================
// AxisInterface::disable
// ============================================================================
void AxisInterface::disable()
{
 // not effective for smargon
}

// ============================================================================
// AxisInterface::get_status_details
// ============================================================================
sgonpmaclib::T_cs_status AxisInterface::get_status_details()
{
	CHECK_CTRL_STD();

	sgonpmaclib::T_cs_status status;
  try
  {
		status = this->m_ctlDataInterface->get_status_details(m_cs_id);
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::get_status_details caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get status details - Caught DevFailed!"), 
                     _CPTC("AxisInterface::get_status_details"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to disable axis" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get status details - Caught unknown exception!"), 
							_CPTC("AxisInterface::get_status_details")); 
  }

	return status;
}

// ============================================================================
// AxisInterface::get_motors_status
// ============================================================================
std::string AxisInterface::get_motors_status()
{
	CHECK_CTRL_STD();

	std::string status;
  try
  {
		status = this->m_ctlDataInterface->get_motors_status(m_cs_id);
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::get_motors_status caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get motors status - Caught DevFailed!"), 
                     _CPTC("AxisInterface::get_motors_status"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to disable axis" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get motors status - Caught unknown exception!"), 
							_CPTC("AxisInterface::get_motors_status")); 
  }

	return status;
}

// ============================================================================
// AxisInterface::get_ref_status
// ============================================================================
sgonpmaclib::E_cs_referencing_status_t AxisInterface::get_ref_status()
{
	CHECK_CTRL_STD();

	sgonpmaclib::E_cs_referencing_status_t ref_status;
  ref_status = sgonpmaclib::E_cs_referencing_status_t::CS_REF_POS_UNKNOWN;
  try
  {
		ref_status = this->m_stdDataInterface->get_ref_status(m_cs_id);
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::get_ref_status caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get referencing status details - Caught DevFailed!"), 
                     _CPTC("AxisInterface::get_ref_status"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to disable axis" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get referencing status details - Caught unknown exception!"), 
							_CPTC("AxisInterface::get_ref_status")); 
  }

	return ref_status;
}

// ============================================================================
// AxisInterface::get_compens_table_validity
// ============================================================================
bool AxisInterface::get_compens_table_validity()
{
  bool compensTableValidity;
  compensTableValidity = false;
  
  try{
    return this->m_stdDataInterface->get_compens_table_validity(m_cs_id);
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::get_compens_table_validity caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get validity state of compensation table - Caught DevFailed!"), 
                     _CPTC("AxisInterface::get_compens_table_validity"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to get compensation table validity" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get validity state of compensation table - Caught unknown exception!"), 
							_CPTC("AxisInterface::get_compens_table_validity")); 
  }

  return compensTableValidity;
}

// ============================================================================
// AxisInterface::get_state
// ============================================================================
Tango::DevState AxisInterface::get_state()
{
	if (!m_ctlDataInterface || sgonppmac_ns::BoxInitDetector::instance()->is_init_needed(this))
	{
		ERROR_STREAM << "AxisInterface::get_state request aborted - the critical controller isn't accessible !"  << std::endl;
		return Tango::FAULT;
	}

  Tango::DevState state = Tango::STANDBY;
  sgonpmaclib::T_cs_status cs_status;   
  sgonpmaclib::E_cs_referencing_status_t referencing_status;   
  
  try
  {
    cs_status = this->get_status_details();
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::get_state caught DevFAILED: \n" << e << std::endl;
		return Tango::FAULT;
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to get state" << std::endl;
		return Tango::FAULT;
  }
  
  try
  {
    referencing_status = this->get_ref_status();
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::get_state caught DevFAILED: \n" << e << std::endl;
		return Tango::FAULT;
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to get state" << std::endl;
		return Tango::FAULT;
  }
  

  // Device State is FAULT with status bits: 
  if (cs_status.SyncAssignError || 
      cs_status.BufferError || 
      cs_status.LinToPvtError || 
      cs_status.PvtError || 
      cs_status.RunTimeError || 
      cs_status.RadiusError || 
      cs_status.RadiusError2 || 
      cs_status.I2tFault || 
      cs_status.AmpFault || 
      cs_status.FeFatal )
  {
   return Tango::FAULT;
  }
  
  // if referencing not done or if an error occured
  if (referencing_status == sgonpmaclib::E_cs_referencing_status_t::CS_REF_POS_KO ||
      referencing_status == sgonpmaclib::E_cs_referencing_status_t::CS_REF_POS_UNREF)
    return Tango::CLOSE;

  // Device State is ALARM with status bits: 
  if (cs_status.FeWarn || 
      cs_status.SoftLimit ||  
      cs_status.EncLoss || 
      cs_status.AmpWarn || 
      cs_status.TriggerNotFound || 
      cs_status.SoftPlusLimit || 
      cs_status.SoftMinusLimit || 
      cs_status.PlusLimit || 
      cs_status.MinusLimit|| 
      cs_status.LimitStop)
  {
    state = Tango::ALARM;
  }   
  
  // If compensation tables not valid, check if compensation is enabled for at least one axis
  // if so, set state = ALARM
  if (!this->m_stdDataInterface->get_compens_table_validity(m_cs_id))
  {
    if (this->compensation_enabled())
    {
      state = Tango::ALARM;
    }
  }
       
  // then force state to moving if "move in progress" flag is set
  if (m_isWritePosInProgress)
  {
    state = Tango::MOVING;
    // keep moving flag until low level returns moving state...
  }
  
  // If Move timer enabled
  if (cs_status.TimerEnabled)
  {
    state = Tango::MOVING;
    m_isWritePosInProgress = false; // moving state taken into account at low level
  }
  
  // AmpEnabled = 0, at least one motor is off
  if (!cs_status.AmpEna)
  {
    state = Tango::OFF;
  }
  
  // then get if box in error
  bool err = this->m_stdDataInterface->get_box_in_error();

  // if so, propagate box error to axis
  if (err)
  {
    state = Tango::FAULT;
  }
  
  return state;
}

// ============================================================================
// AxisInterface::get_acceleration
// ============================================================================
double AxisInterface::get_acceleration()
{
	CHECK_CTRL_STD();

	double accel = yat::IEEE_NAN;
  try
  {
		accel = this->m_stdDataInterface->get_acceleration(m_cs_id);
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::get_acceleration caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get acceleration - Caught DevFailed!"), 
                     _CPTC("AxisInterface::get_acceleration"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to get acceleration" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get acceleration - Caught unknown exception!"), 
							_CPTC("AxisInterface::get_acceleration")); 
  }

	return accel;
}

// ============================================================================
// AxisInterface::set_acceleration
// ============================================================================
void AxisInterface::set_acceleration(double acceleration)
{
	CHECK_CTRL_CMD();

#if defined (_SIMULATION_)
	CHECK_CTRL_STD();
#endif
  
	try
  {
		this->m_cmdInterface->set_acceleration(m_cs_id, acceleration);

#if defined (_SIMULATION_)
		this->m_stdDataInterface->set_acceleration(m_cs_id, acceleration);
#endif
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::set_acceleration caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to set acceleration - Caught DevFailed!"), 
                     _CPTC("AxisInterface::set_acceleration"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to set acceleration" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to set acceleration - Caught unknown exception!"), 
							_CPTC("AxisInterface::set_acceleration")); 
  }
}

// ============================================================================
// AxisInterface::get_accelSCurve
// ============================================================================
double AxisInterface::get_accelSCurve()
{
	CHECK_CTRL_STD();

	double accelSCurve = yat::IEEE_NAN;
  try
  {
		accelSCurve = this->m_stdDataInterface->get_accelSCurve(m_cs_id);
    
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::get_accelSCurve caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get accelSCurve - Caught DevFailed!"), 
                     _CPTC("AxisInterface::get_accelSCurve"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to get acceleration" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get accelSCurve - Caught unknown exception!"), 
							_CPTC("AxisInterface::get_accelSCurve")); 
  }

	return accelSCurve;
}

// ============================================================================
// AxisInterface::set_accelSCurve
// ============================================================================
void AxisInterface::set_accelSCurve(double accelSCurve)
{
	CHECK_CTRL_CMD();

#if defined (_SIMULATION_)
	CHECK_CTRL_STD();
#endif
  
	try
  {
		this->m_cmdInterface->set_accelSCurve(m_cs_id, accelSCurve);

#if defined (_SIMULATION_)
		this->m_stdDataInterface->set_accelSCurve(m_cs_id, accelSCurve);
#endif
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::set_accelSCurve caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to set accelSCurve - Caught DevFailed!"), 
                     _CPTC("AxisInterface::set_accelSCurve"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to set accelSCurve" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to set accelSCurve - Caught unknown exception!"), 
							_CPTC("AxisInterface::set_accelSCurve")); 
  }
}

// ============================================================================
// AxisInterface::get_deceleration
// ============================================================================
double AxisInterface::get_deceleration()
{
	CHECK_CTRL_STD();

	double decel = yat::IEEE_NAN;
  try
  {
		decel = this->m_stdDataInterface->get_deceleration(m_cs_id);
  
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::get_deceleration caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get deceleration - Caught DevFailed!"), 
                     _CPTC("AxisInterface::get_deceleration"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to get deceleration" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get deceleration - Caught unknown exception!"), 
							_CPTC("AxisInterface::get_deceleration")); 
  }

	return decel;
}

// ============================================================================
// AxisInterface::set_deceleration
// ============================================================================
void AxisInterface::set_deceleration(double deceleration)
{
	CHECK_CTRL_CMD();

#if defined (_SIMULATION_)
	CHECK_CTRL_STD();
#endif

	try
  {
		this->m_cmdInterface->set_deceleration(m_cs_id, deceleration);

#if defined (_SIMULATION_)
		this->m_stdDataInterface->set_deceleration(m_cs_id, deceleration);
#endif

  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::set_deceleration caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to set deceleration - Caught DevFailed!"), 
                     _CPTC("AxisInterface::set_deceleration"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to set deceleration" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to set deceleration - Caught unknown exception!"), 
							_CPTC("AxisInterface::set_deceleration")); 
  }
}

// ============================================================================
// AxisInterface::get_velocity
// ============================================================================
double AxisInterface::get_velocity()
{
	CHECK_CTRL_STD();

	double velocity = yat::IEEE_NAN;
  
  try
  {
		velocity = this->m_stdDataInterface->get_velocity(m_cs_id);
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::get_velocity caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get velocity - Caught DevFailed!"), 
                     _CPTC("AxisInterface::get_velocity"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to get velocity" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get velocity - Caught unknown exception!"), 
							_CPTC("AxisInterface::get_velocity")); 
  }

	return velocity;
}

// ============================================================================
// AxisInterface::set_velocity
// ============================================================================
void AxisInterface::set_velocity( double velocity)
{
	CHECK_CTRL_CMD();

#if defined (_SIMULATION_)
	CHECK_CTRL_STD();
#endif
	
	try
  {
		this->m_cmdInterface->set_velocity(m_cs_id, velocity);

#if defined (_SIMULATION_)
		this->m_stdDataInterface->set_velocity(m_cs_id, velocity);
#endif

	}
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::set_velocity caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to set velocity - Caught DevFailed!"), 
                     _CPTC("AxisInterface::set_velocity"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to set velocity" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to set velocity - Caught unknown exception!"), 
							_CPTC("AxisInterface::set_velocity")); 
  }
}

// ============================================================================
// AxisInterface::start_mp
// ============================================================================
void AxisInterface::start_mp(int mp)
{
	CHECK_CTRL_CMD();

#if defined (_SIMULATION_)
	CHECK_CTRL_CTL();
#endif

  try
  {
		this->m_cmdInterface->start_mp(m_cs_id, mp);

#if defined (_SIMULATION_)
		this->m_ctlDataInterface->start_mp(m_cs_id, mp);
#endif
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::start_mp caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to start MP - Caught DevFailed!"), 
                     _CPTC("AxisInterface::start_mp"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to start MP" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to start MP - Caught unknown exception!"), 
							_CPTC("AxisInterface::start_mp")); 
  }
}

// ============================================================================
// AxisInterface::pause_mp
// ============================================================================
void AxisInterface::pause_mp()
{
	CHECK_CTRL_CMD();

#if defined (_SIMULATION_)
	CHECK_CTRL_CTL();
#endif

  try
  {
		this->m_cmdInterface->pause_mp(m_cs_id);

#if defined (_SIMULATION_)
		this->m_ctlDataInterface->pause_mp(m_cs_id);
#endif
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::pause_mp caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to pause MP - Caught DevFailed!"), 
                     _CPTC("AxisInterface::pause_mp"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to pause MP" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to pause MP - Caught unknown exception!"), 
							_CPTC("AxisInterface::pause_mp")); 
  }
}

// ============================================================================
// AxisInterface::resume_mp
// ============================================================================
void AxisInterface::resume_mp()
{
	CHECK_CTRL_CMD();

#if defined (_SIMULATION_)
	CHECK_CTRL_CTL();
#endif

  try
  {
		this->m_cmdInterface->resume_mp(m_cs_id);

#if defined (_SIMULATION_)
		this->m_ctlDataInterface->resume_mp(m_cs_id);
#endif
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::resume_mp caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to resume MP - Caught DevFailed!"), 
                     _CPTC("AxisInterface::resume_mp"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to resume MP" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to resume MP - Caught unknown exception!"), 
							_CPTC("AxisInterface::resume_mp")); 
  }
}

// ============================================================================
// AxisInterface::get_current_mp_state
// ============================================================================
sgonpmaclib::E_mp_state_t AxisInterface::get_current_mp_state()
{
	CHECK_CTRL_STD();

	sgonpmaclib::E_mp_state_t st = sgonpmaclib::MP_STATE_UNKNOWN;
  try
  {
		st = this->m_stdDataInterface->get_mp_state(m_cs_id);
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "AxisInterface::get_current_mp_state caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get MP state - Caught DevFailed!"), 
                     _CPTC("AxisInterface::get_current_mp_state"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to get MP state" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get MP state - Caught unknown exception!"), 
							_CPTC("AxisInterface::get_current_mp_state")); 
  }

	return st;
}

// ============================================================================
// ComposedAxis::get_moving_mode
// ============================================================================
Tango::DevUShort AxisInterface::get_moving_mode()
{
	CHECK_CTRL_STD();

	Tango::DevUShort mv_mode = 0;
  try
  {
		mv_mode = static_cast<Tango::DevUShort>( this->m_stdDataInterface->get_cs_moving_mode(m_cs_id) );
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "ComposedAxis::get_moving_mode caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get moving mode - Caught DevFailed!"), 
                     _CPTC("ComposedAxis::get_moving_mode"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to get moving mode" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get moving mode - Caught unknown exception!"), 
							_CPTC("ComposedAxis::get_moving_mode")); 
  }

	return mv_mode;
}

// ============================================================================
// ComposedAxis::set_moving_mode
// ============================================================================
void AxisInterface::set_moving_mode(Tango::DevUShort value)
{
	CHECK_CTRL_CMD();

#if defined (_SIMULATION_)
	CHECK_CTRL_STD();
#endif
	

	try
  {
		this->m_cmdInterface->set_cs_moving_mode(m_cs_id, static_cast<sgonpmaclib::E_moving_mode_t>(value));

#if defined (_SIMULATION_)
		this->m_stdDataInterface->set_cs_moving_mode(m_cs_id, static_cast<sgonpmaclib::E_moving_mode_t>(value));
#endif

	}
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "ComposedAxis::set_moving_mode caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to set moving mode - Caught DevFailed!"), 
                     _CPTC("ComposedAxis::set_moving_mode"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to set moving mode" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to set moving mode - Caught unknown exception!"), 
							_CPTC("ComposedAxis::set_moving_mode")); 
  }
}

} // namespace sgonppmac_ns



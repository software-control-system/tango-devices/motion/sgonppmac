//=============================================================================
// AxisInterface.h
//=============================================================================
// class.............Axis Interface (Base classs for all Axis type)
// original author...F. THIAM
//=============================================================================

#ifndef _AXIS_INTERFACE_H_
#define _AXIS_INTERFACE_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <yat4tango/InnerAppender.h>
#include <yat4tango/LogHelper.h>
#include "SGonPPMACTypesAndConsts.h"
#include "ControllerFactory.h"


namespace sgonppmac_ns {


// ============================================================================
// class: AxisInterface
// ============================================================================
class AxisInterface : public yat4tango::TangoLogAdapter
{

public:

	
  // Constructor.
  AxisInterface(Tango::DeviceImpl * hostDevice);

  // Destructor.
  virtual ~AxisInterface();

	
  //---------------------------------------------------------------------------
  // Controller management
  //---------------------------------------------------------------------------

	// Set the axis initialization data
	virtual void set_axis_init_data(AxisInitData &initData ) = 0;

	// Declare axis list
	// @param axisDefinition   (depend of type axis) list of Cs:Axis or Cs:Axis:Label.
	virtual void init_axis( vector<string>	axisDefinition) = 0;

	// Undeclare axis list
	virtual void undeclare_axes() = 0;

  // Powers on the underlying motor.
  void motor_on();

  // Powers off the underlying motor.
  // @param id CS id.
	void motor_off();

  // Powers off the underlying motor.
  // @param id CS id.
  void motor_omega_off();  
  
  // Powers off the underlying motor.
  // @param id CS id.
  void motor_other_off();
  
	// Stop current axis movement.
  void axis_stop();

	// Disable an axis
	void disable();

  // get motion program state
  sgonpmaclib::E_mp_state_t get_current_mp_state();

  // start motion program id
  void start_mp(int mp);

  // pause motion program
  void pause_mp();

  // resume motion program
  void resume_mp();

  // Gets current CS status.
  // @return CS status.
	virtual sgonpmaclib::T_cs_status get_status_details();
  
  // Gets current CS Motor status.
  // @return CS Motor status.
	virtual std::string get_motors_status();
  
  // Gets current ref CS status.
  // @return ref CS status.
	virtual sgonpmaclib::E_cs_referencing_status_t get_ref_status();

  // Gets current dev state.
  // @return DevState.
	virtual Tango::DevState get_state();

	// Gets current axis acceleration, in user unit/s^2.
  // @return Current axis acceleration, in user unit/s^2.
	virtual double get_acceleration();

  // Sets axis acceleration, in user unit/s^2.
  // @param acceleration Axis acceleration setpoint in user unit/s^2.
	void set_acceleration(double acceleration);

  // Gets current axis deceleration, in user unit/s^2.
  // @return Current axis deceleration, in user unit/s^2.
	virtual double get_deceleration();

  // Sets axis deceleration, in user unit/s^2.
  // @param deceleration Axis deceleration setpoint in user unit/s^2.
	void set_deceleration(double deceleration);

  // Gets current axis velocity, in user unit/s.
  // @return Current axis velocity, in user unit/s.
	virtual double get_velocity();

	// Sets axis velocity, in user unit/s.
  // @param velocity Axis velocity setpoint in user unit/s.
	void set_velocity(double velocity);
  
  // Gets current axis moving mode.
  // @return Current axis moving mode, among: 0 = rapid, 1 = linear, ... (see E_moving_mode_t type).
  virtual Tango::DevUShort get_moving_mode();

	// Sets axis moving mode, among: 0 = rapid, 1 = linear, ... (see E_moving_mode_t type).
  // @param value Axis moving mode.
  void set_moving_mode(Tango::DevUShort value);
  
  // Checks if at least one compensation table is enabled.
  // @return True = at least ne compensation table enabled, false otherwise.
  virtual bool compensation_enabled() = 0;
  
  // Gets compensation table validity.
  // @return Current compentation validity.
  bool get_compens_table_validity();

  // Gets accelSCurve.
  // @return Current accelSCurve.
  double get_accelSCurve();
  
	// Sets axis accelSCurve.
  // @param accelSCurve 
  void set_accelSCurve(double accelSCurve);
  

protected:

	//- Cmd ControlerInterface
	sgonppmac_ns::ControllerInterface * m_cmdInterface;

	//- Std Data ControlerInterface
	sgonppmac_ns::ControllerInterface * m_stdDataInterface;

	//- Ctl Data ControlerInterface
	sgonppmac_ns::ControllerInterface * m_ctlDataInterface;

	
	// Parent device
	Tango::DeviceImpl *m_hostDevice;

	// Current Cs Id
	sgonpmaclib::T_cs_id m_cs_id;

  // write position asked flag
  bool m_isWritePosInProgress;

	// Init all ControlerInterface pointers
	void init_controler_interface( );

	// Declare axis list
	// @param axis_list Axis id (cs, axis name) list.
	void declare_axis( std::vector<sgonpmaclib::T_axis_id> axis_list );

	// Undeclare axis list
	// @param axis_list Axis id (cs, axis name) list.
	void undeclare_axis( std::vector<sgonpmaclib::T_axis_id> axis_list );

};

} // namespace sgonppmac_ns

#endif // _AXIS_INTERFACE_H_

//=============================================================================
// BoxInitDetector.cpp
//=============================================================================
// class.............BoxInitDetector
// original author...S. MINOLLI
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "BoxInitDetector.h"


namespace sgonppmac_ns {

// ============================================================================
// SINGLETON
// ============================================================================
BoxInitDetector * BoxInitDetector::singleton = 0;

// ============================================================================
// BoxInitDetector::init <STATIC MEMBER>
// ============================================================================
void BoxInitDetector::init () 
{
  //- already instanciated?
  if (BoxInitDetector::singleton)
    return;

  //- actual instanciation
  BoxInitDetector::singleton = new BoxInitDetector();
  if (BoxInitDetector::singleton == 0)
    throw std::bad_alloc();
}

// ============================================================================
// BoxInitDetector::close  <STATIC MEMBER>
// ============================================================================
void BoxInitDetector::close ()
{
  if (! BoxInitDetector::singleton)
    return;

  //- release the <BoxInitDetector> singleton
  delete (BoxInitDetector::singleton);
}

// ============================================================================
// BoxInitDetector::BoxInitDetector
// ============================================================================
BoxInitDetector::BoxInitDetector ()
{
  yat::AutoMutex<> guard(BoxInitDetector::m_state_lock);
	m_init_state = true;
  m_listeners.clear();
}

// ============================================================================
// BoxInitDetector::~BoxInitDetector
// ============================================================================
BoxInitDetector::~BoxInitDetector (void)
{
}

// ============================================================================
// BoxInitDetector::set_box_init_state
// ============================================================================
void BoxInitDetector::set_box_init_state(bool tof)
{
  yat::AutoMutex<> guard(BoxInitDetector::m_state_lock);
  m_init_state = tof;

  // if box initializing, set listeners flags to true
  if (m_init_state)
  {
    std::cout << "BoxInitDetector::set_box_init_state true!" << std::endl;
		for (std::map<void *, bool>::iterator it = m_listeners.begin(); 
         it != m_listeners.end(); ++it)
		{
			it->second = true;
		}
  }
}

// ============================================================================
// BoxInitDetector::add_listener
// ============================================================================
void BoxInitDetector::add_listener(void * id)
{
  std::cout << "BoxInitDetector::add_listener id = " << id << std::endl;

  yat::AutoMutex<> guard(BoxInitDetector::m_state_lock);
  
  // add listener if not already in list
  if (m_listeners.count(id) == 0)
  {
    m_listeners.insert( std::pair<void *, bool>(id, false) ); 
  }
  else
  {
    // reinitialize flag
    m_listeners[id] = false;
  }
}

// ============================================================================
// BoxInitDetector::is_init_needed
// ============================================================================
bool BoxInitDetector::is_init_needed(void * id)
{
  bool tof = true;

  yat::AutoMutex<> guard(BoxInitDetector::m_state_lock);

  if (m_listeners.count(id) != 0)
  {
    tof = m_listeners[id];
  }

  return tof;
}

} // namespace sgonppmac_ns



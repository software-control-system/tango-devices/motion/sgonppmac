//=============================================================================
// BoxInitDetector.h
//=============================================================================
// class.............BoxInitDetector
// original author...S. MINOLLI
//=============================================================================

#ifndef _BOX_INIT_DETECTOR_H_
#define _BOX_INIT_DETECTOR_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================

#include <tango.h>
#include <yat/threading/Mutex.h>

namespace sgonppmac_ns {


// ============================================================================
// class: BoxInitDetector
// This class is a singleton created at device server initialization.
// It provides "need init" flags for listeners (here axes devices), enabled
// when the main class (here the box device) has been re-initialized.
// ============================================================================
class BoxInitDetector
{

public:

	//----------------------------------------
  // SINGLETON 
  //----------------------------------------
  static inline BoxInitDetector * instance ()
  {
    if (!BoxInitDetector::singleton)
        return 0;

    return BoxInitDetector::singleton;
  }

  //- instanciation
  static void init ();

  //- release
  static void close ();

  //- add a listener (with unique id)
  void add_listener(void * id);

	//- does listener need init?
	bool is_init_needed(void * id);

	//- set box init state
	void set_box_init_state(bool tof);
	
private:
  //- ctor -------------
  BoxInitDetector ();
	
  //- dtor -------------
  virtual ~BoxInitDetector ();

  //- the singleton
  static BoxInitDetector * singleton;

  //- the box init state: true = box intializing, false = box initialized
  bool m_init_state;

  //- state lock
  yat::Mutex m_state_lock;
	
  //- listeners = map of <pt, flag>
  std::map<void *, bool> m_listeners;
};

} // namespace sgonppmac_ns

#endif // _BOX_INIT_DETECTOR_H_

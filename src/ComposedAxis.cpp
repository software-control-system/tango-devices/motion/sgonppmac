//=============================================================================
// ComposedAxis.cpp
//=============================================================================
// class.............ComposedAxis
// original author...F. THIAM
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <sstream>
#include "ComposedAxis.h"
#include "DynComposedAxisAttrib.h"
#include "SGonPPMACComposedAxis.h"
#include <yat4tango/PropertyHelper.h>
#include "BoxInitDetector.h" 

// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================


namespace sgonppmac_ns {

	//- check controller cmd macro:
#define CHECK_CTRL_CMD() \
	do \
	{ \
	if (!m_cmdInterface || sgonppmac_ns::BoxInitDetector::instance()->is_init_needed(this)) \
	THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
	_CPTC("request aborted - the command controller isn't accessible "), \
  _CPTC("ComposedAxis::check_ctrl_cmd")); \
	} while (0)

	//- check controller std macro:
#define CHECK_CTRL_STD() \
	do \
	{ \
	if (!m_stdDataInterface || sgonppmac_ns::BoxInitDetector::instance()->is_init_needed(this)) \
	THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
	_CPTC("request aborted - the standard controller isn't accessible "), \
  _CPTC("ComposedAxis::check_ctrl_std")); \
	} while (0)

	//- check controller ctl macro:
#define CHECK_CTRL_CTL() \
	do \
	{ \
	if (!m_ctlDataInterface || sgonppmac_ns::BoxInitDetector::instance()->is_init_needed(this)) \
	THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
	_CPTC("request aborted - the critical controller isn't accessible "), \
  _CPTC("ComposedAxis::check_ctrl_ctl")); \
	} while (0)


// ============================================================================

// ============================================================================
// ComposedAxis::ComposedAxis
// ============================================================================
ComposedAxis::ComposedAxis(Tango::DeviceImpl * hostDevice)
: AxisInterface(hostDevice), m_freeze(false)
{
	INFO_STREAM << "ComposedAxis: hostDevice=" << hostDevice 
								<< " this=" << this << std::endl;
}

// ============================================================================
// ComposedAxis::~ComposedAxis
// ============================================================================
ComposedAxis::~ComposedAxis ()
{
	// remove dynamic attributes 
	try
	{
    m_local_axes.clear();
  }
	catch (...)
	{
		//- ignore any error
	}
}

// ============================================================================
// ComposedAxis::init_axis
// ============================================================================
void ComposedAxis::init_axis(vector<string> axisDefinition)
{
	try 
	{
		this->init_controler_interface();
	}
	catch(...)
	{
		throw;
	} 
  
	std::vector<sgonpmaclib::T_axis_id> axisList;
	AxisParams axisParams;
  
	// extract axis definition
	for (vector<string>::iterator i = axisDefinition.begin(); i != axisDefinition.end(); ++i)
	{
		// Each line of the axisDefinition property contains: "<CS>::<axis>::<label>:<userunit>"
    // in case of a composed axis, there can be lot of lines 
    sgonpmaclib::T_axis_id local_axis;
    std::string axis_id;
    axis_id = *i; 
    //std::cout<<"init_axis  : "<<axis_id<<std::endl;
    std::vector<std::string> l_vect = split_property_line(axis_id);

    // Extract CS number
    std::string cs_str = l_vect.at(0);
		local_axis.cs = atoi(cs_str.c_str());
		m_cs_id = local_axis.cs;

    // Extract axis name
    local_axis.axis_name = l_vect.at(1);

    // Extract axis label (label should be used to create associated dynamic attribute
    // but here, the attributes are statically defined)
    std::string cs_label = l_vect.at(2);
    
    // Extract axis user unit
    axisParams.user_unit_name = l_vect.at(3);
    
    double userUnit = this->getUserUnitValue(axisParams.user_unit_name);   
    axisParams.user_unit = userUnit;
    
    // should check here if CS is unique, and if axis not declared twice or more...
    this->m_local_axes.insert( std::pair<std::string, sgonpmaclib::T_axis_id>(cs_label, local_axis) ); 
    
    //DEBUG_STREAM << "ComposedAxis::init_axis: axis (cs, name, label) = (" << cs_str << ", " << local_axis.axis_name	<< ", " << cs_label << ")" << std::endl;

		this->m_axesParams.insert( std::pair<std::string, AxisParams>(cs_label, axisParams) );
	
		axisList.push_back(local_axis);
  }

  try
  {
		// Declare the axis 
    this->declare_axis(axisList);
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "ComposedAxis::init_axis Axis declaration NOK: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to init axis - Caught DevFailed!"), 
                     _CPTC("ComposedAxis::init_axis"));
  }
  catch(...)
  {
		ERROR_STREAM << "ComposedAxis::init_axis Axis declaration NOK" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to init axis - Caught unknown exception!"), 
							_CPTC("ComposedAxis::init_axis")); 
  }
}

// ============================================================================
// ComposedAxis::getLabels
// ============================================================================
void ComposedAxis::getLabels(std::vector<std::string> &listLabels)
{
	listLabels.clear();

	// Parse the axes map to get all labels
	for (std::map<std::string, sgonpmaclib::T_axis_id>::iterator itAxis = this->m_local_axes.begin(); itAxis != this->m_local_axes.end(); ++itAxis)
	{
		listLabels.push_back(itAxis->first);
	}
}

// ============================================================================
// ComposedAxis::undeclare_axes
// ============================================================================
void ComposedAxis::undeclare_axes( )
{
	try
  {
		std::vector<sgonpmaclib::T_axis_id> axisList;

		// Parse the axes map to get all T_axis_id
		for (std::map<std::string, sgonpmaclib::T_axis_id>::iterator itAxis = this->m_local_axes.begin(); itAxis != this->m_local_axes.end(); ++itAxis)
		{
			axisList.push_back(itAxis->second);
		}

		// Undeclare the axis 
		if( axisList.size() > 0 )
		{
			this->undeclare_axis(axisList);
		}
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "ComposedAxis::undeclare_axes Axis undeclaration NOK: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to undeclare axis - Caught DevFailed!"), 
                     _CPTC("ComposedAxis::undeclare_axes"));
  }
  catch(...)
  {
		ERROR_STREAM << "ComposedAxis::undeclare_axes Axis undeclaration NOK" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to undeclare axis - Caught unknown exception!"), 
							_CPTC("ComposedAxis::undeclare_axes")); 
  }
}

// ============================================================================
// ComposedAxis::set_axis_init_data
// ============================================================================
void ComposedAxis::set_axis_init_data(AxisInitData &initData )
{
	// Do nothing for ComposedAxis
}

// ============================================================================
// ComposedAxis::init_dynamic_params
// ============================================================================
void ComposedAxis::init_dynamic_params()
{

	// Dynamic attribute
	try
	{
		// Parse the axes map to get all label and create associate dynamic attributes
		for (std::map<std::string, sgonpmaclib::T_axis_id>::iterator itAxis = this->m_local_axes.begin(); itAxis != this->m_local_axes.end(); ++itAxis)
		{
			string cs_label = itAxis->first;
			INFO_STREAM << "ComposedAxis::init_dynamic_params label=" << cs_label << std::endl;

			create_attr_label(cs_label);
			create_attr_labelOffset(cs_label);
      create_attr_labelCompensationEnabled(cs_label);
		}

		// Attach attributes to the device	
		this->attach_dynamic_attributes_to_device();
		INFO_STREAM << "ComposedAxis::init_dynamic_params OK"  << std::endl;
	}
	catch( Tango::DevFailed &df )
	{
		ERROR_STREAM << "ComposedAxis::init_dynamic_params Dynamic attribute creation failed: " << df << std::endl;
		RETHROW_DEVFAILED(df,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to create dynamic attribute - Caught DevFailed!"), 
                     _CPTC("ComposedAxis::init_dynamic_params"));
	}
	catch(...)
	{
		ERROR_STREAM << "Failed to create dynamic attribute" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to create dynamic attribute - Caught unknown exception!"), 
							_CPTC("ComposedAxis::init_dynamic_params")); 
	}
}

// ============================================================================
// ComposedAxis::remove_dynamic_params
// ============================================================================
void ComposedAxis::remove_dynamic_params()
{

	try
	{
		// Detach and remove dynamic attributes
		this->detach_dynamic_attributes_from_device();

		INFO_STREAM << "ComposedAxis::remove_dynamic_params OK" << endl;
	}
	catch( Tango::DevFailed &df )
	{
		ERROR_STREAM << "ComposedAxis::remove_dynamic_params remove dynamic attributes failed: " << df << std::endl;
		RETHROW_DEVFAILED(df,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to remove dynamic attributes - Caught DevFailed!"), 
                     _CPTC("ComposedAxis::remove_dynamic_params"));
	}
	catch(...)
	{
		ERROR_STREAM << "Failed to remove dynamic attributes" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to remove dynamic attributes - Caught unknown exception!"), 
							_CPTC("ComposedAxis::remove_dynamic_params")); 
	}
}

// ============================================================================
// ComposedAxis::attach_dynamic_attributes_to_device
// ============================================================================
void ComposedAxis::attach_dynamic_attributes_to_device(void)
{
	size_t i;

	for(i=0; i<_dynamic_attribute_axes.size(); ++i)
	{
		INFO_STREAM << "attach_dynamic_attributes_to_device: attr=" << _dynamic_attribute_axes[i] 
								<< " name=" << _dynamic_attribute_axes[i]->get_name() << std::endl;
			
		this->m_hostDevice->add_attribute(_dynamic_attribute_axes[i]);
	}
}

// ============================================================================
// ComposedAxis::detach_dynamic_attributes_from_device
// ============================================================================
void ComposedAxis::detach_dynamic_attributes_from_device(void)
{
	size_t i;
	for(i=0; i<_dynamic_attribute_axes.size(); ++i)
	{
		if(_dynamic_attribute_axes[i])
		{
			
			INFO_STREAM << "detach_dynamic_attributes_from_device: attr=" << _dynamic_attribute_axes[i] << std::endl;

			try 
			{
				this->m_hostDevice->remove_attribute(_dynamic_attribute_axes[i], false, false); // add no db cleanup at remove
			}
			catch(...)
			{
				ERROR_STREAM << "ComposedAxis::detach_dynamic_attributes_from_device Failed to remove_attribute " << _dynamic_attribute_axes[i] << endl;
			}

			try 
			{
				delete _dynamic_attribute_axes[i];
			}
			catch(...)
			{
				ERROR_STREAM << "ComposedAxis::detach_dynamic_attributes_from_device Failed to delete attribute " << _dynamic_attribute_axes[i] << endl;
			}

			_dynamic_attribute_axes[i] = NULL;
			
		}
	}
	_dynamic_attribute_axes.clear();
}

// ============================================================================
// ComposedAxis::freeze_read_cb
// ============================================================================
bool ComposedAxis::get_freeze()
{
	//DEBUG_STREAM << "ComposedAxis::get_freeze entering... "<< endl;
	return m_freeze;
}

// ============================================================================
// ComposedAxis::set_freeze
// ============================================================================
void ComposedAxis::set_freeze(bool value)
{
	DEBUG_STREAM << "ComposedAxis::set_freeze entering... "<< endl;

	CHECK_CTRL_CMD();
	
#if defined (_SIMULATION_)
	CHECK_CTRL_CTL();
#endif

  // do something only if freeze value has changed
  if (m_freeze != value) 
  {
    if (!value) // unfreeze
    {
      try
      {
        this->apply_write_label_part();
        //then set freeze to false...
        m_freeze = value;
        this->m_cmdInterface->apply_position(m_cs_id);

        // if apply position is OK, set "move in progress" flag to true (for scan needs)
        m_isWritePosInProgress = true;

#if defined (_SIMULATION_)
	this->m_ctlDataInterface->apply_position(m_cs_id);
#endif
			}
			catch(Tango::DevFailed &e)
			{
				ERROR_STREAM << "ComposedAxis::set_freeze caught DevFAILED: \n" << e << std::endl;
				RETHROW_DEVFAILED(e,
												 _CPTC("DEVICE_ERROR"),
												 _CPTC("Failed to apply position - Caught DevFailed!"), 
												 _CPTC("ComposedAxis::set_freeze"));
			}
			catch(...)
			{
				ERROR_STREAM << "Failed to apply position" << std::endl;
				THROW_DEVFAILED(
									_CPTC("DEVICE_ERROR"), 
									_CPTC("Failed to apply position - Caught unknown exception!"), 
									_CPTC("ComposedAxis::set_freeze")); 
			}
		}
    else 
      m_freeze = value;
	}
}

// ============================================================================
// ComposedAxis::apply_write_label_part
// ============================================================================
void ComposedAxis::apply_write_label_part()
{
  std::vector<std::string> listLabels;
  this->getLabels(listLabels);

  bool memorized = true;
  //For each label
  for(size_t i=0 ; i < listLabels.size(); ++i){
    try{
      std::string attr_label_name = listLabels[i];
      // Get Attribute write part
      Tango::WAttribute &attrW = m_hostDevice->get_device_attr()->get_w_attr_by_name(attr_label_name.c_str());
      Tango::DevDouble attr_write_value;
      attrW.get_write_value(attr_write_value);
      //apply last write part on labels before unfreeze 
      this->label_write_cb (attr_label_name, attr_write_value, memorized);
    }catch(Tango::DevFailed &e)
			{
				ERROR_STREAM << "ComposedAxis::apply_write_label_part caught DevFAILED: \n" << e << std::endl;
				RETHROW_DEVFAILED(e,
												 _CPTC("DEVICE_ERROR"),
												 _CPTC("Failed to prepare unfreeze - Caught DevFailed!"), 
												 _CPTC("ComposedAxis::apply_write_label_part"));
			}
			catch(...)
			{
				ERROR_STREAM << "Failed to prepare unfreeze" << std::endl;
				THROW_DEVFAILED(
									_CPTC("DEVICE_ERROR"), 
									_CPTC("Failed to prepare unfreeze - Caught unknown exception!"), 
									_CPTC("ComposedAxis::apply_write_label_part")); 
			}
  }
}
/*
MOVE MOVING MODE METHODS DIRECTLY TO AxisInterface class
NEED TO USE MODE TO SET VELOCITY...
// ============================================================================
// ComposedAxis::get_moving_mode
// ============================================================================
// ============================================================================
// ComposedAxis::set_moving_mode
// ============================================================================
*/

// ============================================================================
// ComposedAxis::create_attr_label
// ============================================================================
void ComposedAxis::create_attr_label(std::string label)
{
	string name = label;
	DynComposedAxisAttrib<SGonPPMACComposedAxis_ns::SGonPPMACComposedAxis> *att;
	
	att = new DynComposedAxisAttrib<SGonPPMACComposedAxis_ns::SGonPPMACComposedAxis>(name.c_str(), Tango::DEV_DOUBLE, Tango::READ_WRITE);
	
  const char *unit_to_set = m_axesParams[label].user_unit_name.c_str();
  
	Tango::UserDefaultAttrProp axis_prop;
	axis_prop.set_label(name.c_str());
	axis_prop.set_format("%6.2f");
	axis_prop.set_description("Current axis position, in user unit");
	axis_prop.set_unit(unit_to_set);
  
	att->set_user_data(label);
	att->setReadCallback(&SGonPPMACComposedAxis_ns::SGonPPMACComposedAxis::label_read_cb);
	att->setWriteCallback(&SGonPPMACComposedAxis_ns::SGonPPMACComposedAxis::label_write_cb);
	att->set_default_properties(axis_prop);

	_dynamic_attribute_axes.push_back(att);
}

// ============================================================================
// ComposedAxis::label_read_cb
// ============================================================================
void ComposedAxis::label_read_cb (string label, Tango::Attribute &attr)
{
	//DEBUG_STREAM << "ComposedAxis::label_read_cb entering... "<< endl;

	CHECK_CTRL_CTL();

	//DEBUG_STREAM << "label_read_cb: label = " << label << std::endl;

	if (this->m_local_axes.count(label) != 0 && this->m_axesParams.count(label) != 0)
	{
    m_axesParams[label].position = this->m_ctlDataInterface->get_position(m_local_axes[label]);
    
    //Apply UU
    m_axesParams[label].position = m_axesParams[label].position / m_axesParams[label].user_unit;
     
		attr.set_value(&m_axesParams[label].position);
	}
	else
	{
		ERROR_STREAM << "ComposedAxis::label_read_cb attribute label " << label << " doesn't exists ! " << endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get position - Caught DevFailed!"), 
							_CPTC("ComposedAxis::label_read_cb")); 
	}
}

// ============================================================================
// ComposedAxis::position_write_cb
// ============================================================================
void ComposedAxis::label_write_cb (string label, Tango::DevDouble value, bool bMemorized)
{
	DEBUG_STREAM << "ComposedAxis::position_write_cb entering... "<< endl;

	CHECK_CTRL_CMD();
		
	#if defined (_SIMULATION_)
		CHECK_CTRL_CTL();
	#endif
  
  //std::cout<<"ComposedAxis::label_write_cb =  "<<value<<std::endl;
  //Apply user Unit 
  double valueToApply = m_axesParams[label].user_unit * value;
  
	if (this->m_local_axes.count(label) != 0)
	{
		try
		{
      this->m_cmdInterface->set_position(m_local_axes[label], valueToApply, !m_freeze);

      // if set position is OK & apply required, set "move in progress" flag to true (for scan needs)
      if (!m_freeze)
        m_isWritePosInProgress = true;

#if defined (_SIMULATION_)
      this->m_ctlDataInterface->set_position(m_local_axes[label], valueToApply, !m_freeze);
#endif

     if( bMemorized )
        {
          // memorize new value as device property
          std::string prop_name = std::string("__") + label;
          yat4tango::PropertyHelper::set_property<Tango::DevDouble>(this->m_hostDevice, prop_name, value);
        }
		}

		catch(Tango::DevFailed &e)
		{
			ERROR_STREAM << "ComposedAxis::label_write_cb caught DevFAILED: \n" << e << std::endl;
			RETHROW_DEVFAILED(e,
											 _CPTC("DEVICE_ERROR"),
											 _CPTC("Failed to set position - Caught DevFailed!"), 
											 _CPTC("ComposedAxis::label_write_cb"));
		}
		catch(...)
		{
			ERROR_STREAM << "Failed to set position" << std::endl;
			THROW_DEVFAILED(
								_CPTC("DEVICE_ERROR"), 
								_CPTC("Failed to set position - Caught unknown exception!"), 
								_CPTC("ComposedAxis::label_write_cb")); 
		}
	}
	else
	{
		ERROR_STREAM << "ComposedAxis::label_write_cb attribute label " << label << " doesn't exists ! " << endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to set position - Caught DevFailed!"), 
							_CPTC("ComposedAxis::label_write_cb")); 
	}
}

// ============================================================================
// ComposedAxis::create_attr_labelOffset
// ============================================================================
void ComposedAxis::create_attr_labelOffset(std::string label)
{
	string name = label + "Offset";
	DynComposedAxisAttrib<SGonPPMACComposedAxis_ns::SGonPPMACComposedAxis> *att;
	
	att = new DynComposedAxisAttrib<SGonPPMACComposedAxis_ns::SGonPPMACComposedAxis>(name.c_str(), Tango::DEV_DOUBLE, Tango::READ_WRITE);
	
	Tango::UserDefaultAttrProp axis_prop;
	axis_prop.set_label(name.c_str());
	axis_prop.set_format("%6.2f");
	axis_prop.set_description("User offset on axis position, in user unit");
  
	att->set_user_data(label);
	att->setReadCallback(&SGonPPMACComposedAxis_ns::SGonPPMACComposedAxis::labelOffset_read_cb);
	att->setWriteCallback(&SGonPPMACComposedAxis_ns::SGonPPMACComposedAxis::labelOffset_write_cb);
	att->set_default_properties(axis_prop);
	att->set_disp_level(Tango::EXPERT);

	_dynamic_attribute_axes.push_back(att);
}

// ============================================================================
// ComposedAxis::labelOffset_read_cb
// ============================================================================
void ComposedAxis::labelOffset_read_cb (string label, Tango::Attribute &attr)
{
	//DEBUG_STREAM << "ComposedAxis::offset_read_cb entering... "<< endl;

	CHECK_CTRL_STD();
	
	//DEBUG_STREAM << "labelOffset_read_cb: label = " << label << std::endl;

	if (this->m_local_axes.count(label) != 0 && this->m_axesParams.count(label) != 0)
	{
	    m_axesParams[label].offset = this->m_ctlDataInterface->get_offset(m_local_axes[label]);
    //Apply user Unit 
    m_axesParams[label].offset = m_axesParams[label].offset / m_axesParams[label].user_unit;
		//Apply value
		attr.set_value(&m_axesParams[label].offset);
	}
	else
	{
		ERROR_STREAM << "ComposedAxis::labelOffset_read_cb attribute label " << label << " doesn't exists ! " << endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get offset - Caught DevFailed!"), 
							_CPTC("ComposedAxis::labelOffset_read_cb")); 
	}
}

// ============================================================================
// ComposedAxis::labelOffset_write_cb
// ============================================================================
void ComposedAxis::labelOffset_write_cb (string label, Tango::DevDouble value, bool bMemorized)
{
	DEBUG_STREAM << "ComposedAxis::labelOffset_write_cb entering... "<< endl;

	CHECK_CTRL_CMD();
	
#if defined (_SIMULATION_)
	CHECK_CTRL_STD();
#endif
	
  //Apply user unit
  double valueToApply = value * m_axesParams[label].user_unit;
  
	if (this->m_local_axes.count(label) != 0)
	{

		try
		{
			this->m_cmdInterface->set_offset(m_local_axes[label], valueToApply);

#if defined (_SIMULATION_)
			this->m_stdDataInterface->set_offset(m_local_axes[label], valueToApply);
#endif

			if( bMemorized )
			{
				// memorize new value as device property
				std::string prop_name = std::string("__") + label + "Offset";
				yat4tango::PropertyHelper::set_property<Tango::DevDouble>(this->m_hostDevice, prop_name, value);
			}
			
		}
		catch(Tango::DevFailed &e)
		{
			ERROR_STREAM << "ComposedAxis::labelOffset_write_cb caught DevFAILED: \n" << e << std::endl;
			RETHROW_DEVFAILED(e,
											 _CPTC("DEVICE_ERROR"),
											 _CPTC("Failed to set offset - Caught DevFailed!"), 
											 _CPTC("ComposedAxis::labelOffset_write_cb"));
		}
		catch(...)
		{
			ERROR_STREAM << "Failed to set offset" << std::endl;
			THROW_DEVFAILED(
								_CPTC("DEVICE_ERROR"), 
								_CPTC("Failed to set offset - Caught unknown exception!"), 
								_CPTC("ComposedAxis::labelOffset_write_cb")); 
		}
	}
	else
	{
		ERROR_STREAM << "ComposedAxis::labelOffset_write_cb attribute label " << label << " doesn't exists ! " << endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to set offset - Caught DevFailed!"), 
							_CPTC("ComposedAxis::labelOffset_write_cb")); 
	}
}

// ============================================================================
// ComposedAxis::create_attr_labelCompensationEnabled
// ============================================================================
void ComposedAxis::create_attr_labelCompensationEnabled(std::string label)
{
	string name = label + "CompensationEnabled";
	DynComposedAxisAttrib<SGonPPMACComposedAxis_ns::SGonPPMACComposedAxis> *att;
	
	att = new DynComposedAxisAttrib<SGonPPMACComposedAxis_ns::SGonPPMACComposedAxis>(name.c_str(), Tango::DEV_BOOLEAN, Tango::READ_WRITE);
	
	Tango::UserDefaultAttrProp axis_prop;
	axis_prop.set_label(name.c_str());
	axis_prop.set_description("Enable/disable compensation table");
  
	att->set_user_data(label);
	att->setReadCallback(&SGonPPMACComposedAxis_ns::SGonPPMACComposedAxis::labelCompensation_read_cb);
	att->setWriteCallback(&SGonPPMACComposedAxis_ns::SGonPPMACComposedAxis::labelCompensation_write_cb);
	att->set_default_properties(axis_prop);
	att->set_disp_level(Tango::EXPERT);

	_dynamic_attribute_axes.push_back(att);
}

// ============================================================================
// ComposedAxis::labelCompensation_read_cb
// ============================================================================
void ComposedAxis::labelCompensation_read_cb (string label, Tango::Attribute &attr)
{
	CHECK_CTRL_STD();
	
	//DEBUG_STREAM << "labelCompensation_read_cb: label = " << label << std::endl;

	if (this->m_local_axes.count(label) != 0 && this->m_axesParams.count(label) != 0)
	{
		m_axesParams[label].compensation = this->m_stdDataInterface->get_compensation(m_local_axes[label]);
    bool comp = m_axesParams[label].compensation;
		//Apply value
		attr.set_value(&comp);
	}
	else
	{
		ERROR_STREAM << "ComposedAxis::labelCompensation_read_cb attribute label " << label << " doesn't exists ! " << endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get offset - Caught DevFailed!"), 
							_CPTC("ComposedAxis::labelCompensation_read_cb")); 
	}
}

// ============================================================================
// ComposedAxis::labelCompensation_write_cb
// ============================================================================
void ComposedAxis::labelCompensation_write_cb (string label, Tango::DevBoolean value, bool bMemorized)
{
	DEBUG_STREAM << "ComposedAxis::labelCompensation_write_cb entering... "<< endl;

	CHECK_CTRL_CMD();
	
#if defined (_SIMULATION_)
	CHECK_CTRL_STD();
#endif
  
	if (this->m_local_axes.count(label) != 0)
	{
    
		try
		{
			this->m_cmdInterface->set_compensation(m_local_axes[label], value);

#if defined (_SIMULATION_)
			this->m_stdDataInterface->set_compensation(m_local_axes[label], value);
#endif

			if( bMemorized )
			{
				// memorize new value as device property
				std::string prop_name = std::string("__") + label + "Compensation";
				yat4tango::PropertyHelper::set_property<Tango::DevBoolean>(this->m_hostDevice, prop_name, value);
			}
			
		}
		catch(Tango::DevFailed &e)
		{
			ERROR_STREAM << "ComposedAxis::labelCompensation_write_cb caught DevFAILED: \n" << e << std::endl;
			RETHROW_DEVFAILED(e,
											 _CPTC("DEVICE_ERROR"),
											 _CPTC("Failed to set offset - Caught DevFailed!"), 
											 _CPTC("ComposedAxis::labelCompensation_write_cb"));
		}
		catch(...)
		{
			ERROR_STREAM << "Failed to set offset" << std::endl;
			THROW_DEVFAILED(
								_CPTC("DEVICE_ERROR"), 
								_CPTC("Failed to set offset - Caught unknown exception!"), 
								_CPTC("ComposedAxis::labelCompensation_write_cb")); 
		}
	}
	else
	{
		ERROR_STREAM << "ComposedAxis::labelCompensation_write_cb attribute label " << label << " doesn't exists ! " << endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to set offset - Caught DevFailed!"), 
							_CPTC("ComposedAxis::labelCompensation_write_cb")); 
	}
}

// ============================================================================
// ComposedAxis::init_axis_ref_pos
// ============================================================================
void ComposedAxis::init_axis_ref_pos()
{
	DEBUG_STREAM << "ComposedAxis::init_axis_ref_pos entering... "<< endl;

	if (this->get_state() == Tango::MOVING)
	{
		THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
			_CPTC("Start position initialization process is forbidden when the state is MOVING !"), 
			_CPTC("ComposedAxis::init_axis_ref_pos")); 
	}

	CHECK_CTRL_CMD();

#if defined (_SIMULATION_)
	CHECK_CTRL_CTL();
#endif

	try
  {
		this->m_cmdInterface->axis_init_ref_pos(m_cs_id);

#if defined (_SIMULATION_)
		this->m_ctlDataInterface->axis_init_ref_pos(m_cs_id);
#endif
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "ComposedAxis::init_axis_ref_pos caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to axis init ref pos - Caught DevFailed!"), 
                     _CPTC("ComposedAxis::init_axis_ref_pos"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to axis init ref pos" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to axis init ref pos - Caught unknown exception!"), 
							_CPTC("ComposedAxis::init_axis_ref_pos")); 
  }
}

// ============================================================================
// ComposedAxis::getUserUnitValue
// ============================================================================
double ComposedAxis::getUserUnitValue(std::string user_unit)
{
  //Return numerical value for a string unit...
  if (user_unit == kUNIT_M_NAME){
    return kUNIT_M;
  }
  if (user_unit == kUNIT_MM_NAME){
    return kUNIT_MM;
  }
  if (user_unit == kUNIT_UM_NAME){
    return kUNIT_UM;
  }
  if (user_unit == kUNIT_NM_NAME){
    return kUNIT_NM;
  }
  if (user_unit == kUNIT_RAD_NAME){
    return kUNIT_RAD;
  }
  if (user_unit == kUNIT_MRAD_NAME){
    return kUNIT_MRAD;
  }
  if (user_unit == kUNIT_DEG_NAME){
    return kUNIT_DEG;
  }
  if (user_unit == kUNIT_MDEG_NAME){
    return kUNIT_MDEG;
  }
  else
  {
    return 1.0;
  }
}

// ============================================================================
// ComposedAxis::compensation_enabled
// ============================================================================
bool ComposedAxis::compensation_enabled()
{
  // check if at least one axis of the composed axis has compensation enabled
  bool enabled = false;

	for (std::map<std::string, sgonpmaclib::T_axis_id>::iterator itAxis = this->m_local_axes.begin(); 
       itAxis != this->m_local_axes.end(); 
       ++itAxis)
	{
    if (this->m_stdDataInterface->get_compensation((itAxis->second)))
    {
      enabled = true;
      break;
    }
  }
  
  return (enabled);
}

} // namespace sgonppmac_ns



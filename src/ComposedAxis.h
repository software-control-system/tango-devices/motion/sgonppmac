//=============================================================================
// ComposedAxis.h
//=============================================================================
// class.............ComposedAxis
// original author...F. THIAM
//=============================================================================

#ifndef _COMPOSED_AXIS_H_
#define _COMPOSED_AXIS_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>

#include "AxisInterface.h"


namespace sgonppmac_ns {



// ============================================================================
// class: ComposedAxis
// ============================================================================
class ComposedAxis : public AxisInterface
{

public:

	
  // Constructor.
  ComposedAxis(Tango::DeviceImpl * hostDevice);

  // Destructor.
  virtual ~ComposedAxis();
	
  //---------------------------------------------------------------------------
  // Controller management
  //---------------------------------------------------------------------------

	// Set the axis initialization data
	virtual void set_axis_init_data(AxisInitData &initData );
	
	// Create and initialize the dynamic params
	virtual void init_dynamic_params();

	// Remove the dynamic params
	virtual void remove_dynamic_params();

	// Declare axis list
	// @param axisDefinition   (depend of type axis) list of Cs:Axis or Cs:Axis:Label.
	virtual void init_axis( vector<string>	axisDefinition);

	// Undeclare axis list
	virtual void undeclare_axes();

	// Freeze attribute setter & getter
	virtual bool get_freeze();
  void set_freeze(bool value);

	// Callback for read position <label>
	void label_read_cb (string label, Tango::Attribute &attr);

	// Callback for write position <label>
  void label_write_cb (string label, Tango::DevDouble value, bool bMemorized=true);

	// Callback for read offset <label>
	void labelOffset_read_cb (string label, Tango::Attribute &attr);

	// Callback for write offset <label>
  void labelOffset_write_cb (string label, Tango::DevDouble value, bool bMemorized=true);
  
  // Callback for read offset <label>
	void labelCompensation_read_cb (string label, Tango::Attribute &attr);

	// Callback for write offset <label>
  void labelCompensation_write_cb (string label, Tango::DevBoolean value, bool bMemorized=true);
  
	// Get the labels list associated to the composed axis
	void getLabels(std::vector<std::string> &listLabels);
  
  // starts initialization on axes
	void init_axis_ref_pos();
	
	//- Attach dynamic attributes to the device
	void attach_dynamic_attributes_to_device(void);

	//- Detach dynamic attributes from the device
	void detach_dynamic_attributes_from_device(void);

	//- Create dynamic attribute position for <label>
	void create_attr_label(std::string label);

	//- Create dynamic attribute offset for <label>
	void create_attr_labelOffset(std::string label);
  
  //- Create dynamic attribute offset fo <label>CompensationEnabled
  void create_attr_labelCompensationEnabled(std::string label);
  
  //- get user unit as a value
  double getUserUnitValue(std::string user_unit);
  
  // Checks if at least one compensation table is enabled.
  // @return True = at least ne compensation table enabled, false otherwise.
  bool compensation_enabled();
  
private : 
  
  //- Function to reaply write part label values before unfreeze
  void apply_write_label_part();
	
	//- Freeze flag
	bool m_freeze;

	// map of <label>, <axis>
	std::map<std::string, sgonpmaclib::T_axis_id> m_local_axes;
	
	//- Axes parameters
	std::map<std::string, AxisParams> m_axesParams;
	  
	//- Vector of dynamic attributes
	std::vector<Tango::Attr *> _dynamic_attribute_axes;

};

} // namespace sgonppmac_ns

#endif // _COMPOSED_AXIS_H_

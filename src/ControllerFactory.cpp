//=============================================================================
// ControllerFactory.cpp
//=============================================================================
// class.............ControllerFactory
// original author...F. THIAM
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <sstream>
#include "ControllerFactory.h"

// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================


namespace sgonppmac_ns {

	// ============================================================================
// SINGLETON
// ============================================================================
ControllerFactory * ControllerFactory::singleton = 0;

// ============================================================================
// ControllerFactory::init <STATIC MEMBER>
// ============================================================================
void ControllerFactory::init (PowerPMACConfig &cfg) 
{
  //- already instanciated?
  if (ControllerFactory::singleton)
    return;

  try
  {
    
    
  	//- actual instanciation
    ControllerFactory::singleton = new ControllerFactory(cfg);
    
    //std::cout<<"ControllerFactory::init singleton created...."<<std::endl;
    if (ControllerFactory::singleton == 0)
      throw std::bad_alloc();
  }
  catch (const std::bad_alloc&)
  {
    THROW_DEVFAILED(_CPTC("OUT_OF_MEMORY"),
                    _CPTC("ControllerFactory::singleton instanciation failed"),
                    _CPTC("ControllerFactory::init"));	
  }
  catch (...)
  {
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("unknown exception caught while while trying to instanciate the ControllerFactory::singleton"),
                    _CPTC("ControllerFactory::init"));
  }
}

// ============================================================================
// ControllerFactory::close  <STATIC MEMBER>
// ============================================================================
void ControllerFactory::close ()
{
  if (! ControllerFactory::singleton)
    return;

  try
  {
    //- release the <ControllerFactory> singleton
    SAFE_DELETE_PTR(ControllerFactory::singleton);
  }
  catch (Tango::DevFailed& df)
  {
    RETHROW_DEVFAILED(df,
                      _CPTC("SOFTWARE_ERROR"),
                      _CPTC("Tango exception caught while trying to shutdown the ControllerFactory::singleton"),
                      _CPTC("ControllerFactory::close"));
  }
  catch (...)
  {
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("unknown exception caught while while trying to shutdown the ControllerFactory::singleton"),
                    _CPTC("ControllerFactory::close"));
  }
}

// ============================================================================
// ControllerFactory::ControllerFactory
// ============================================================================
ControllerFactory::ControllerFactory (PowerPMACConfig &cfg)
  : m_cfg(cfg), m_cmdInterface(NULL), m_stdDataInterface(NULL), m_ctlDataInterface(NULL)
{
	//m_cfg.dump();
}

// ============================================================================
// ControllerFactory::~ControllerFactory
// ============================================================================
ControllerFactory::~ControllerFactory (void)
{
		//- Exit the Cmd ControlerInterface
	 if(m_cmdInterface) 
	 {
			m_cmdInterface->exit();
			m_cmdInterface = NULL;
	 }

	//- Exit the Std Data ControlerInterface
	if(m_stdDataInterface)
	{
			m_stdDataInterface->exit();
			m_stdDataInterface = NULL;
	}

	//- Exit the Ctl Data ControlerInterface
	if(m_ctlDataInterface)
	{
			m_ctlDataInterface->exit();
			m_ctlDataInterface = NULL;
	}
}

// ============================================================================
// ControllerFactory::get_cmd_interface
// ============================================================================
ControllerInterface *ControllerFactory::get_cmd_interface()
{
	if (m_cmdInterface) 
	{
    return m_cmdInterface;
	}

	m_cfg.dump();

//	- allocate the ControllerInterface for Cmd
  try
  {
		m_cmdInterface = new ControllerInterface(m_cfg.hostDevice);

    if (m_cmdInterface == 0)
      throw std::bad_alloc();
  }
  catch (std::bad_alloc&)
  {
     //std::cout<<"ControllerFactory::get_cmd_interface() :		new ControllerInterface(m_cfg.hostDevice) bad alloc"<<std::endl;

    THROW_DEVFAILED(_CPTC("OUT_OF_MEMORY"),
                    _CPTC("ControllerInterface instanciation failed"),
                    _CPTC("ControllerFactory::get_cmd_interface"));
  }
  catch (Tango::DevFailed& df)
  {
     //std::cout<<"ControllerFactory::get_cmd_interface() :		new ControllerInterface(m_cfg.hostDevice) ft"<<std::endl;

    RETHROW_DEVFAILED(df,
                      _CPTC("SOFTWARE_ERROR"),
                      _CPTC("Tango exception caught while trying to instanciate a ControllerInterface"),
                      _CPTC("ControllerFactory::get_cmd_interface"));
  }
  catch (...)
  {
    //std::cout<<"ControllerFactory::get_cmd_interface() :		new ControllerInterface(m_cfg.hostDevice) ..."<<std::endl;

    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("unknown exception caught while trying to instanciate a ControllerInterface"),
                    _CPTC("ControllerFactory::get_cmd_interface")); 
  }

	//init the Cmd ControllerInterface
	//--------------------------------------------
	try
	{
		m_cmdInterface->init_interface(m_cfg);
	}
	catch (Tango::DevFailed & df)
	{
    //std::cout<<"ControllerFactory::get_cmd_interface() :		m_cmdInterface->init_interface(m_cfg) DF"<<std::endl;
		RETHROW_DEVFAILED(df,
											_CPTC("DEVICE_ERROR"),
											_CPTC("Tango exception caught while trying to init a ControllerInterface"), 
											_CPTC("ControllerFactory::get_cmd_interface"));
	}
	catch (...)
	{
    //std::cout<<"ControllerFactory::get_cmd_interface() :		m_cmdInterface->init_interface(m_cfg) ..."<<std::endl;
		THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("unknown exception caught while trying to init a ControllerInterface"),
                    _CPTC("ControllerFactory::get_cmd_interface")); 
	}

	//- GO for ControllerInterface
	try
	{
		m_cmdInterface->go();
	}
	catch (Tango::DevFailed & df)
	{
    //std::cout<<"ControllerFactory::get_cmd_interface() :		m_cmdInterface->go DF"<<std::endl;
		RETHROW_DEVFAILED(df,
											_CPTC("DEVICE_ERROR"),
											_CPTC("Tango exception caught while trying to go for cmd ControllerInterface"), 
											_CPTC("ControllerFactory::get_cmd_interface"));
	}
	catch (...)
	{
    //std::cout<<"ControllerFactory::get_cmd_interface() :		m_cmdInterface->go ...."<<std::endl;
		THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("unknown exception caught while trying to go for cmd ControllerInterface"),
                    _CPTC("ControllerFactory::get_cmd_interface")); 
	}

	return m_cmdInterface;
}

// ============================================================================
// ControllerFactory::get_std_data_interface
// ============================================================================
ControllerInterface *ControllerFactory::get_std_data_interface()
{
	if (m_stdDataInterface) 
	{
    return m_stdDataInterface;
	}

	//- allocate the ControllerInterface or ControllerInterface_std for Cmd, depend of std period
  try
  {
		if (m_cfg.stdDataPollingPeriod == 0) 
		{
			m_stdDataInterface = new ControllerInterface(m_cfg.hostDevice);
		} 
		else
		{
			m_stdDataInterface = new ControllerInterface_std(m_cfg.hostDevice);
		}

    if (m_stdDataInterface == 0)
      throw std::bad_alloc();
  }
  catch (std::bad_alloc&)
  {
    THROW_DEVFAILED(_CPTC("OUT_OF_MEMORY"),
                    _CPTC("ControllerInterface std instanciation failed"),
                    _CPTC("ControllerFactory::get_std_data_interface"));
  }
  catch (Tango::DevFailed& df)
  {
    RETHROW_DEVFAILED(df,
                      _CPTC("SOFTWARE_ERROR"),
                      _CPTC("Tango exception caught while trying to instanciate a ControllerInterface std"),
                      _CPTC("ControllerFactory::get_std_data_interface"));
  }
  catch (...)
  {
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("unknown exception caught while trying to instanciate a ControllerInterface std"),
                    _CPTC("ControllerFactory::get_std_data_interface")); 
  }

	// init the std ControllerInterface
	//--------------------------------------------
	try
	{
		m_stdDataInterface->init_interface(m_cfg);
	}
	catch (Tango::DevFailed & df)
	{
		RETHROW_DEVFAILED(df,
											_CPTC("DEVICE_ERROR"),
											_CPTC("Tango exception caught while trying to initialize a ControllerInterface"), 
											_CPTC("ControllerFactory::get_std_data_interface"));
	}
	catch (...)
	{
		THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("Unknown exception caught while trying to initialize a ControllerInterface"),
                    _CPTC("ControllerFactory::get_std_data_interface")); 
	}

	
	// Check polling period
	if (m_cfg.stdDataPollingPeriod != 0) 
	{
		// Set the periodic msg period for ControllerInterface_std
		m_stdDataInterface->set_periodic_msg_period((size_t)m_cfg.stdDataPollingPeriod);
	  m_stdDataInterface->enable_periodic_msg(true);
	}

	//- GO for ControllerInterface
	try
	{
		m_stdDataInterface->go();
	}
	catch (Tango::DevFailed & df)
	{
		RETHROW_DEVFAILED(df,
											_CPTC("DEVICE_ERROR"),
											_CPTC("Tango exception caught while trying to go for std ControllerInterface"), 
											_CPTC("ControllerFactory::get_std_data_interface"));
	}
	catch (...)
	{
		THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("unknown exception caught while trying to go for std ControllerInterface"),
                    _CPTC("ControllerFactory::get_std_data_interface")); 
	}

	return m_stdDataInterface;
}

// ============================================================================
// ControllerFactory::get_ctl_data_interface
// ============================================================================
ControllerInterface *ControllerFactory::get_ctl_data_interface()
{
	if(m_ctlDataInterface) 
	{
    return m_ctlDataInterface;
	}

	//- allocate the ControllerInterface or ControllerInterface_ctl for Cmd, depend of ctl period
  try
  {
		if (m_cfg.criticalDataPollingPeriod == 0) 
		{
			m_ctlDataInterface = new ControllerInterface(m_cfg.hostDevice);
		} 
		else
		{
			m_ctlDataInterface = new ControllerInterface_ctl(m_cfg.hostDevice);
		}

    if (m_ctlDataInterface == 0)
      throw std::bad_alloc();
  }
  catch (std::bad_alloc&)
  {
    THROW_DEVFAILED(_CPTC("OUT_OF_MEMORY"),
                    _CPTC("ControllerInterface ctl instanciation failed"),
                    _CPTC("ControllerFactory::get_ctl_data_interface"));
  }
  catch (Tango::DevFailed& df)
  {
    RETHROW_DEVFAILED(df,
                      _CPTC("SOFTWARE_ERROR"),
                      _CPTC("Tango exception caught while trying to instanciate a ControllerInterface ctl"),
                      _CPTC("ControllerFactory::get_ctl_data_interface"));
  }
  catch (...)
  {
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("unknown exception caught while trying to instanciate a ControllerInterface ctl"),
                    _CPTC("ControllerFactory::get_ctl_data_interface")); 
  }

	// init the ctl ControllerInterface
	//--------------------------------------------
	try
	{
		m_ctlDataInterface->init_interface(m_cfg);
	}
	catch (Tango::DevFailed & df)
	{
		RETHROW_DEVFAILED(df,
											_CPTC("DEVICE_ERROR"),
											_CPTC("Tango exception caught while trying to init a ControllerInterface"), 
											_CPTC("ControllerFactory::get_ctl_data_interface"));
	}
	catch (...)
	{
		THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("unknown exception caught while trying to init a ControllerInterface"),
                    _CPTC("ControllerFactory::get_ctl_data_interface")); 
	}

	// Check polling period
	if (m_cfg.criticalDataPollingPeriod != 0) 
	{
		// Set the periodic msg period for ControllerInterface_ctl
		m_ctlDataInterface->set_periodic_msg_period((size_t)m_cfg.criticalDataPollingPeriod);
	  m_ctlDataInterface->enable_periodic_msg(true);
	}

	//- GO for ControllerInterface
	try
	{
		m_ctlDataInterface->go();
	}
	catch (Tango::DevFailed & df)
	{
		RETHROW_DEVFAILED(df,
											_CPTC("DEVICE_ERROR"),
											_CPTC("Tango exception caught while trying to go for std ControllerInterface"), 
											_CPTC("ControllerFactory::get_std_data_interface"));
	}
	catch (...)
	{
		THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("unknown exception caught while trying to go for std ControllerInterface"),
                    _CPTC("ControllerFactory::get_std_data_interface")); 
	}

	return m_ctlDataInterface;
}

// ============================================================================
// ControllerFactory::getNbOfComposedAxes
// ============================================================================
Tango::DevUShort ControllerFactory::getNbOfComposedAxes()
{
	return m_cfg.nbOfComposedAxes;
}

} // namespace sgonppmac_ns



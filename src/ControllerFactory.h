//=============================================================================
// ControllerFactory.h
//=============================================================================
// class.............ControllerFactory
// original author...F. THIAM
//=============================================================================

#ifndef _CONTROLLER_FACTORY_H_
#define _CONTROLLER_FACTORY_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include "SGonPPMACTypesAndConsts.h"
#include "ControllerInterface.h"
#include "ControllerInterface_std.h"
#include "ControllerInterface_ctl.h"


namespace sgonppmac_ns {


// ============================================================================
// class: ControllerFactory
// ============================================================================
class ControllerFactory
{

public:

	//----------------------------------------
  // SINGLETON 
  //----------------------------------------
  static inline ControllerFactory * instance ()
  {
    if (! ControllerFactory::singleton)
        THROW_DEVFAILED(_CPTC("SOFTWARE_ERROR"),
                        _CPTC("unexpected NULL pointer [ControllerFactory singleton not properly initialized]"),
                        _CPTC("ControllerFactory::instance")); 
    return ControllerFactory::singleton;
  }

  //- singleton instanciation --------------
  static void init (PowerPMACConfig &cfg);

  //- singleton release --------------------
  static void close ();

	//- get command ControllerInterface
	ControllerInterface *get_cmd_interface();

	//- get std ControllerInterface (polling period depend)
	ControllerInterface *get_std_data_interface();

	//- get ctl ControllerInterface (polling period depend)
	ControllerInterface *get_ctl_data_interface();

	//- get number of deployed instances of composed axes
	Tango::DevUShort getNbOfComposedAxes();

	
private:
  //- ctor -------------
  ControllerFactory (PowerPMACConfig &cfg);
	
  //- dtor -------------
  virtual ~ControllerFactory ();

  //- the singleton
  static ControllerFactory * singleton;

	//- configuration
	PowerPMACConfig m_cfg;

	//- Cmd ControlerInterface
	ControllerInterface * m_cmdInterface;

	//- Std Data ControlerInterface
	ControllerInterface * m_stdDataInterface;

	//- Ctl Data ControlerInterface
	ControllerInterface * m_ctlDataInterface;
	
};

} // namespace sgonppmac_ns

#endif // _CONTROLLER_FACTORY_H_

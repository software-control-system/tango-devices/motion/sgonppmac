//=============================================================================
// ControllerInterface.cpp
//=============================================================================
// class.............Controler Interface (Default implementation synchronous task)
//                   Serialize user request
// original author...F. THIAM
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <sstream>
#include "ControllerInterface.h"

// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================
#define kRESET_MSG (yat::FIRST_USER_MSG + 1000)
#define kREBOOT_MSG (yat::FIRST_USER_MSG + 1001)
#define kABORT_MSG (yat::FIRST_USER_MSG + 1002)
#define kEXEC_LOW_LEVEL_MSG (yat::FIRST_USER_MSG + 1003)
#define kMOTOR_ON_MSG (yat::FIRST_USER_MSG + 1004)
#define kMOTOR_OFF_MSG (yat::FIRST_USER_MSG + 1005)
#define kAXIS_STOP_MSG (yat::FIRST_USER_MSG + 1006)
#define kSET_POSITION_MSG (yat::FIRST_USER_MSG + 1008)
#define kAPPLY_POSITION_MSG (yat::FIRST_USER_MSG + 1009)
#define kSET_OFFSET_MSG (yat::FIRST_USER_MSG + 1010)
#define kSET_COMPENSASION_MSG (yat::FIRST_USER_MSG + 1011)
#define kSET_ACCELERATION_MSG (yat::FIRST_USER_MSG + 1015)
#define kSET_DECELERATION_MSG (yat::FIRST_USER_MSG + 1016)
#define kSET_VELOCITY_MSG (yat::FIRST_USER_MSG + 1017)
#define kSET_MOVING_MODE_MSG (yat::FIRST_USER_MSG + 1018)
#define kAXIS_INIT_REF_POS_MSG (yat::FIRST_USER_MSG + 1019)
#define kMOTOR_OMEGA_OFF_MSG (yat::FIRST_USER_MSG + 1020)
#define kMOTOR_OTHER_OFF_MSG (yat::FIRST_USER_MSG + 1021)
#define kCS_START_MP_MSG (yat::FIRST_USER_MSG + 1022)
#define kCS_PAUSE_MP_MSG (yat::FIRST_USER_MSG + 1023)
#define kCS_RESUME_MP_MSG (yat::FIRST_USER_MSG + 1024)

//-------------------------------------------------------------
#define kGET_CONNECTION_STATE_MSG (yat::FIRST_USER_MSG + 1025)
#define kGET_BOX_STATUS_MSG (yat::FIRST_USER_MSG + 1026)
#define kGET_FIRMWARE_VERS_MSG (yat::FIRST_USER_MSG + 1027)
#define kGET_UCODE_VERS_MSG (yat::FIRST_USER_MSG + 1029)
#define kGET_CPU_TEMP_MSG (yat::FIRST_USER_MSG + 1030)
#define kGET_CPU_RUNNING_TIME_MSG (yat::FIRST_USER_MSG + 1031)
#define kGET_CPU_PHASE_USAGE_MSG (yat::FIRST_USER_MSG + 1032)
#define kGET_CPU_SERVO_USAGE_MSG (yat::FIRST_USER_MSG + 1033)
#define kGET_CPU_RTI_USAGE_MSG (yat::FIRST_USER_MSG + 1034)
#define kGET_CPU_BG_USAGE_MSG (yat::FIRST_USER_MSG + 1035)
#define kGET_POSITION_MSG (yat::FIRST_USER_MSG + 1036)
#define kGET_OFFSET_MSG (yat::FIRST_USER_MSG + 1037)
#define kGET_COMPENSASION_MSG (yat::FIRST_USER_MSG + 1038)
#define kGET_COMPENSATION_TABLE_VALIDITY_MSG (yat::FIRST_USER_MSG + 1039)
#define kGET_STATUS_DETAILS_MSG (yat::FIRST_USER_MSG + 1040)
#define kGET_REF_STATUS_MSG (yat::FIRST_USER_MSG + 1041)
#define kGET_ACCELERATION_MSG (yat::FIRST_USER_MSG + 1042)
#define kGET_DECELERATION_MSG (yat::FIRST_USER_MSG + 1043)
#define kGET_VELOCITY_MSG (yat::FIRST_USER_MSG + 1044)
#define kGET_MOVING_MODE_MSG (yat::FIRST_USER_MSG + 1045)
#define kCS_GET_STATE_MP_MSG (yat::FIRST_USER_MSG + 1046)
#define kGET_SCURVE_MSG (yat::FIRST_USER_MSG + 1047)
#define kSET_SCURVE_MSG (yat::FIRST_USER_MSG + 1048)
#define kGET_MOTORS_STATUS_MSG (yat::FIRST_USER_MSG + 1049)


// Command Timeout
#define kDEFAULT_TIMEOUT_CMD 3000


namespace sgonppmac_ns {

// ============================================================================
//- Check hw interface pointer:
#define CHECK_HW_INTERFACE \
  if (!m_hw_interface) \
  { \
     THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
                     _CPTC("request aborted - the controller isn't accessible "), \
                     _CPTC("ControllerInterface::CHECK_HW_INTERFACE()")); \
  }
// ============================================================================

// ============================================================================
// ControllerInterface::ControllerInterface
// ============================================================================
ControllerInterface::ControllerInterface(Tango::DeviceImpl * hostDevice)
	: yat4tango::DeviceTask(hostDevice)
{
  //DEBUG_STREAM << "ControllerInterface constructor entering..." << std::endl;
  m_hw_interface = NULL;
	this->enable_timeout_msg(false);
	this->enable_periodic_msg(false);
	
	//m_axisState = sgonpmaclib::CS_STATE_UNDEF;
	m_isConnected = false;
	m_firmwareVers = "";
  m_brickLvVers = "";
	m_uCodeVers = "";
  m_cpuTemp = 0.0;
	m_cpuRunningTime = 0.0;
	m_cpuPhaseUsage = 0.0;
	m_cpuServoUsage = 0.0;
	m_cpuRtiUsage = 0.0;
	m_cpuBgUsage = 0.0;

	// Create HW Access
  try
  {
    m_hw_interface = new HwAccess(hostDevice);
  }
  catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to create HwAccess controler"), 
                     _CPTC("ControllerInterface::ControllerInterface"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"),
							_CPTC("Failed to create HwAccess controler"), 
              _CPTC("ControllerInterface::ControllerInterface"));
  }
}

// ============================================================================
// ControllerInterface::~ControllerInterface
// ============================================================================
ControllerInterface::~ControllerInterface ()
{
  this->enable_periodic_msg(false);
	this->m_csList.clear();
	this->m_axisList.clear();
	this->m_axisMap.clear();
	this->m_csMap.clear();

	// Remove HW Access
	if(m_hw_interface) 
	{
		delete m_hw_interface;
		m_hw_interface = NULL;
	}
}

// ============================================================================
// ControllerInterface::process_message
// ============================================================================
void ControllerInterface::process_message (yat::Message& msg)
  throw (Tango::DevFailed)
{
	//- handle msg
	switch (msg.type())
	{
		//- THREAD_INIT ----------------------
	case yat::TASK_INIT:
		{
			//DEBUG_STREAM << "ControllerInterface::handle_message::THREAD_INIT::thread is starting up" << std::endl;
			
		} 
		break;

		//- THREAD_EXIT ----------------------
	case yat::TASK_EXIT:
		{
			//DEBUG_STREAM << "ControllerInterface::handle_message::THREAD_EXIT::thread is quitting" << std::endl;
		}
		break;

		//- THREAD_PERIODIC ------------------
	case yat::TASK_PERIODIC:
		{
			//DEBUG_STREAM << "ControllerInterface::handle_message::THREAD_PERIODIC" << std::endl;
			this->periodic_job_i();
		}
		break;

		//- THREAD_TIMEOUT -------------------
	case yat::TASK_TIMEOUT:
		{
			//- not used in this example
		}
		break;

	case kRESET_MSG:
		{
			this->reset_i();
		}
		break;

	case kREBOOT_MSG:
		{
			this->reboot_i();
		}
		break;

	case kABORT_MSG:
		{
			this->abort_i();
		}
		break;

	case kEXEC_LOW_LEVEL_MSG:
		{
				std::string out_str;
        std::string & in_str_ref = msg.get_data<std::string>();
				this->exec_low_level_cmd_i(in_str_ref, out_str);
        in_str_ref = out_str;
		}
		break;

	case kMOTOR_ON_MSG:
		{
			//- extract T_cs_id from message
      sgonpmaclib::T_cs_id id = msg.get_data<sgonpmaclib::T_cs_id>();
			this->motor_on_i(id);
		}
		break;

	case kMOTOR_OFF_MSG:
		{
			//- extract T_cs_id from message
      sgonpmaclib::T_cs_id id = msg.get_data<sgonpmaclib::T_cs_id>();
			this->motor_off_i(id);
		}
		break;
    
	case kMOTOR_OMEGA_OFF_MSG:
		{
			//- extract T_cs_id from message
      sgonpmaclib::T_cs_id id = msg.get_data<sgonpmaclib::T_cs_id>();
			this->motor_omega_off_i(id);
		}
		break;
  
  case kMOTOR_OTHER_OFF_MSG:
		{
			//- extract T_cs_id from message
      sgonpmaclib::T_cs_id id = msg.get_data<sgonpmaclib::T_cs_id>();
			this->motor_other_off_i(id);
		}
		break;

	case kAXIS_STOP_MSG:
		{
			//- extract T_cs_id from message
      sgonpmaclib::T_cs_id id = msg.get_data<sgonpmaclib::T_cs_id>();
			this->axis_stop_i(id);
		}
		break;
	case kSET_POSITION_MSG:
		{
			//- extract ParamAxisValueBool from message
			ParamAxisValueBool paramSetPosition = msg.get_data<ParamAxisValueBool>();
			this->set_position_i(paramSetPosition.axisParams.idAxis, paramSetPosition.axisParams.valueDouble, paramSetPosition.bValue );
		}
		break;

	case kAPPLY_POSITION_MSG:
		{
			//- extract T_cs_id from message
      sgonpmaclib::T_cs_id id = msg.get_data<sgonpmaclib::T_cs_id>();
			this->apply_position_i(id);
		}
		break;

	case kSET_OFFSET_MSG:
		{
			//- extract ParamAxisValueMsg from message
			ParamAxisValueMsg paramSetOffset = msg.get_data<ParamAxisValueMsg>();
			this->set_offset_i(paramSetOffset.idAxis, paramSetOffset.valueDouble);
		}
		break;
    
  case kSET_COMPENSASION_MSG:
		{
			//- extract ParamAxisValueMsg from message
			ParamAxisValueMsg paramSetOffset = msg.get_data<ParamAxisValueMsg>();
			this->set_compensation_i(paramSetOffset.idAxis, paramSetOffset.valueBool);
		}
		break;

	case kSET_ACCELERATION_MSG:
		{
			//- extract ParamCsValueMsg from message
			ParamCsValueMsg paramSetAcceleration = msg.get_data<ParamCsValueMsg>();
			this->set_acceleration_i(paramSetAcceleration.idCs, paramSetAcceleration.valueDouble);
		}
		break;
    
  case kSET_SCURVE_MSG:
		{
			//- extract ParamCsValueMsg from message
			ParamCsValueMsg paramSetAcceleration = msg.get_data<ParamCsValueMsg>();
			this->set_scurve_i(paramSetAcceleration.idCs, paramSetAcceleration.valueDouble);
		}
		break;

	case kSET_DECELERATION_MSG:
		{
			//- extract ParamCsValueMsg from message
			ParamCsValueMsg paramSetDeceleration = msg.get_data<ParamCsValueMsg>();
			this->set_deceleration_i(paramSetDeceleration.idCs, paramSetDeceleration.valueDouble);
		}
		break;

	case kSET_VELOCITY_MSG:
		{
			//- extract ParamCsValueMsg from message
			ParamCsValueMsg paramSetVelocity = msg.get_data<ParamCsValueMsg>();
			this->set_velocity_i(paramSetVelocity.idCs, paramSetVelocity.valueDouble);
		}
		break;

	case kGET_CONNECTION_STATE_MSG:
		{
			bool isConnected = this->get_connection_state_i();
      msg.attach_data(isConnected);
		}
		break;
	case kGET_BOX_STATUS_MSG:
		{
			sgonpmaclib::T_ppmac_brick_status status = this->get_box_status_i();
      msg.attach_data(status);
		}
		break;
	case kGET_FIRMWARE_VERS_MSG:
		{
			std::string vers = this->get_firmware_vers_i();
      msg.attach_data(vers);
		}
		break;
	case kGET_UCODE_VERS_MSG:
		{
			std::string vers = this->get_ucode_vers_i();
      msg.attach_data(vers);
		}
		break;
	case kGET_CPU_TEMP_MSG:
		{
			double temp = this->get_cpu_temp_i();
      msg.attach_data(temp);
		}
		break;
	case kGET_CPU_RUNNING_TIME_MSG:
		{
			double cpu_time = this->get_cpu_running_time_i();
      msg.attach_data(cpu_time);
		}
		break;
	case kGET_CPU_PHASE_USAGE_MSG:
		{
			double cpu_usage = this->get_cpu_phase_usage_i();
      msg.attach_data(cpu_usage);
		}
		break;
	case kGET_CPU_SERVO_USAGE_MSG:
		{
			double cpu_usage = this->get_cpu_servo_usage_i();
      msg.attach_data(cpu_usage);
		}
		break;
	case kGET_CPU_RTI_USAGE_MSG:
		{
			double cpu_usage = this->get_cpu_rti_usage_i();
      msg.attach_data(cpu_usage);
		}
		break;
	case kGET_CPU_BG_USAGE_MSG:
		{
			double cpu_usage = this->get_cpu_bg_usage_i();
      msg.attach_data(cpu_usage);
		}
		break;
  case kGET_POSITION_MSG:
		{
			double position;
			sgonpmaclib::T_axis_id id = msg.get_data<sgonpmaclib::T_axis_id>();
			position = this->get_position_i(id);
			msg.attach_data(position);
		}
		break;
	case kGET_OFFSET_MSG:
		{
			double position;
			sgonpmaclib::T_axis_id id = msg.get_data<sgonpmaclib::T_axis_id>();
			position = this->get_offset_i(id);
			msg.attach_data(position);
		}
		break;
  case kGET_COMPENSASION_MSG:
		{
			bool compensation;
			sgonpmaclib::T_axis_id id = msg.get_data<sgonpmaclib::T_axis_id>();
			compensation = this->get_compensation_i(id);
			msg.attach_data(compensation);
		}
		break;
  case kGET_COMPENSATION_TABLE_VALIDITY_MSG:
		{
			bool compensationValid;
			sgonpmaclib::T_cs_id id = msg.get_data<sgonpmaclib::T_cs_id>();
			compensationValid = this->get_compens_table_validity_i(id);
			msg.attach_data(compensationValid);
		}
		break;
	case kGET_STATUS_DETAILS_MSG:
		{
 			sgonpmaclib::T_cs_status status;
			sgonpmaclib::T_cs_id id = msg.get_data<sgonpmaclib::T_cs_id>();
			status = this->get_status_details_i(id);
			msg.attach_data(status);
		}
		break;
	case kGET_MOTORS_STATUS_MSG:
		{
 			std::string status;
			sgonpmaclib::T_cs_id id = msg.get_data<sgonpmaclib::T_cs_id>();
			status = this->get_motors_status_i(id);
			msg.attach_data(status);
		}
		break;
  case kGET_REF_STATUS_MSG:
		{
 			sgonpmaclib::E_cs_referencing_status_t ref_status;
			sgonpmaclib::T_cs_id id = msg.get_data<sgonpmaclib::T_cs_id>();
			ref_status = this->get_ref_status_i(id);
			msg.attach_data(ref_status);
		}
		break;
    
		
	case kGET_ACCELERATION_MSG:
		{
			double accel;
			sgonpmaclib::T_cs_id id = msg.get_data<sgonpmaclib::T_cs_id>();
			accel = this->get_acceleration_i(id);
			msg.attach_data(accel);
		}
		break;
    
  case kGET_SCURVE_MSG:
		{
			double accelScurve;
			sgonpmaclib::T_cs_id id = msg.get_data<sgonpmaclib::T_cs_id>();
			accelScurve = this->get_scurve_i(id);
			msg.attach_data(accelScurve);
		}
  break;
    
	case kGET_DECELERATION_MSG:
		{
			double decel;
			sgonpmaclib::T_cs_id id = msg.get_data<sgonpmaclib::T_cs_id>();
			decel = this->get_deceleration_i(id);
			msg.attach_data(decel);
		}
		break;
	case kGET_VELOCITY_MSG:
		{
			double velocity;
			sgonpmaclib::T_cs_id id = msg.get_data<sgonpmaclib::T_cs_id>();
			velocity = this->get_velocity_i(id);
			msg.attach_data(velocity);
		}
		break;
	case kAXIS_INIT_REF_POS_MSG:
		{
			sgonpmaclib::T_cs_id id = msg.get_data<sgonpmaclib::T_cs_id>();
			this->axis_init_ref_pos_i(id);
		}
		break;
	
	case kGET_MOVING_MODE_MSG:
		{
			sgonpmaclib::E_moving_mode_t mv_mode;
			sgonpmaclib::T_cs_id id= msg.get_data<sgonpmaclib::T_cs_id>();
			mv_mode = this->get_cs_moving_mode_i(id);
			msg.attach_data(mv_mode);
		}
		break;
	case kSET_MOVING_MODE_MSG:
		{
			//- extract ParamCsValueMsg from message
			ParamCsModeMsg paramSetMovingMode = msg.get_data<ParamCsModeMsg>();
			this->set_cs_moving_mode_i(paramSetMovingMode.idCs, paramSetMovingMode.mv_mode);
		}
		break;
	case kCS_START_MP_MSG:
		{
			//- extract ParamCsValueMsg from message
			ParamCsValueMsg paramStartMP = msg.get_data<ParamCsValueMsg>();
			this->start_mp_i(paramStartMP.idCs, (int)paramStartMP.valueDouble);
		}
		break;
	case kCS_PAUSE_MP_MSG:
		{
			sgonpmaclib::T_cs_id id = msg.get_data<sgonpmaclib::T_cs_id>();
			this->pause_mp_i(id);
		}
		break;
	case kCS_RESUME_MP_MSG:
		{
      sgonpmaclib::T_cs_id id = msg.get_data<sgonpmaclib::T_cs_id>();
			this->resume_mp_i(id);
		}
		break;
	case kCS_GET_STATE_MP_MSG:
		{
			sgonpmaclib::E_mp_state_t st;
			sgonpmaclib::T_cs_id id = msg.get_data<sgonpmaclib::T_cs_id>();
			st = this->get_mp_state_i(id);
			msg.attach_data(st);
		}
		break;
		//- UNHANDLED MSG --------------------
	default:
		//DEBUG_STREAM << "ControllerInterface::handle_message::unhandled msg type received" << std::endl;
		break;
	}
}

// ============================================================================
// ControllerInterface::periodic_job_i
// ============================================================================
void ControllerInterface::periodic_job_i ()
{
	//- not used in synchronous mode
}

// ============================================================================
// ControllerInterface::open
// ============================================================================
void ControllerInterface::init_interface(PowerPMACConfig &cfg)
{
	CHECK_HW_INTERFACE;

	m_cfg = cfg;
  //DEBUG_STREAM << "ControllerInterface init_interface entering..." << std::endl;

	// open connection
  try
  {
    m_hw_interface->open(cfg.iPAddress, cfg.tCPPort);
  }
  catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Hardware connection NOK - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::init_interface"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Hardware connection NOK - Caught unknown exception!"), 
							_CPTC("ControllerInterface::init_interface")); 
  }
}

// ============================================================================
// ControllerInterface::set_axis_init_data
// ============================================================================
void ControllerInterface::set_axis_init_data(sgonpmaclib::T_axis_id id, AxisInitData &initData )
{
	// NOOP - FUNCTION DOESN'T DO ANYTHING IN SMARGON CASE.
}

// ============================================================================
// ControllerInterface::declare_axis
// ============================================================================
void ControllerInterface::declare_axis( std::vector<sgonpmaclib::T_axis_id> axis_list)
{
	yat::AutoMutex<> guard(ControllerInterface::m_lock);

	// for each axis_id in axis_list
	for (vector<sgonpmaclib::T_axis_id>::iterator i = axis_list.begin(); i != axis_list.end(); ++i)
	{
    // get axis_id 
		sgonpmaclib::T_axis_id axis_id = *i;
		
		try
		{
			// Init axis
			m_hw_interface->init_axis(axis_id);
			
			if (m_axisMap.count(axis_id) == 0) 
			{
				// Insert the axis_id in axisMap
				AxisParams default_axis;
				m_axisMap.insert(std::pair<sgonpmaclib::T_axis_id, AxisParams>(axis_id, default_axis));

				// Insert the axis_id in axisList
				m_axisList.push_back(axis_id);

				if (m_csMap.count(axis_id.cs) == 0) 
				{
					// Insert the cs_id in csMap
					CsParams default_cs;
					m_csMap.insert(std::pair<sgonpmaclib::T_cs_id, CsParams>(axis_id.cs, default_cs ));

					// Insert the cs_id in csList
					m_csList.push_back(axis_id.cs);
				}
			}

		}
		catch(Tango::DevFailed &e)
		{
			RETHROW_DEVFAILED(e,
											 _CPTC("DEVICE_ERROR"),
											 _CPTC("Failed to init axis - Caught DevFailed!"), 
											 _CPTC("ControllerInterface::declare_axis"));
		}
		catch(...)
		{
			 THROW_DEVFAILED(
								_CPTC("DEVICE_ERROR"), 
								_CPTC("Failed to init axis - Caught unknown exception!"), 
								_CPTC("ControllerInterface::declare_axis")); 
		}
	}
}

// ============================================================================
// ControllerInterface::undeclare_axis
// ============================================================================
void ControllerInterface::undeclare_axis( std::vector<sgonpmaclib::T_axis_id> axis_list)
{
	yat::AutoMutex<> guard(ControllerInterface::m_lock);

	// for each axis_id in axis_list
	for (vector<sgonpmaclib::T_axis_id>::iterator i = axis_list.begin(); i != axis_list.end(); ++i)
	{
    // get axis_id 
		sgonpmaclib::T_axis_id axis_id = *i;

		try
		{
			// Undeclare axis
			m_hw_interface->undeclare_axis(axis_id);
		}
		catch(Tango::DevFailed &e)
		{
			RETHROW_DEVFAILED(e,
											 _CPTC("DEVICE_ERROR"),
											 _CPTC("Failed to undeclare axis - Caught DevFailed!"), 
											 _CPTC("ControllerInterface::undeclare_axis"));
		}
		catch(...)
		{
			 THROW_DEVFAILED(
								_CPTC("DEVICE_ERROR"), 
								_CPTC("Failed to undeclare axis - Caught unknown exception!"), 
								_CPTC("ControllerInterface::undeclare_axis")); 
		}
		
	}
}

// ============================================================================
// ControllerInterface::reset
// ============================================================================
void ControllerInterface::reset()
{
	this->post(kRESET_MSG);
}

// ============================================================================
// ControllerInterface::reboot_os
// ============================================================================
void ControllerInterface::reboot()
{
  this->post(kREBOOT_MSG);
}

// ============================================================================
// ControllerInterface::abort
// ============================================================================
void ControllerInterface::abort()
{
	this->post(kABORT_MSG);
}

// ============================================================================
// ControllerInterface::send_low_level_cmd
// ============================================================================
void ControllerInterface::exec_low_level_cmd(std::string cmd, std::string &outParam)
{
	yat::Message * msg = yat::Message::allocate(kEXEC_LOW_LEVEL_MSG, DEFAULT_MSG_PRIORITY, true);

  try
  {
    msg->attach_data(cmd);
    
    this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  
    outParam = msg->get_data<std::string>();
  }
  catch (...)
  {
    msg->release();
    throw;
  }
     
  msg->release();
}

// ============================================================================
// ControllerInterface::exec_low_level_cmd_i
// ============================================================================
void ControllerInterface::exec_low_level_cmd_i(std::string cmd, std::string &outParam)
{
  CHECK_HW_INTERFACE;

	try 
  {
		m_hw_interface->send_low_level_cmd(cmd, outParam);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to send low level cmd - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::exec_low_level_cmd_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to send low level cmd - Caught unknown exception!"), 
							_CPTC("ControllerInterface::exec_low_level_cmd_i")); 
  }
}

// ============================================================================
// ControllerInterface::get_box_status
// ============================================================================
sgonpmaclib::T_ppmac_brick_status ControllerInterface::get_box_status()
{
	yat::Message * msg = yat::Message::allocate(kGET_BOX_STATUS_MSG, DEFAULT_MSG_PRIORITY, true);

  try
  {
		this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  }
  catch (...)
  {
    msg->release();
    throw;
  }

  sgonpmaclib::T_ppmac_brick_status status = msg->get_data<sgonpmaclib::T_ppmac_brick_status>();
  
  msg->release();
  
	return status;
}

// ============================================================================
// ControllerInterface::get_box_status_i
// ============================================================================
sgonpmaclib::T_ppmac_brick_status ControllerInterface::get_box_status_i()
{
	CHECK_HW_INTERFACE;

	sgonpmaclib::T_ppmac_brick_status status;
	try 
  {
		status = m_hw_interface->get_raw_status();
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get box status - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_box_status_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get box status - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_box_status_i")); 
  }

	{
    yat::AutoMutex<> guard(ControllerInterface::m_lock);
    m_boxStatus = status;
  }

	return status;
}

// ============================================================================
// ControllerInterface::get_connection_state
// ============================================================================
void ControllerInterface::get_connection_state(bool &isConnected)
{
	yat::Message * msg = yat::Message::allocate(kGET_CONNECTION_STATE_MSG, DEFAULT_MSG_PRIORITY, true);

  try
  {
		this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  }
  catch (...)
  {
    msg->release();
    throw;
  }

  isConnected = msg->get_data<bool>();
  
  msg->release();
}

// ============================================================================
// ControllerInterface::get_connection_state_i
// ============================================================================
bool ControllerInterface::get_connection_state_i()
{
	CHECK_HW_INTERFACE;

	bool isConnected;
	try 
  {
		m_hw_interface->get_connection_state(isConnected);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get connection state - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_connection_state_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get connection state - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_connection_state_i")); 
  }

	{
    yat::AutoMutex<> guard(ControllerInterface::m_lock);
    m_isConnected = isConnected;
  }

	return isConnected;
}

// ============================================================================
// ControllerInterface::get_firmware_vers
// ============================================================================
std::string ControllerInterface::get_firmware_vers()
{
	yat::Message * msg = yat::Message::allocate(kGET_FIRMWARE_VERS_MSG, DEFAULT_MSG_PRIORITY, true);

  try
  {
		this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  }
  catch (...)
  {
    msg->release();
    throw;
  }

	std::string vers = msg->get_data<std::string>();
  
  msg->release();

	return vers;
}

// ============================================================================
// ControllerInterface::get_firmware_vers_i
// ============================================================================
std::string ControllerInterface::get_firmware_vers_i()
{
	CHECK_HW_INTERFACE;

	std::string vers;
	try {
		vers = m_hw_interface->get_firmware_vers();
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get firmware version - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_firmware_vers_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get firmware version - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_firmware_vers_i")); 
  }

	{
    yat::AutoMutex<> guard(ControllerInterface::m_lock);
    m_firmwareVers = vers;
  }

	return vers;
}

// ============================================================================
// ControllerInterface::get_ucode_vers
// ============================================================================
std::string ControllerInterface::get_ucode_vers()
{
	yat::Message * msg = yat::Message::allocate(kGET_UCODE_VERS_MSG, DEFAULT_MSG_PRIORITY, true);

  try
  {
		this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  }
  catch (...)
  {
    msg->release();
    throw;
  }

	std::string vers = msg->get_data<std::string>();
  
  msg->release();

	return vers;
}

// ============================================================================
// ControllerInterface::get_ucode_vers_i
// ============================================================================
std::string ControllerInterface::get_ucode_vers_i()
{
	CHECK_HW_INTERFACE;

	std::string vers = "";
	try {
		vers = m_hw_interface->get_ucode_vers();
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get ucode version - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_ucode_vers_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get ucode version - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_ucode_vers_i")); 
  }

	{
    yat::AutoMutex<> guard(ControllerInterface::m_lock);
    m_uCodeVers = vers;
  }

	return vers;
}

// ============================================================================
// ControllerInterface::get_cpu_temp
// ============================================================================
double ControllerInterface::get_cpu_temp()
{
	yat::Message * msg = yat::Message::allocate(kGET_CPU_TEMP_MSG, DEFAULT_MSG_PRIORITY, true);

  try
  {
		this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  }
  catch (...)
  {
    msg->release();
    throw;
  }

	double temp = msg->get_data<double>();
  
  msg->release();

	return temp;
}

// ============================================================================
// ControllerInterface::get_cpu_temp_i
// ============================================================================
double ControllerInterface::get_cpu_temp_i()
{
	CHECK_HW_INTERFACE;

	double temp = yat::IEEE_NAN;
	try {
		temp = m_hw_interface->get_cpu_temperature();
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get cpu temperature - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_cpu_temp_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get cpu temperature - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_cpu_temp_i")); 
  }

	{
    yat::AutoMutex<> guard(ControllerInterface::m_lock);
    m_cpuTemp = temp;
  }

	return temp;
}

// ============================================================================
// ControllerInterface::get_cpu_running_time
// ============================================================================
double ControllerInterface::get_cpu_running_time()
{
	yat::Message * msg = yat::Message::allocate(kGET_CPU_RUNNING_TIME_MSG, DEFAULT_MSG_PRIORITY, true);

  try
  {
		this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  }
  catch (...)
  {
    msg->release();
    throw;
  }

	double cpu_time = msg->get_data<double>();
  
  msg->release();

	return cpu_time;
}

// ============================================================================
// ControllerInterface::get_cpu_running_time_i
// ============================================================================
double ControllerInterface::get_cpu_running_time_i()
{
	CHECK_HW_INTERFACE;

	double cpu_time = yat::IEEE_NAN;
	try {
		cpu_time = m_hw_interface->get_cpu_running_time();
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get cpu running time - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_cpu_running_time_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get cpu running time - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_cpu_running_time_i")); 
  }

	{
    yat::AutoMutex<> guard(ControllerInterface::m_lock);
    m_cpuRunningTime = cpu_time;
  }

	return cpu_time;
}

// ============================================================================
// ControllerInterface::get_cpu_phase_usage
// ============================================================================
double ControllerInterface::get_cpu_phase_usage()
{
	yat::Message * msg = yat::Message::allocate(kGET_CPU_PHASE_USAGE_MSG, DEFAULT_MSG_PRIORITY, true);

  try
  {
		this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  }
  catch (...)
  {
    msg->release();
    throw;
  }

	double cpu_usage = msg->get_data<double>();
  
  msg->release();

	return cpu_usage;
}

// ============================================================================
// ControllerInterface::get_cpu_phase_usage_i
// ============================================================================
double ControllerInterface::get_cpu_phase_usage_i()
{
	CHECK_HW_INTERFACE;

	double cpu_usage = yat::IEEE_NAN;
	try {
		cpu_usage = m_hw_interface->get_cpu_phase_usage();
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get cpu phase usage - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_cpu_phase_usage_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get cpu phase usage - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_cpu_phase_usage_i")); 
  }

	{
    yat::AutoMutex<> guard(ControllerInterface::m_lock);
    m_cpuPhaseUsage = cpu_usage;
  }

	return cpu_usage;
}

// ============================================================================
// ControllerInterface::get_cpu_servo_usage
// ============================================================================
double ControllerInterface::get_cpu_servo_usage()
{
	yat::Message * msg = yat::Message::allocate(kGET_CPU_SERVO_USAGE_MSG, DEFAULT_MSG_PRIORITY, true);

  try
  {
		this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  }
  catch (...)
  {
    msg->release();
    throw;
  }

	double cpu_usage = msg->get_data<double>();
  
  msg->release();

	return cpu_usage;
}

// ============================================================================
// ControllerInterface::get_cpu_servo_usage_i
// ============================================================================
double ControllerInterface::get_cpu_servo_usage_i()
{
	CHECK_HW_INTERFACE;

	double cpu_usage = yat::IEEE_NAN;
	try {
		cpu_usage = m_hw_interface->get_cpu_servo_usage();
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get cpu servo usage - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_cpu_servo_usage_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get cpu servo usage - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_cpu_servo_usage_i")); 
  }

	{
    yat::AutoMutex<> guard(ControllerInterface::m_lock);
    m_cpuServoUsage = cpu_usage;
  }

	return cpu_usage;
}

// ============================================================================
// ControllerInterface::get_cpu_rti_usage
// ============================================================================
double ControllerInterface::get_cpu_rti_usage()
{
	yat::Message * msg = yat::Message::allocate(kGET_CPU_RTI_USAGE_MSG, DEFAULT_MSG_PRIORITY, true);

  try
  {
		this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  }
  catch (...)
  {
    msg->release();
    throw;
  }

	double cpu_usage = msg->get_data<double>();
  
  msg->release();

	return cpu_usage;
}

// ============================================================================
// ControllerInterface::get_cpu_rti_usage_i
// ============================================================================
double ControllerInterface::get_cpu_rti_usage_i()
{
	CHECK_HW_INTERFACE;

	double cpu_usage = yat::IEEE_NAN;
	try {
		cpu_usage = m_hw_interface->get_cpu_rti_usage();
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get cpu rti usage - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_cpu_rti_usage_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get cpu rti usage - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_cpu_rti_usage_i")); 
  }

	{
    yat::AutoMutex<> guard(ControllerInterface::m_lock);
    m_cpuRtiUsage = cpu_usage;
  }

	return cpu_usage;
}

// ============================================================================
// ControllerInterface::get_cpu_bg_usage
// ============================================================================
double ControllerInterface::get_cpu_bg_usage()
{
	yat::Message * msg = yat::Message::allocate(kGET_CPU_BG_USAGE_MSG, DEFAULT_MSG_PRIORITY, true);

  try
  {
		this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  }
  catch (...)
  {
    msg->release();
    throw;
  }

	double cpu_usage = msg->get_data<double>();
  
  msg->release();

	return cpu_usage;
}

// ============================================================================
// ControllerInterface::get_cpu_bg_usage_i
// ============================================================================
double ControllerInterface::get_cpu_bg_usage_i()
{
	CHECK_HW_INTERFACE;

	double cpu_usage = yat::IEEE_NAN;
	try {
		cpu_usage = m_hw_interface->get_cpu_bg_usage();
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get cpu bg usage - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_cpu_bg_usage_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get cpu bg usage - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_cpu_bg_usage_i")); 
  }

	{
    yat::AutoMutex<> guard(ControllerInterface::m_lock);
    m_cpuBgUsage = cpu_usage;
  }

	return cpu_usage;
}

// ============================================================================
// ControllerInterface::motor_on
// ============================================================================
void ControllerInterface::motor_on(sgonpmaclib::T_cs_id id)
{
	this->post(kMOTOR_ON_MSG, id, kDEFAULT_POST_MSG_TMO);
}

// ============================================================================
// ControllerInterface::motor_off
// ============================================================================
void ControllerInterface::motor_off(sgonpmaclib::T_cs_id id)
{
	this->post(kMOTOR_OFF_MSG, id, kDEFAULT_POST_MSG_TMO);
}

// ============================================================================
// ControllerInterface::motor_omega_off
// ============================================================================
void ControllerInterface::motor_omega_off(sgonpmaclib::T_cs_id id)
{
	this->post(kMOTOR_OMEGA_OFF_MSG, id, kDEFAULT_POST_MSG_TMO);
}

// ============================================================================
// ControllerInterface::motor_other_off
// ============================================================================
void ControllerInterface::motor_other_off(sgonpmaclib::T_cs_id id)
{
	this->post(kMOTOR_OTHER_OFF_MSG, id, kDEFAULT_POST_MSG_TMO);
}

// ============================================================================
// ControllerInterface::axis_stop
// ============================================================================
void ControllerInterface::axis_stop(sgonpmaclib::T_cs_id id)
{
	this->post(kAXIS_STOP_MSG, id, kDEFAULT_POST_MSG_TMO);
}

// ============================================================================
// ControllerInterface::set_position
// ============================================================================
void ControllerInterface::set_position(sgonpmaclib::T_axis_id id, double position, bool apply)
{
	ParamAxisValueBool paramSetPosition;
	paramSetPosition.axisParams.idAxis = id;
	paramSetPosition.axisParams.valueDouble = position;
	paramSetPosition.bValue = apply;

	this->post(kSET_POSITION_MSG, paramSetPosition, kDEFAULT_POST_MSG_TMO);
}

// ============================================================================
// ControllerInterface::apply_position
// ============================================================================
void ControllerInterface::apply_position(sgonpmaclib::T_cs_id id)
{
	this->post(kAPPLY_POSITION_MSG, id, kDEFAULT_POST_MSG_TMO);
}

// ============================================================================
// ControllerInterface::set_offset
// ============================================================================
void ControllerInterface::set_offset(sgonpmaclib::T_axis_id id, double offset)
{
	ParamAxisValueMsg paramSetOffset;
	paramSetOffset.idAxis = id;
	paramSetOffset.valueDouble = offset;
	
	this->post(kSET_OFFSET_MSG, paramSetOffset, kDEFAULT_POST_MSG_TMO);
}

// ============================================================================
// ControllerInterface::get_offset
// ============================================================================
double ControllerInterface::get_offset(sgonpmaclib::T_axis_id id)
{
	yat::Message * msg = yat::Message::allocate(kGET_OFFSET_MSG, DEFAULT_MSG_PRIORITY, true);

	double offset = yat::IEEE_NAN;
  try
  {
    msg->attach_data(id);
    
    this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  
    offset = msg->get_data<double>();
  }
  catch (...)
  {
    msg->release();
    throw;
  }
     
  msg->release();

	return offset;
}
// ============================================================================
// ControllerInterface::set_compensation
// ============================================================================
void ControllerInterface::set_compensation(sgonpmaclib::T_axis_id id, bool compensation)
{
	ParamAxisValueMsg paramSetOffset;
	paramSetOffset.idAxis = id;
	paramSetOffset.valueBool = compensation;
	
	this->post(kSET_COMPENSASION_MSG, paramSetOffset, kDEFAULT_POST_MSG_TMO);
}

// ============================================================================
// ControllerInterface::get_compensation
// ============================================================================
bool ControllerInterface::get_compensation(sgonpmaclib::T_axis_id id)
{
	yat::Message * msg = yat::Message::allocate(kGET_COMPENSASION_MSG, DEFAULT_MSG_PRIORITY, true);

	bool compensation = yat::IEEE_NAN;
  try
  {
    msg->attach_data(id);
    
    this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  
    compensation = msg->get_data<bool>();
  }
  catch (...)
  {
    msg->release();
    throw;
  }
     
  msg->release();

	return compensation;
}


// ============================================================================
// ControllerInterface::get_offset_i
// ============================================================================
double ControllerInterface::get_offset_i(sgonpmaclib::T_axis_id id)
{
	CHECK_HW_INTERFACE;

	double offset = yat::IEEE_NAN;
	try 
  {
		offset = m_hw_interface->get_axis_pos_offset(id);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get offset - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_offset_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get offset - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_offset_i")); 
  }

	return offset;
}
// ============================================================================
// ControllerInterface::get_compens_table_validity
// ============================================================================
bool ControllerInterface::get_compens_table_validity(sgonpmaclib::T_cs_id id)
{
	yat::Message * msg = yat::Message::allocate(kGET_COMPENSATION_TABLE_VALIDITY_MSG, DEFAULT_MSG_PRIORITY, true);

	bool compensationTableValid ;
  compensationTableValid = false;
  try
  {
    msg->attach_data(id);
    
    this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  
    compensationTableValid = msg->get_data<bool>();
  }
  catch (...)
  {
    msg->release();
    throw;
  }
     
  msg->release();

	return compensationTableValid;
}

// ============================================================================
// ControllerInterface::get_compens_table_validity_i
// ============================================================================
bool ControllerInterface::get_compens_table_validity_i(sgonpmaclib::T_cs_id id)
{
	CHECK_HW_INTERFACE;

	bool compensationValid = yat::IEEE_NAN;
  
	try 
  {
		compensationValid = m_hw_interface->get_cs_compens_valid(id);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get compensationValid - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_offset_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get compensationValid - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_offset_i")); 
  }

	return compensationValid;
}

// ============================================================================
// ControllerInterface::get_compensation_i
// ============================================================================
bool ControllerInterface::get_compensation_i(sgonpmaclib::T_axis_id id)
{
	CHECK_HW_INTERFACE;

	bool compensation = yat::IEEE_NAN;
  
	try 
  {
		compensation = m_hw_interface->get_axis_compensation(id);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get compensation - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_offset_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get compensation - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_offset_i")); 
  }

	return compensation;
}

// ============================================================================
// ControllerInterface::axis_init_ref_pos
// ============================================================================
void ControllerInterface::axis_init_ref_pos(sgonpmaclib::T_cs_id cs)
{
	this->post(kAXIS_INIT_REF_POS_MSG, cs, kDEFAULT_POST_MSG_TMO);
}

// ============================================================================
// ControllerInterface::axis_init_ref_pos_i
// ============================================================================
void ControllerInterface::axis_init_ref_pos_i(sgonpmaclib::T_cs_id cs)
{
	CHECK_HW_INTERFACE;

	try {
		m_hw_interface->axis_init_ref_pos(cs);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to axis init ref pos - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::axis_init_ref_pos_i"));
  }
  catch(...)
  {
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to axis init ref pos - Caught unknown exception!"), 
							_CPTC("ControllerInterface::axis_init_ref_pos_i")); 
  }
}	

// ============================================================================
// ControllerInterface::get_status_details
// ============================================================================
sgonpmaclib::T_cs_status ControllerInterface::get_status_details(sgonpmaclib::T_cs_id id)
{
	yat::Message * msg = yat::Message::allocate(kGET_STATUS_DETAILS_MSG, DEFAULT_MSG_PRIORITY, true);

	sgonpmaclib::T_cs_status status;
  try
  {
    msg->attach_data(id);
    
    this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  
    status = msg->get_data<sgonpmaclib::T_cs_status>();
  }
  catch (...)
  {
    msg->release();
    throw;
  }
     
  msg->release();

	return status;
}

// ============================================================================
// ControllerInterface::get_motors_status
// ============================================================================
std::string ControllerInterface::get_motors_status(sgonpmaclib::T_cs_id id)
{
	yat::Message * msg = yat::Message::allocate(kGET_MOTORS_STATUS_MSG, DEFAULT_MSG_PRIORITY, true);

	std::string status;
  try
  {
    msg->attach_data(id);
    
    this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  
    status = msg->get_data<std::string>();
  }
  catch (...)
  {
    msg->release();
    throw;
  }
     
  msg->release();

	return status;
}

// ============================================================================
// ControllerInterface::get_status_details_i
// ============================================================================
sgonpmaclib::T_cs_status ControllerInterface::get_status_details_i(sgonpmaclib::T_cs_id id)
{
	CHECK_HW_INTERFACE;

	sgonpmaclib::T_cs_status status;
	try 
  {
		status = m_hw_interface->get_cs_status(id);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get status details - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_status_details_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get status details - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_status_details_i")); 
  }

	{
    yat::AutoMutex<> guard(ControllerInterface::m_lock);
    m_statusDetails = status;
  }

	return status;
}


// ============================================================================
// ControllerInterface::get_motors_status_i
// ============================================================================
std::string  ControllerInterface::get_motors_status_i(sgonpmaclib::T_cs_id id)
{
	CHECK_HW_INTERFACE;

	std::string status;
	try 
  {
		status = m_hw_interface->get_cs_motors_status(id);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get motors status - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_motors_status_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get motors status - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_motors_status_i")); 
  }

	return status;
}


// ============================================================================
// ControllerInterface::get_ref_status
// ============================================================================
sgonpmaclib::E_cs_referencing_status_t ControllerInterface::get_ref_status(sgonpmaclib::T_cs_id id)
{
	yat::Message * msg = yat::Message::allocate(kGET_REF_STATUS_MSG, DEFAULT_MSG_PRIORITY, true);

	sgonpmaclib::E_cs_referencing_status_t status;
  try
  {
    msg->attach_data(id);
    
    this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  
    status = msg->get_data<sgonpmaclib::E_cs_referencing_status_t>();
  }
  catch (...)
  {
    msg->release();
    throw;
  }
     
  msg->release();

	return status;
}

// ============================================================================
// ControllerInterface::get_ref_status_i
// ============================================================================
sgonpmaclib::E_cs_referencing_status_t ControllerInterface::get_ref_status_i(sgonpmaclib::T_cs_id id)
{
	CHECK_HW_INTERFACE;

	sgonpmaclib::E_cs_referencing_status_t ref_status = sgonpmaclib::CS_REF_POS_UNKNOWN;
	try 
  {
		ref_status = m_hw_interface->get_cs_ref_status(id);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get ref_status details - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_ref_status_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get ref_status details - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_ref_status_i")); 
  }


	return ref_status;
}
// ============================================================================
// ControllerInterface::get_acceleration
// ============================================================================
double ControllerInterface::get_acceleration(sgonpmaclib::T_cs_id id)
{
	yat::Message * msg = yat::Message::allocate(kGET_ACCELERATION_MSG, DEFAULT_MSG_PRIORITY, true);

	double accel = yat::IEEE_NAN;
  try
  {
    msg->attach_data(id);
    
    this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  
    accel = msg->get_data<double>();
  }
  catch (...)
  {
    msg->release();
    throw;
  }
     
  msg->release();

	return accel;
}

// ============================================================================
// ControllerInterface::get_accelSCurve
// ============================================================================
double ControllerInterface::get_accelSCurve(sgonpmaclib::T_cs_id id)
{
	yat::Message * msg = yat::Message::allocate(kGET_SCURVE_MSG, DEFAULT_MSG_PRIORITY, true);

	double accelScurve = yat::IEEE_NAN;
  try
  {
    msg->attach_data(id);
    
    this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  
    accelScurve = msg->get_data<double>();
  }
  catch (...)
  {
    msg->release();
    throw;
  }
     
  msg->release();

	return accelScurve;
}

// ============================================================================
// ControllerInterface::get_acceleration_i
// ============================================================================
double ControllerInterface::get_acceleration_i(sgonpmaclib::T_cs_id id)
{
	CHECK_HW_INTERFACE;

	double accel = yat::IEEE_NAN;
	try 
  {
		accel = m_hw_interface->get_cs_acceleration(id);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get acceleration - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_acceleration_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get acceleration - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_acceleration_i")); 
  }

  return accel;
}

// ============================================================================
// ControllerInterface::get_scurve_i
// ============================================================================
double ControllerInterface::get_scurve_i(sgonpmaclib::T_cs_id id)
{
	CHECK_HW_INTERFACE;

	double acceleSCurve = yat::IEEE_NAN;
	try 
  {
		acceleSCurve = m_hw_interface->get_cs_scurve_time(id);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get acceleSCurve - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_scurve_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get acceleSCurve - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_scurve_i")); 
  }

  return acceleSCurve;
}

// ============================================================================
// ControllerInterface::set_acceleration
// ============================================================================
void ControllerInterface::set_acceleration(sgonpmaclib::T_cs_id id, double acceleration)
{
	ParamCsValueMsg paramSetAcceleration;
	paramSetAcceleration.idCs = id;
	paramSetAcceleration.valueDouble = acceleration;
	
	this->post(kSET_ACCELERATION_MSG, paramSetAcceleration, kDEFAULT_POST_MSG_TMO);
}

// ============================================================================
// ControllerInterface::set_accelSCurve
// ============================================================================
void ControllerInterface::set_accelSCurve(sgonpmaclib::T_cs_id id, double accelScurve)
{
	ParamCsValueMsg paramAccelScurve;
	paramAccelScurve.idCs = id;
	paramAccelScurve.valueDouble = accelScurve;
	
	this->post(kSET_SCURVE_MSG, paramAccelScurve, kDEFAULT_POST_MSG_TMO);
}

// ============================================================================
// ControllerInterface::get_deceleration
// ============================================================================
double ControllerInterface::get_deceleration(sgonpmaclib::T_cs_id id)
{
	yat::Message * msg = yat::Message::allocate(kGET_DECELERATION_MSG, DEFAULT_MSG_PRIORITY, true);

	double decel = yat::IEEE_NAN;
  try
  {
    msg->attach_data(id);
    
    this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  
    decel = msg->get_data<double>();
  }
  catch (...)
  {
    msg->release();
    throw;
  }
     
  msg->release();

	return decel;
}

// ============================================================================
// ControllerInterface::get_deceleration_i
// ============================================================================
double ControllerInterface::get_deceleration_i(sgonpmaclib::T_cs_id id)
{
	CHECK_HW_INTERFACE;

	double decel = yat::IEEE_NAN;
	try 
  {
		decel = m_hw_interface->get_cs_deceleration(id);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get deceleration - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_deceleration_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get deceleration - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_deceleration_i")); 
  }

	return decel;
}

// ============================================================================
// ControllerInterface::set_deceleration
// ============================================================================
void ControllerInterface::set_deceleration(sgonpmaclib::T_cs_id id, double deceleration)
{
	ParamCsValueMsg paramSetDeceleration;
	paramSetDeceleration.idCs = id;
	paramSetDeceleration.valueDouble = deceleration;
	
	this->post(kSET_DECELERATION_MSG, paramSetDeceleration, kDEFAULT_POST_MSG_TMO);
}

// ============================================================================
// ControllerInterface::get_velocity
// ============================================================================
double ControllerInterface::get_velocity(sgonpmaclib::T_cs_id id)
{
	yat::Message * msg = yat::Message::allocate(kGET_VELOCITY_MSG, DEFAULT_MSG_PRIORITY, true);

	double velocity = yat::IEEE_NAN;
  try
  {
    msg->attach_data(id);
    
    this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  
    velocity = msg->get_data<double>();
  }
  catch (...)
  {
    msg->release();
    throw;
  }
     
  msg->release();

	return velocity;
}

// ============================================================================
// ControllerInterface::get_velocity_i
// ============================================================================
double ControllerInterface::get_velocity_i(sgonpmaclib::T_cs_id id)
{
	CHECK_HW_INTERFACE;

	double velocity = yat::IEEE_NAN;
	try 
  {
		velocity = m_hw_interface->get_cs_velocity(id);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get velocity - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_cs_velocity_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get velocity - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_cs_velocity_i")); 
  }

	return velocity;
}

// ============================================================================
// ControllerInterface::set_velocity
// ============================================================================
void ControllerInterface::set_velocity(sgonpmaclib::T_cs_id id, double velocity)
{
	ParamCsValueMsg paramSetVelocity;
	paramSetVelocity.idCs = id;
	paramSetVelocity.valueDouble = velocity;
	
	this->post(kSET_VELOCITY_MSG, paramSetVelocity, kDEFAULT_POST_MSG_TMO);
}

// ============================================================================
// ControllerInterface::reset_i
// ============================================================================
void ControllerInterface::reset_i()
{
	CHECK_HW_INTERFACE;

	try {
		m_hw_interface->reset();
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to reset - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::reset_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to reset - Caught unknown exception!"), 
							_CPTC("ControllerInterface::reset_i")); 
  }

}

// ============================================================================
// ControllerInterface::reboot_i
// ============================================================================
void ControllerInterface::reboot_i()
{
	CHECK_HW_INTERFACE;

	try {
		m_hw_interface->reboot();
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to reboot - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::reboot_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to reboot - Caught unknown exception!"), 
							_CPTC("ControllerInterface::reboot_i")); 
  }
}

// ============================================================================
// ControllerInterface::abort_i
// ============================================================================
void ControllerInterface::abort_i()
{
	CHECK_HW_INTERFACE;

	try 
  {
		m_hw_interface->abort();
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to abort - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::abort_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to abort - Caught unknown exception!"), 
							_CPTC("ControllerInterface::abort_i")); 
  }
}

// ============================================================================
// ControllerInterface::motor_on_i
// ============================================================================
void ControllerInterface::motor_on_i(sgonpmaclib::T_cs_id id)
{
	CHECK_HW_INTERFACE;

	try {
		m_hw_interface->motor_on(id);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to motor on - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::motor_on_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to motor on - Caught unknown exception!"), 
							_CPTC("ControllerInterface::motor_on_i")); 
  }
}

// ============================================================================
// ControllerInterface::motor_omega_off_i
// ============================================================================
void ControllerInterface::motor_omega_off_i(sgonpmaclib::T_cs_id id)
{
	CHECK_HW_INTERFACE;

	try 
  {
		m_hw_interface->motor_omega_off(id);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to motor off - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::motor_omega_off_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to motor off - Caught unknown exception!"), 
							_CPTC("ControllerInterface::motor_omega_off_i")); 
  }
}// ============================================================================
// ControllerInterface::motor_other_off_i
// ============================================================================
void ControllerInterface::motor_other_off_i(sgonpmaclib::T_cs_id id)
{
	CHECK_HW_INTERFACE;

	try 
  {
		m_hw_interface->motor_other_off(id);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to motor off - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::motor_other_off_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to motor off - Caught unknown exception!"), 
							_CPTC("ControllerInterface::motor_other_off_i")); 
  }
}
// ============================================================================
// ControllerInterface::motor_off_i
// ============================================================================
void ControllerInterface::motor_off_i(sgonpmaclib::T_cs_id id)
{
	CHECK_HW_INTERFACE;

	try 
  {
		m_hw_interface->motor_off(id);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to motor off - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::motor_off_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to motor off - Caught unknown exception!"), 
							_CPTC("ControllerInterface::motor_off_i")); 
  }
}

// ============================================================================
// ControllerInterface::axis_stop_i
// ============================================================================
void ControllerInterface::axis_stop_i(sgonpmaclib::T_cs_id id)
{
	CHECK_HW_INTERFACE;

	try 
  {
		m_hw_interface->axis_stop(id);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to axis stop - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::axis_stop_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to axis stop - Caught unknown exception!"), 
							_CPTC("ControllerInterface::axis_stop_i")); 
  }
}

// ============================================================================
// ControllerInterface::set_position_i
// ============================================================================
void ControllerInterface::set_position_i(sgonpmaclib::T_axis_id id, double position, bool apply)
{
	CHECK_HW_INTERFACE;

  try 
  {
		m_hw_interface->set_axis_position(id, position, apply);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to set position - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::set_position_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to set position - Caught unknown exception!"), 
							_CPTC("ControllerInterface::set_position_i")); 
  }
}

// ============================================================================
// ControllerInterface::apply_position_i
// ============================================================================
void ControllerInterface::apply_position_i(sgonpmaclib::T_cs_id id)
{
	CHECK_HW_INTERFACE;

	try 
  {
		m_hw_interface->apply_axis_position(id);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to apply position - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::apply_position_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to apply position - Caught unknown exception!"), 
							_CPTC("ControllerInterface::apply_position_i")); 
  }
}

// ============================================================================
// ControllerInterface::set_offset_i
// ============================================================================
void ControllerInterface::set_offset_i(sgonpmaclib::T_axis_id id, double offset)
{
	CHECK_HW_INTERFACE;

	try 
  {
		m_hw_interface->set_axis_pos_offset(id, offset);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to set offset - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::set_offset_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to set offset - Caught unknown exception!"), 
							_CPTC("ControllerInterface::set_offset_i")); 
  }
}
// ============================================================================
// ControllerInterface::set_compensation_i
// ============================================================================
void ControllerInterface::set_compensation_i(sgonpmaclib::T_axis_id id, bool compensation)
{
	CHECK_HW_INTERFACE;

	try 
  {
		m_hw_interface->set_axis_compensation(id, compensation);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to set compensation - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::set_compensation_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to set compensation - Caught unknown exception!"), 
							_CPTC("ControllerInterface::set_compensation_i")); 
  }
}
// ============================================================================
// ControllerInterface::set_acceleration_i
// ============================================================================
void ControllerInterface::set_acceleration_i(sgonpmaclib::T_cs_id id, double acceleration)
{
	CHECK_HW_INTERFACE;

	try 
  {
		m_hw_interface->set_cs_acceleration(id, acceleration);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to set acceleration - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::set_acceleration_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to set acceleration - Caught unknown exception!"), 
							_CPTC("ControllerInterface::set_acceleration_i")); 
  }
}

// ============================================================================
// ControllerInterface::set_scurve_i
// ============================================================================
void ControllerInterface::set_scurve_i(sgonpmaclib::T_cs_id id, double accelSCurve)
{
	CHECK_HW_INTERFACE;

	try 
  {
		m_hw_interface->set_cs_scurve_time(id, accelSCurve);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to set accelSCurve - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::set_scurve_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to set accelSCurve - Caught unknown exception!"), 
							_CPTC("ControllerInterface::set_scurve_i")); 
  }
}

// ============================================================================
// ControllerInterface::set_deceleration_i
// ============================================================================
void ControllerInterface::set_deceleration_i(sgonpmaclib::T_cs_id id, double deceleration)
{
	CHECK_HW_INTERFACE;

	try
  {
		m_hw_interface->set_cs_deceleration(id, deceleration);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to set deceleration - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::set_deceleration_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to set deceleration - Caught unknown exception!"), 
							_CPTC("ControllerInterface::set_deceleration_i")); 
  }
}

// ============================================================================
// ControllerInterface::set_velocity_i
// ============================================================================
void ControllerInterface::set_velocity_i(sgonpmaclib::T_cs_id id, double velocity)
{
	CHECK_HW_INTERFACE;

	try
  {
		m_hw_interface->set_cs_velocity(id, velocity);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to set velocity - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::set_velocity_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to set velocity - Caught unknown exception!"), 
							_CPTC("ControllerInterface::set_velocity_i")); 
  }
}

// ============================================================================
// ControllerInterface::get_cs_moving_mode
// ============================================================================
sgonpmaclib::E_moving_mode_t ControllerInterface::get_cs_moving_mode(sgonpmaclib::T_cs_id id)
{
	yat::Message * msg = yat::Message::allocate(kGET_MOVING_MODE_MSG, DEFAULT_MSG_PRIORITY, true);

	sgonpmaclib::E_moving_mode_t mode = sgonpmaclib::CS_MV_MODE_RAPID;
  try
  {
    msg->attach_data(id);
    
    this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  
    mode = msg->get_data<sgonpmaclib::E_moving_mode_t>();
  }
  catch (...)
  {
    msg->release();
    throw;
  }
     
  msg->release();

	return mode;
}

// ============================================================================
// ControllerInterface::get_cs_moving_mode_i
// ============================================================================
sgonpmaclib::E_moving_mode_t ControllerInterface::get_cs_moving_mode_i(sgonpmaclib::T_cs_id id)
{
	CHECK_HW_INTERFACE;
	sgonpmaclib::E_moving_mode_t mode = sgonpmaclib::CS_MV_MODE_RAPID;

	try 
  {
		mode = m_hw_interface->get_cs_moving_mode(id);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get cs moving mode - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_cs_moving_mode_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get cs moving mode - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_cs_moving_mode_i")); 
  }

	return mode;
}

// ============================================================================
// ControllerInterface::set_cs_moving_mode
// ============================================================================
void ControllerInterface::set_cs_moving_mode(sgonpmaclib::T_cs_id id, sgonpmaclib::E_moving_mode_t mode)
{
	ParamCsModeMsg paramSetCsMovingMode;
	paramSetCsMovingMode.idCs = id;
	paramSetCsMovingMode.mv_mode = mode;
	
	this->post(kSET_MOVING_MODE_MSG, paramSetCsMovingMode, kDEFAULT_POST_MSG_TMO);
}

// ============================================================================
// ControllerInterface::set_cs_moving_mode_i
// ============================================================================
void ControllerInterface::set_cs_moving_mode_i(sgonpmaclib::T_cs_id id, sgonpmaclib::E_moving_mode_t mode)
{
	CHECK_HW_INTERFACE;

	try 
  {
		m_hw_interface->set_cs_moving_mode(id, mode);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to set cs moving mode - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::set_cs_moving_mode_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to set cs moving mode - Caught unknown exception!"), 
							_CPTC("ControllerInterface::set_cs_moving_mode_i")); 
  }
}

// ============================================================================
// ControllerInterface::get_box_in_error
// ============================================================================
bool ControllerInterface::get_box_in_error()
{
  bool err = false;
  sgonpmaclib::T_ppmac_brick_status box_st;

  // get current box status
  try
  {
    box_st = this->get_box_status();
  }
  catch(...)
  {
    // Cannot get box status, set error to true!
    err = true;
    return err;
  }

  // apply error mask
  err = box_error_mask(box_st);

  return err;
}

// ============================================================================
// ControllerInterface::start_mp
// ============================================================================
void ControllerInterface::start_mp(sgonpmaclib::T_cs_id id, int mp)
{
	ParamCsValueMsg paramStartMP;
	paramStartMP.idCs = id;
	paramStartMP.valueDouble = (double)mp;
	
	this->post(kCS_START_MP_MSG, paramStartMP, kDEFAULT_POST_MSG_TMO);
}

// ============================================================================
// ControllerInterface::pause_mp
// ============================================================================
void ControllerInterface::pause_mp(sgonpmaclib::T_cs_id id)
{
	this->post(kCS_PAUSE_MP_MSG, id, kDEFAULT_POST_MSG_TMO);
}

// ============================================================================
// ControllerInterface::resume_mp
// ============================================================================
void ControllerInterface::resume_mp(sgonpmaclib::T_cs_id id)
{
	this->post(kCS_RESUME_MP_MSG, id, kDEFAULT_POST_MSG_TMO);
}

// ============================================================================
// ControllerInterface::get_mp_state
// ============================================================================
sgonpmaclib::E_mp_state_t ControllerInterface::get_mp_state(sgonpmaclib::T_cs_id id)
{
	yat::Message * msg = yat::Message::allocate(kCS_GET_STATE_MP_MSG, DEFAULT_MSG_PRIORITY, true);

	sgonpmaclib::E_mp_state_t st = sgonpmaclib::MP_STATE_UNKNOWN;
  try
  {
    msg->attach_data(id);
    
    this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  
    st = msg->get_data<sgonpmaclib::E_mp_state_t>();
  }
  catch (...)
  {
    msg->release();
    throw;
  }
     
  msg->release();

	return st;
}

// ============================================================================
// ControllerInterface::start_mp_i
// ============================================================================
void ControllerInterface::start_mp_i(sgonpmaclib::T_cs_id id, int mp)
{
	CHECK_HW_INTERFACE;

	try 
  {
		m_hw_interface->start_mp(id, mp);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to start MP - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::start_mp_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to start MP - Caught unknown exception!"), 
							_CPTC("ControllerInterface::start_mp_i")); 
  }
}

// ============================================================================
// ControllerInterface::pause_mp_i
// ============================================================================
void ControllerInterface::pause_mp_i(sgonpmaclib::T_cs_id id)
{
	CHECK_HW_INTERFACE;

	try {
		m_hw_interface->pause_mp(id);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to pause MP - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::pause_mp_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to pause MP - Caught unknown exception!"), 
							_CPTC("ControllerInterface::pause_mp_i")); 
  }
}	

// ============================================================================
// ControllerInterface::resume_mp_i
// ============================================================================
void ControllerInterface::resume_mp_i(sgonpmaclib::T_cs_id id)
{
	CHECK_HW_INTERFACE;

	try {
		m_hw_interface->resume_mp(id);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to resume MP - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::resume_mp_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to resume MP - Caught unknown exception!"), 
							_CPTC("ControllerInterface::resume_mp_i")); 
  }
}	

// ============================================================================
// ControllerInterface::get_position
// ============================================================================
double ControllerInterface::get_position(sgonpmaclib::T_axis_id id)
{
	yat::Message * msg = yat::Message::allocate(kGET_POSITION_MSG, DEFAULT_MSG_PRIORITY, true);

	double position = yat::IEEE_NAN;
  try
  {
    msg->attach_data(id);
    
    this->wait_msg_handled(msg->duplicate(), kDEFAULT_TIMEOUT_CMD);
  
    position = msg->get_data<double>();
  }
  catch (...)
  {
    msg->release();
    throw;
  }
     
  msg->release();

	return position;
}

// ============================================================================
// ControllerInterface::get_position_i
// ============================================================================
double ControllerInterface::get_position_i(sgonpmaclib::T_axis_id id)
{
	CHECK_HW_INTERFACE;
  // Update all axes positions
	std::vector<double> axes_pos;
 
	try 
  {
		m_hw_interface->get_axes_positions(m_axisList, axes_pos);

		// Update m_axisMap with all position
		for (size_t idx = 0; idx < axes_pos.size(); idx++)
	  {
			m_axisMap[m_axisList[idx]].position = axes_pos[idx];
		}
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get position - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_position_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get position - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_position_i")); 
  }

	return m_axisMap[id].position;
}

// ============================================================================
// ControllerInterface::get_mp_state_i
// ============================================================================
sgonpmaclib::E_mp_state_t ControllerInterface::get_mp_state_i(sgonpmaclib::T_cs_id id)
{
	CHECK_HW_INTERFACE;

	sgonpmaclib::E_mp_state_t st = sgonpmaclib::MP_STATE_UNKNOWN;
	try 
  {
		st = m_hw_interface->get_mp_state(id);
	} 
	catch(Tango::DevFailed &e)
  {
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get MP state - Caught DevFailed!"), 
                     _CPTC("ControllerInterface::get_mp_state_i"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get MP state - Caught unknown exception!"), 
							_CPTC("ControllerInterface::get_mp_state_i")); 
  }

	return st;
}

} // namespace sgonppmac_ns


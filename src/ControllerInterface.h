//=============================================================================
// ControllerInterface.h
//=============================================================================
// class.............Controler Interface (Default implementation synchronous task)
// original author...F. THIAM
//=============================================================================

#ifndef _CONTROLLER_INTERFACE_H_
#define _CONTROLLER_INTERFACE_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <yat4tango/DeviceTask.h>
#include "SGonPPMACTypesAndConsts.h"
#include "HwAccess.h"


namespace sgonppmac_ns {


// ============================================================================
// class: ControllerInterface
// ============================================================================
class ControllerInterface : public yat4tango::DeviceTask
{

public:

	
  // Constructor.
  ControllerInterface(Tango::DeviceImpl * hostDevice);

  // Destructor.
  virtual ~ControllerInterface();

	
  //---------------------------------------------------------------------------
  // Controller management
  //---------------------------------------------------------------------------

	// Initializes the driver's connection.
  // This function opens a session with the controller.
  //void init_interface(std::string ipAddress, unsigned int tcpPort);
	void init_interface(PowerPMACConfig &cfg);

	// Gets the controller's connection state (ok/nok).
  // @param isConnected  (Out) true or false
  virtual void get_connection_state(bool &isConnected);

	// Set the axis initialization data
	void set_axis_init_data(sgonpmaclib::T_axis_id id, AxisInitData &initData );

	// Declare axis list
	// @param axis_list Axis id (cs, axis name) list.
	void declare_axis( std::vector<sgonpmaclib::T_axis_id> axis_list);

	// Undeclare axis list
	// @param axis_list Axis id (cs, axis name) list.
	void undeclare_axis( std::vector<sgonpmaclib::T_axis_id> axis_list);
    
	// Resets the controller.
  // Does not break ssh connection.
  void reset();

  // Reboots the controller operating system.
  void reboot();
	
  // Stops all axes movements and plc programs.
  void abort();
  
  // Executes a low level command.
  // @param cmd The low level command
  // @param outParam Command reply
  void exec_low_level_cmd(std::string cmd, std::string &outParam);

  // Gets the controller status word.
  // @return PPMAC controller current status (binary word)
	virtual sgonpmaclib::T_ppmac_brick_status get_box_status();

	// Gets firmware version.
  // @return Firmware version
  virtual std::string get_firmware_vers();

  // Gets micro code version.
  // @return Micro code version
  virtual std::string get_ucode_vers();
	
	// Gets the CPU temperature.
  // @return CPU temperature in Celsius degrees
  virtual double get_cpu_temp();
	
  // Gets CPU running time from power-on to present, in seconds.
  // @return Running time (s). 
  virtual double get_cpu_running_time();

  // Gets CPU usage to complete tasks in last phase cycle, in �sec.
  // @return Phase usage. 
  virtual double get_cpu_phase_usage();

  // Gets CPU usage to complete tasks in last servo cycle, in �sec.
  // @return So usage. 
  virtual double get_cpu_servo_usage();

  // Gets CPU usage to complete tasks in last RTI cycle, in �sec.
  // @return RTI usage. 
  virtual double get_cpu_rti_usage();

  // Gets CPU usage to complete tasks in last BG cycle, in �sec.
  // @return BG usage. 
  virtual double get_cpu_bg_usage();

  // Is box in error?
  // @return Box in error or not. 
  virtual bool get_box_in_error();

  // Powers on the underlying motor.
  // @param id CS id.
	void motor_on(sgonpmaclib::T_cs_id id);
  
  // Powers off the underlying motor.
  // @param id CS id.
	void motor_omega_off(sgonpmaclib::T_cs_id id); 
  
  // Powers off the underlying motor.
  // @param id CS id.
	void motor_other_off(sgonpmaclib::T_cs_id id);

  // Powers off the underlying motor.
  // @param id CS id.
	void motor_off(sgonpmaclib::T_cs_id id);

	// Stop current axis movement.
  // @param id CS id.
	void axis_stop(sgonpmaclib::T_cs_id id);

  // Initializes reference position with current init type 
  // and init position.
  // @param id CS id.
	void axis_init_ref_pos(sgonpmaclib::T_cs_id cs);

	// Moves axis to specified absolute position, in user unit.
  // @param id Axis id (cs, axis name).
  // @param position Axis absolute position setpoint in user unit.
  // @param apply True to set and apply position setpoint, false to only set position setpoint.
	void set_position(sgonpmaclib::T_axis_id id, double position, bool apply = true);

  // Apply axis position setpoints in specified CS.
  // @param id CS id.
	void apply_position(sgonpmaclib::T_cs_id id);

  // Sets axis position offset, in user unit.
  // @param id Axis id (cs, axis name).
  // @param offset Axis position offset in user unit.
	void set_offset(sgonpmaclib::T_axis_id id, double offset);
	
  // Gets current axis position offset, in user unit.
  // @param id Axis id (cs, axis name).
  // @return Current axis position offset in user unit.
	virtual double get_offset(sgonpmaclib::T_axis_id id);

  // Gets current CS status.
  // @param id CS id.
  // @return CS status.
	virtual sgonpmaclib::T_cs_status get_status_details(sgonpmaclib::T_cs_id id);
  
  // Gets current CS status.
  // @param id CS id.
  // @return CS status.
	virtual std::string get_motors_status(sgonpmaclib::T_cs_id id);
  
  // Gets current referencing status.
  // @param id CS id.
  // @return referencing status.
  virtual sgonpmaclib::E_cs_referencing_status_t get_ref_status(sgonpmaclib::T_cs_id id);

	// Gets current axis acceleration, in user unit/s^2.
  // @param id CS id.
  // @return Current axis acceleration, in user unit/s^2.
	virtual double get_acceleration(sgonpmaclib::T_cs_id id);

  // Sets axis acceleration, in user unit/s^2.
  // @param id CS id.
  // @param acceleration Axis acceleration setpoint in user unit/s^2.
	void set_acceleration(sgonpmaclib::T_cs_id id, double acceleration);

  // Gets current axis deceleration, in user unit/s^2.
  // @param id CS id.
  // @return Current axis deceleration, in user unit/s^2.
	virtual double get_deceleration(sgonpmaclib::T_cs_id id);
  
  // Sets axis deceleration, in user unit/s^2.
  // @param id CS id.
  // @param deceleration Axis deceleration setpoint in user unit/s^2.
	void set_deceleration(sgonpmaclib::T_cs_id id, double deceleration);

  // Gets current axis velocity, in user unit/s.
  // @param id CS id.
  // @return Current axis velocity, in user unit/s.
	virtual double get_velocity(sgonpmaclib::T_cs_id id);

	// Sets axis velocity, in user unit/s.
  // @param id CS id.
  // @param velocity Axis velocity setpoint in user unit/s.
	void set_velocity(sgonpmaclib::T_cs_id id, double velocity);

	// Sets axis moving mode.
  // @param id CS id.
  // @param mode CS moving mode.
  void set_cs_moving_mode(sgonpmaclib::T_cs_id id, sgonpmaclib::E_moving_mode_t mode);

  // Gets current axis moving mode.
  // @param id CS id.
  // @return Current axis moving mode.
  virtual sgonpmaclib::E_moving_mode_t get_cs_moving_mode(sgonpmaclib::T_cs_id id);

  // Starts motion program.
  // @param id CS id.
  // @param mp MP id.
  void start_mp(sgonpmaclib::T_cs_id id, int mp);

  // Pauses current motion program.
  // @param id CS id.
  void pause_mp(sgonpmaclib::T_cs_id id);

  // Resumes current motion program.
  // @param id CS id.
  void resume_mp(sgonpmaclib::T_cs_id id);

  // Gets current MP state.
  // @param id CS id.
  // @return Current MP state.
  virtual sgonpmaclib::E_mp_state_t get_mp_state(sgonpmaclib::T_cs_id id);
  
  // Gets current axis absolute position, in user unit.
  // @param id Axis id (cs, axis name).
  // @return Current axis position in user unit.
	virtual double get_position(sgonpmaclib::T_axis_id id);
  
  // Sets axis commpensation.
  // @param id Axis id (cs, axis name).
  // @param compensation 
  void set_compensation(sgonpmaclib::T_axis_id id, bool compensation);

  // Gets current axis compensation state.
  // @param id Axis id (cs, axis name).
  // @return compensation state
  virtual bool get_compensation(sgonpmaclib::T_axis_id id);

  // Returns true if compensation tables are valid
  // @return compensation validity
  virtual bool get_compens_table_validity(sgonpmaclib::T_cs_id id);
  
  // To get accelScurve
  // @param T_cs_id
  // @return accelScurve
  virtual double get_accelSCurve(sgonpmaclib::T_cs_id id);
  
  // To set accelScurve
  // @param T_cs_id
  // @param accelScurve
  void set_accelSCurve(sgonpmaclib::T_cs_id id, double accelScurve);

protected:

  // Hw Access to low level lib access
  HwAccess *m_hw_interface;

	//- to avoid race condition between ControllerInterface class & periodic_job
  yat::Mutex m_lock;

	// Axis Params map
	AxisParamsMap_t m_axisMap;

	// Axis list
	AxisList_t m_axisList;

	// Cs Params map
	CsParamsMap_t m_csMap;

	// Cs list
	CsList_t m_csList;


//- process_message (implements yat4tango::DeviceTask pure virtual method)
	virtual void process_message (yat::Message& msg)
		throw (Tango::DevFailed);
	
	//- configuration
  PowerPMACConfig m_cfg;

	//- box statut
	sgonpmaclib::T_ppmac_brick_status m_boxStatus;

	//- Connected to the power brick or not
	bool m_isConnected;

	// Current CS status.
  sgonpmaclib::T_cs_status m_statusDetails;

  // Current CS synthetic state.
  //sgonpmaclib::E_cs_state_t m_axisState;

	// Firmware version.
  std::string m_firmwareVers;

  // Bricklv version.
  std::string m_brickLvVers;

  // Micro code version.
  std::string m_uCodeVers;
	
	// CPU temperature.
  double m_cpuTemp;
	
  // CPU running time from power-on to present, in seconds.
  double m_cpuRunningTime;

  // CPU usage to complete tasks in last phase cycle, in �sec.
  double m_cpuPhaseUsage;

  // CPU usage to complete tasks in last servo cycle, in �sec.
  double m_cpuServoUsage;

  // CPU usage to complete tasks in last RTI cycle, in �sec.
  double m_cpuRtiUsage;

  // CPU usage to complete tasks in last BG cycle, in �sec.
  double m_cpuBgUsage;

	//- periodic job
  virtual void periodic_job_i ();

	// Resets the controller. (internal)
  void reset_i();

  // Reboots the controller operating system. (internal)
  void reboot_i();
	
  // Stops all axes movements and plc programs. (internal)
  void abort_i();
  
  // Powers on the underlying motor. (internal)
  // @param id CS id.
	void motor_on_i(sgonpmaclib::T_cs_id id);

  // Powers off the underlying motor. (internal)
  // @param id CS id.
	void motor_off_i(sgonpmaclib::T_cs_id id);
  
  // Powers off the underlying motor. (internal)
  // @param id CS id.
	void motor_omega_off_i(sgonpmaclib::T_cs_id id);
  
  // Powers off the underlying motor. (internal)
  // @param id CS id.
	void motor_other_off_i(sgonpmaclib::T_cs_id id);

	// Stop current axis movement. (internal)
  // @param id CS id.
	void axis_stop_i(sgonpmaclib::T_cs_id id);

  // Initializes reference position with current init type
  // and init position.   (internal)
  // @param id CS id.
	void axis_init_ref_pos_i(sgonpmaclib::T_cs_id cs);

	// Moves axis to specified absolute position, in user unit. (internal)
  // @param id Axis id (cs, axis name).
  // @param position Axis absolute position setpoint in user unit.
  // @param apply True to set and apply position setpoint, false to only set position setpoint.
	void set_position_i(sgonpmaclib::T_axis_id id, double position, bool apply);

  // Apply axis position setpoints in specified CS. (internal)
  // @param id CS id.
	void apply_position_i(sgonpmaclib::T_cs_id id);
  
  // Sets axis position offset, in user unit. (internal)
  // @param id Axis id (cs, axis name).
  // @param offset Axis position offset in user unit.
	void set_offset_i(sgonpmaclib::T_axis_id id, double offset);
  
  // Sets axis compensation. (internal)
  // @param id Axis id (cs, axis name).
  // @param compensation state.
  void set_compensation_i(sgonpmaclib::T_axis_id id, bool compensation);
  
  // gets cs compensation table state. (internal)
  // @param id T_cs_id  (cs, axis name).
  // @return table validity
  bool get_compens_table_validity_i(sgonpmaclib::T_cs_id id);

  // Sets axis acceleration, in user unit/s^2. (internal)
  // @param id CS id.
  // @param acceleration Axis acceleration setpoint in user unit/s^2.
	void set_acceleration_i(sgonpmaclib::T_cs_id id, double acceleration);
  
  // Sets axis scurve
  // @param id CS id.
  // @param accelScurve.
	void set_scurve_i(sgonpmaclib::T_cs_id id, double accelScurve);
  
  // Sets axis scurve
  // @param id CS id.
  // @param accelScurve.
	double get_scurve_i(sgonpmaclib::T_cs_id id);

  // Sets axis deceleration, in user unit/s^2. (internal)
  // @param id CS id.
  // @param deceleration Axis deceleration setpoint in user unit/s^2.
	void set_deceleration_i(sgonpmaclib::T_cs_id id, double deceleration);

  // Sets axis velocity, in user unit/s. (internal)
  // @param id CS id.
  // @param velocity Axis velocity setpoint in user unit/s.
	void set_velocity_i(sgonpmaclib::T_cs_id id, double velocity);

	
	// Gets the controller's connection state (ok/nok). (internal)
  // @return true or false
  virtual bool get_connection_state_i();

	// Executes a low level command. (internal)
  // @param cmd The low level command
  // @param outParam Command reply
  void exec_low_level_cmd_i(std::string cmd, std::string &outParam);

  // Gets the controller status word. (internal)
  // @return PPMAC controller current status (binary word)
	virtual sgonpmaclib::T_ppmac_brick_status get_box_status_i();

	// Gets firmware version. (internal)
  // @return Firmware version
  virtual std::string get_firmware_vers_i();

  // Gets micro code version. (internal)
  // @return Micro code version
  virtual std::string get_ucode_vers_i();
	
	// Gets the CPU temperature. (internal)
  // @return CPU temperature in Celsius degrees
  virtual double get_cpu_temp_i();
	
  // Gets CPU running time from power-on to present, in seconds. (internal)
  // @return Running time (s). 
  virtual double get_cpu_running_time_i();

  // Gets CPU usage to complete tasks in last phase cycle, in �sec. (internal)
  // @return Phase usage. 
  virtual double get_cpu_phase_usage_i();

  // Gets CPU usage to complete tasks in last servo cycle, in �sec. (internal)
  // @return So usage. 
  virtual double get_cpu_servo_usage_i();

  // Gets CPU usage to complete tasks in last RTI cycle, in �sec. (internal)
  // @return RTI usage. 
  virtual double get_cpu_rti_usage_i();

  // Gets CPU usage to complete tasks in last BG cycle, in �sec. (internal)
  // @return BG usage. 
  virtual double get_cpu_bg_usage_i();

  // Gets current axis position offset, in user unit. (internal)
  // @param id Axis id (cs, axis name).
  // @return Current axis position offset in user unit.
	virtual double get_offset_i(sgonpmaclib::T_axis_id id);
  
  // Gets current axis compensation state (internal)
  // @param id Axis id (cs, axis name).
  // @return compensation state
	virtual bool get_compensation_i(sgonpmaclib::T_axis_id id);

  // Gets current CS status. (internal)
  // @param id CS id.
  // @return CS status.
	virtual sgonpmaclib::T_cs_status get_status_details_i(sgonpmaclib::T_cs_id id);

  // Gets current CS status. (internal)
  // @param id CS id.
  // @return CS status.
	virtual std::string get_motors_status_i(sgonpmaclib::T_cs_id id);
  
  // Gets current CS status. (internal)
  // @param id CS id.
  // @return CS status.
  virtual sgonpmaclib::E_cs_referencing_status_t get_ref_status_i(sgonpmaclib::T_cs_id id);

  // Gets current axis position. (internal)
  // @param id CS id.
  // @return Current axis position.
	virtual double get_position_i(sgonpmaclib::T_axis_id id);

	// Gets current axis acceleration, in user unit/s^2. (internal)
  // @param id CS id.
  // @return Current axis acceleration, in user unit/s^2.
	virtual double get_acceleration_i(sgonpmaclib::T_cs_id id);

  // Gets current axis deceleration, in user unit/s^2. (internal)
  // @param id CS id.
  // @return Current axis deceleration, in user unit/s^2.
	virtual double get_deceleration_i(sgonpmaclib::T_cs_id id);

  // Gets current axis velocity, in user unit/s. (internal)
  // @param id CS id.
  // @return Current axis velocity, in user unit/s.
	virtual double get_velocity_i(sgonpmaclib::T_cs_id id);

	// Sets axis moving mode.  (internal)
  // @param id CS id.
  // @param mode CS moving mode.
  void set_cs_moving_mode_i(sgonpmaclib::T_cs_id id, sgonpmaclib::E_moving_mode_t mode);

  // Gets current axis moving mode. (internal)
  // @param id CS id.
  // @return Current axis moving mode.
  virtual sgonpmaclib::E_moving_mode_t get_cs_moving_mode_i(sgonpmaclib::T_cs_id id);
  // Starts motion program. (internal)
  // @param id CS id.
  // @param mp MP id.
  void start_mp_i(sgonpmaclib::T_cs_id id, int mp);

  // Pauses current motion program. (internal)
  // @param id CS id.
  void pause_mp_i(sgonpmaclib::T_cs_id id);

  // Resumes current motion program. (internal)
  // @param id CS id.
  void resume_mp_i(sgonpmaclib::T_cs_id id);

  // Gets current MP state. (internal)
  // @param id CS id.
  // @return Current MP state.
  virtual sgonpmaclib::E_mp_state_t get_mp_state_i(sgonpmaclib::T_cs_id id);

};

} // namespace sgonppmac_ns

#endif // _CONTROLLER_INTERFACE_H_

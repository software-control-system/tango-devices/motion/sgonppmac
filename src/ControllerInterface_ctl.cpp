//=============================================================================
// ControllerInterface_ctl.cpp
//=============================================================================
// class.............Controler Interface for Critical Data (asynchronous)
// original author...F. THIAM
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <sstream>
#include "ControllerInterface_ctl.h"

// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================


namespace sgonppmac_ns {

// ============================================================================
//- Check hw interface pointer:
#define CHECK_HW_INTERFACE \
  if (!m_hw_interface) \
  { \
     THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
                     _CPTC("request aborted - the controller isn't accessible "), \
                     _CPTC("ControllerInterface_ctl::CHECK_HW_INTERFACE()")); \
  }
// ============================================================================

// ============================================================================
// ControllerInterface_ctl::ControllerInterface_ctl
// ============================================================================
ControllerInterface_ctl::ControllerInterface_ctl(Tango::DeviceImpl * hostDevice)
	: ControllerInterface(hostDevice)
{
  m_axesStateError = false;
  m_axesPosError = false;
}

// ============================================================================
// ControllerInterface_ctl::~ControllerInterface_ctl
// ============================================================================
ControllerInterface_ctl::~ControllerInterface_ctl ()
{
  this->enable_periodic_msg(false);
}

// ============================================================================
// ControllerInterface_ctl::periodic_job_i
// ============================================================================
void ControllerInterface_ctl::periodic_job_i ()
{
  CHECK_HW_INTERFACE;

  m_axesStateError = false;
  m_axesPosError = false;

  AxisParamsMap_it_t itAxis;
  sgonpmaclib::T_axis_id  axis_id;

  CdParamsMap_it_t itCs;
  sgonpmaclib::T_cs_id  cs_id;
  
  bool referencing_in_progress = false;

  yat::AutoMutex<> guard(ControllerInterface::m_lock);

  // get connection state
  try 
  {
    bool isConnected = false;
    m_hw_interface->get_connection_state(isConnected);
    {
      m_isConnected = isConnected;
    }
  } 
  catch(...)
  {
    ERROR_STREAM << "Failed to get connection state in periodic job!" << std::endl;
  }

  // Parse the cs map to get status informations
  for (itCs = this->m_csMap.begin(); itCs != this->m_csMap.end(); ++itCs)
  {
    cs_id = itCs->first;

    if (m_hw_interface->get_cs_ref_status(cs_id) == sgonpmaclib::CS_REF_POS_RUNNING)
      referencing_in_progress = true;

    sgonpmaclib::T_cs_status detailed_status;
    try 
    {
      detailed_status = m_hw_interface->get_cs_status(cs_id);
      (itCs->second).status = detailed_status;
    } 
    catch(...)
    {
      ERROR_STREAM << "Failed to get cs status in periodic job!" << std::endl;			
    }
  }
  
  // when position reference in progress, do not read attributes...
  if (!referencing_in_progress)
  {
    // Update all axes positions
    std::vector<double> axes_pos;

    try 
    {	
      m_hw_interface->get_axes_positions(m_axisList, axes_pos);

      // Update m_axisMap with all position
      for (size_t idx = 0; idx < axes_pos.size(); idx++)
      {
        m_axisMap[m_axisList[idx]].position = axes_pos[idx];
      }
    }
    catch(...)
    {
      m_axesPosError = true;
      ERROR_STREAM << "Failed to get axes positions in periodic job!" << std::endl;
    }

    // Parse the axis map to get offset informations
    for (itAxis = this->m_axisMap.begin(); itAxis != this->m_axisMap.end(); ++itAxis)
    {
      axis_id = itAxis->first;
      double offset = yat::IEEE_NAN;
      try 
      {
        offset = m_hw_interface->get_axis_pos_offset(axis_id);
      } 
      catch(...)
      {
        ERROR_STREAM << "Failed to get offset in periodic job!" << std::endl;
      }
      (itAxis->second).offset = offset;
    }
  }
}

// ============================================================================
// ControllerInterface_ctl::get_status_details
// ============================================================================
sgonpmaclib::T_cs_status ControllerInterface_ctl::get_status_details(sgonpmaclib::T_cs_id id)
{
  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  if (this->m_csMap.count(id) != 0)
  {
    return this->m_csMap[id].status;
  }
  else 
  {
     THROW_DEVFAILED(
	_CPTC("DEVICE_ERROR"), 
	_CPTC("Failed to get status details - Undeclared cs!"), 
	_CPTC("ControllerInterface_ctl::get_status_details")); 
  }

  // for compil warning
  sgonpmaclib::T_cs_status dflt;
  return dflt;
}

// ============================================================================
// ControllerInterface_ctl::get_connection_state
// ============================================================================
void ControllerInterface_ctl::get_connection_state(bool &isConnected)
{
  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  isConnected = m_isConnected;
}

// ============================================================================
// ControllerInterface_ctl::get_position
// ============================================================================
double ControllerInterface_ctl::get_position(sgonpmaclib::T_axis_id id)
{
  yat::AutoMutex<> guard(ControllerInterface::m_lock);

  if (m_axesPosError)
  {
    THROW_DEVFAILED(
	_CPTC("DEVICE_ERROR"), 
	_CPTC("Failed to get axis position - periodic update error!"), 
	_CPTC("ControllerInterface_ctl::get_position"));    
  }

  double pos = yat::IEEE_NAN;

  if (this->m_axisMap.count(id) != 0)
  {
    pos = this->m_axisMap[id].position;
  }
  else 
  {
    THROW_DEVFAILED(
	_CPTC("DEVICE_ERROR"), 
	_CPTC("Failed to get position - Undeclared axis!"), 
	_CPTC("ControllerInterface_ctl::get_position")); 
  }

  return pos;
}

// ============================================================================
// ControllerInterface_ctl::get_offset
// ============================================================================
double ControllerInterface_ctl::get_offset(sgonpmaclib::T_axis_id id)
{
  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  if (this->m_axisMap.count(id) != 0)
  {
    return this->m_axisMap[id].offset;
  }
  else 
  {
    THROW_DEVFAILED(
	_CPTC("DEVICE_ERROR"), 
	_CPTC("Failed to get offset - Undeclared axis!"), 
	_CPTC("ControllerInterface_ctl::get_offset")); 
  }

  return yat::IEEE_NAN; // should not be here, return value for compilation warning
}

} // namespace sgonppmac_ns



//=============================================================================
// ControllerInterface_ctl.h
//=============================================================================
// class.............Controler Interface for Critical Data (asynchronous)
// original author...F. THIAM
//=============================================================================

#ifndef _CONTROLLER_INTERFACE_CTL_H_
#define _CONTROLLER_INTERFACE_CTL_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <yat4tango/InnerAppender.h>
#include <yat4tango/DeviceTask.h>
#include "SGonPPMACTypesAndConsts.h"
#include "ControllerInterface.h"


namespace sgonppmac_ns {


// ============================================================================
// class: ControllerInterface
// ============================================================================
class ControllerInterface_ctl : public ControllerInterface
{

public:

	
  // Constructor.
  ControllerInterface_ctl(Tango::DeviceImpl * hostDevice);

  // Destructor.
  virtual ~ControllerInterface_ctl();

	
  //---------------------------------------------------------------------------
  // Controller management
  //---------------------------------------------------------------------------
  
	// Gets the controller's connection state (ok/nok).
  // @param isConnected  (Out) true or false
  virtual void get_connection_state(bool &isConnected);

	// Gets current axis absolute position, in user unit.
  // @param id Axis id (cs, axis name).
  // @return Current axis position in user unit.
	virtual double get_position(sgonpmaclib::T_axis_id id);

  // Gets current axis position offset, in user unit.
  // @param id Axis id (cs, axis name).
  // @return Current axis position offset in user unit.
	virtual double get_offset(sgonpmaclib::T_axis_id id);
	
  // Gets current CS status.
  // @param id CS id.
  // @return CS status.
	virtual sgonpmaclib::T_cs_status get_status_details(sgonpmaclib::T_cs_id id);

protected:
	//- periodic job
  virtual void periodic_job_i ();

  //- axes state error flag
  bool m_axesStateError;

  //- axes position error flag
  bool m_axesPosError;
};

} // namespace sgonppmac_ns

#endif // _CONTROLLER_INTERFACE_CTL_H_

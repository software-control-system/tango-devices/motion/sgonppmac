//=============================================================================
// ControllerInterface_std.cpp
//=============================================================================
// class.............Controler Interface for Standard Data (asynchronous)
// original author...F. THIAM
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <sstream>
#include "ControllerInterface_std.h"

// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================


namespace sgonppmac_ns {

// ============================================================================
//- Check hw interface pointer:
#define CHECK_HW_INTERFACE \
  if (!m_hw_interface) \
  { \
     THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
                     _CPTC("request aborted - the controller isn't accessible "), \
                     _CPTC("ControllerInterface_std::CHECK_HW_INTERFACE()")); \
  }
// ============================================================================

// ============================================================================
// ControllerInterface_std::ControllerInterface_std
// ============================================================================
ControllerInterface_std::ControllerInterface_std(Tango::DeviceImpl * hostDevice)
	: ControllerInterface(hostDevice)
{
  m_boxStateError = false;
  m_referencing_status = sgonpmaclib::CS_REF_POS_UNKNOWN;
}

// ============================================================================
// ControllerInterface_std::~ControllerInterface_std
// ============================================================================
ControllerInterface_std::~ControllerInterface_std ()
{
  this->enable_periodic_msg(false);
}

// ============================================================================
// ControllerInterface_std::periodic_job_i
// ============================================================================
void ControllerInterface_std::periodic_job_i ()
{
  CHECK_HW_INTERFACE;

  // when position reference in progress, do not read attributes...
  if (m_referencing_status != sgonpmaclib::CS_REF_POS_RUNNING)
  {
    // box status
    m_boxStateError = false;
    sgonpmaclib::T_ppmac_brick_status box_st;
    try 
    {
      box_st = m_hw_interface->get_raw_status();
      {
        yat::AutoMutex<> guard(ControllerInterface::m_lock);
        m_boxStatus = box_st;
      }
    } 
    catch(...)
    {
      m_boxStateError = true;
      ERROR_STREAM << "Failed to get box raw status in periodic job!" << std::endl;
    }

    // cpu temperature
    double temp = yat::IEEE_NAN;
    try 
    {
      temp = m_hw_interface->get_cpu_temperature();
      {
        yat::AutoMutex<> guard(ControllerInterface::m_lock);
        m_cpuTemp = temp;
      }
    } 
    catch(...)
    {
      ERROR_STREAM << "Failed to get cpu temperature in periodic job! Will retry next time..." << std::endl;
    }

    // Cpu running time
    double cpu_time = yat::IEEE_NAN;
    try 
    {
      cpu_time = m_hw_interface->get_cpu_running_time();
      {
        yat::AutoMutex<> guard(ControllerInterface::m_lock);
        m_cpuRunningTime = cpu_time;
      }
    } 
    catch(...)
    {
      ERROR_STREAM << "Failed to get cpu running time in periodic job! Will retry next time..." << std::endl;
    }

    // Cpu phase usage
    double cpu_usage = yat::IEEE_NAN;
    try 
    {
      cpu_usage = m_hw_interface->get_cpu_phase_usage();
      {
        yat::AutoMutex<> guard(ControllerInterface::m_lock);
        m_cpuPhaseUsage = cpu_usage;
      }
    } 
    catch(...)
    {
      ERROR_STREAM << "Failed to get cpu phase usage in periodic job! Will retry next time..." << std::endl; 
    }

    // Cpu servo usage
    try 
    {
      cpu_usage = m_hw_interface->get_cpu_servo_usage();
      {
        yat::AutoMutex<> guard(ControllerInterface::m_lock);
        m_cpuServoUsage = cpu_usage;
      }
    } 
    catch(...)
    {
      ERROR_STREAM << "Failed to get cpu servo usage in periodic job! Will retry next time..." << std::endl;
    }

    // Cpu rti usage
    try 
    {
      cpu_usage = m_hw_interface->get_cpu_rti_usage();
      {
        yat::AutoMutex<> guard(ControllerInterface::m_lock);
        m_cpuRtiUsage = cpu_usage;
      }
    } 
    catch(...)
    {
      ERROR_STREAM << "Failed to get cpu rti usage in periodic job! Will retry next time..." << std::endl;
    }

    // Cpu bg usage
    try 
    {
      cpu_usage = m_hw_interface->get_cpu_bg_usage();
      {
        yat::AutoMutex<> guard(ControllerInterface::m_lock);
        m_cpuBgUsage = cpu_usage;
      }
    } 
    catch(...)
    {
      ERROR_STREAM << "Failed to get cpu bg usage in periodic job! Will retry next time..." << std::endl;
    }

    // For all axis id
    AxisParamsMap_it_t itAxis;
    sgonpmaclib::T_axis_id  axis_id;
  	
    {
      // lock the mutex before parse and update the map
      yat::AutoMutex<> guard(ControllerInterface::m_lock);

      // Parse the axis map to get all informations
      for (itAxis = this->m_axisMap.begin(); itAxis != this->m_axisMap.end(); ++itAxis)
      {
        axis_id = itAxis->first;

        // Get axis compensation state
        bool compensation = false;
        try 
        {
          compensation = m_hw_interface->get_axis_compensation(axis_id);
        } 
        catch(...)
        {
          ERROR_STREAM << "Failed to get compensation state in periodic job! Will retry next time..." << std::endl;
        }

        // data not available for smargon case :
        double accuracy = yat::IEEE_NAN;
        double backlash = yat::IEEE_NAN;
        bool forward = false;
        bool backward = false;
        double maxSoftLimit = yat::IEEE_NAN;
        double minSoftLimit = yat::IEEE_NAN;
        
        // Update m_axisMap
        (itAxis->second).accuracy = accuracy;
        (itAxis->second).backlash = backlash;
        (itAxis->second).forward_limit = forward;
        (itAxis->second).backward_limit = backward;
        (itAxis->second).max_pos_limit = maxSoftLimit;
        (itAxis->second).min_pos_limit = minSoftLimit;
        (itAxis->second).compensation = compensation;
        }
      }
  }


  // For all Cs id
  CdParamsMap_it_t itCs;
  sgonpmaclib::T_cs_id cs_id;

  {
    // lock the mutex before parse and update the map
    yat::AutoMutex<> guard(ControllerInterface::m_lock);
	
    // Parse the cs map to get all informations
    for (itCs = this->m_csMap.begin(); itCs != this->m_csMap.end(); ++itCs)
    {
      cs_id = itCs->first;

      // Get referencing status
      sgonpmaclib::E_cs_referencing_status_t ref_status = sgonpmaclib::CS_REF_POS_UNKNOWN;
      try 
      {
        ref_status = m_hw_interface->get_cs_ref_status(cs_id);
        m_referencing_status = ref_status;
      } 
      catch(...)
      {
        ERROR_STREAM << "Failed to get cs referencing status in periodic job! Will retry next time..." << std::endl;			
      }

      // Get acceleration
      double accel = yat::IEEE_NAN;
      try 
      {
        if (m_referencing_status != sgonpmaclib::CS_REF_POS_RUNNING)
          accel = m_hw_interface->get_cs_acceleration(cs_id);
      } 
      catch(...)
      {
        ERROR_STREAM << "Failed to get acceleration in periodic job! Will retry next time..." << std::endl;			
      }

      // Get deceleration
      double decel = yat::IEEE_NAN;
      try 
      {
        if (m_referencing_status != sgonpmaclib::CS_REF_POS_RUNNING)
          decel = m_hw_interface->get_cs_deceleration(cs_id);
      } 
      catch(...)
      {
        ERROR_STREAM << "Failed to get deceleration in periodic job! Will retry next time..." << std::endl;			
      }

      // Get velocity
      double velocity = yat::IEEE_NAN;
      try 
      {
        if (m_referencing_status != sgonpmaclib::CS_REF_POS_RUNNING)
          velocity = m_hw_interface->get_cs_velocity(cs_id);
      } 
      catch(...)
      {
        ERROR_STREAM << "Failed to get velocity in periodic job! Will retry next time..." << std::endl;			
      }

      // Get moving mode
      sgonpmaclib::E_moving_mode_t mv_mode = sgonpmaclib::CS_MV_MODE_RAPID;
      try 
      {
        if (m_referencing_status != sgonpmaclib::CS_REF_POS_RUNNING)
          mv_mode = m_hw_interface->get_cs_moving_mode(cs_id);
      } 
      catch(...)
      {
        ERROR_STREAM << "Failed to get moving mode in periodic job! Will retry next time..." << std::endl;			
      }
      
      // Get compensation tables validity
      bool comp_valid = false;
      try 
      {
        if (m_referencing_status != sgonpmaclib::CS_REF_POS_RUNNING)
          comp_valid = m_hw_interface->get_cs_compens_valid(cs_id);
      } 
      catch(...)
      {
        ERROR_STREAM << "Failed to get velocity in periodic job! Will retry next time..." << std::endl;			
      }

      // Get moving time : not available with smargon
      double mv_time = yat::IEEE_NAN;    

      // Update m_csMap
      (itCs->second).ref_status = ref_status;
      (itCs->second).acceleration = accel;
      (itCs->second).deceleration = decel;
      (itCs->second).velocity = velocity;
      (itCs->second).mv_mode = mv_mode;
      (itCs->second).mv_time = mv_time;
      (itCs->second).comp_valid = comp_valid;
    }
  }
}

// ============================================================================
// ControllerInterface_std::get_cpu_temp
// ============================================================================
double ControllerInterface_std::get_cpu_temp()
{
  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  return m_cpuTemp;
}

// ============================================================================
// ControllerInterface_std::get_cpu_running_time
// ============================================================================
double ControllerInterface_std::get_cpu_running_time()
{
  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  return m_cpuRunningTime;
}

// ============================================================================
// ControllerInterface_std::get_cpu_phase_usage
// ============================================================================
double ControllerInterface_std::get_cpu_phase_usage()
{
  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  return m_cpuPhaseUsage;
}

// ============================================================================
// ControllerInterface_std::get_cpu_servo_usage
// ============================================================================
double ControllerInterface_std::get_cpu_servo_usage()
{
  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  return m_cpuServoUsage;
}

// ============================================================================
// ControllerInterface_std::get_cpu_rti_usage
// ============================================================================
double ControllerInterface_std::get_cpu_rti_usage()
{
  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  return m_cpuRtiUsage;
}

// ============================================================================
// ControllerInterface_std::get_cpu_bg_usage
// ============================================================================
double ControllerInterface_std::get_cpu_bg_usage()
{
  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  return m_cpuBgUsage;
}

// ============================================================================
// ControllerInterface_std::get_compensation
// ============================================================================
bool ControllerInterface_std::get_compensation(sgonpmaclib::T_axis_id id)
{
  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  if (this->m_axisMap.count(id) != 0)
  {
    return this->m_axisMap[id].compensation;
  }
  else 
  {
    THROW_DEVFAILED(
	_CPTC("DEVICE_ERROR"), 
	_CPTC("Failed to get compensation - Undeclared axis!"), 
	_CPTC("ControllerInterface_std::get_compensation")); 
  }

  return false; // should not be here, return value for compilation warning
}

// ============================================================================
// ControllerInterface_std::get_compens_table_validity
// ============================================================================
bool ControllerInterface_std::get_compens_table_validity(sgonpmaclib::T_cs_id id)
{
  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  if (this->m_csMap.count(id) != 0)
  {
    return this->m_csMap[id].comp_valid;
  }
  else 
  {
    THROW_DEVFAILED(
	_CPTC("DEVICE_ERROR"), 
	_CPTC("Failed to get compensation table validity - Undeclared axis!"), 
	_CPTC("ControllerInterface_std::get_compens_table_validity")); 
  }

  return false; // should not be here, return value for compilation warning
}

// ============================================================================
// ControllerInterface_std::get_accuracy
// ============================================================================
double ControllerInterface_std::get_accuracy(sgonpmaclib::T_axis_id id)
{
  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  if (this->m_axisMap.count(id) != 0)
  {
    return this->m_axisMap[id].accuracy;
  }
  else 
  {
    THROW_DEVFAILED(
	_CPTC("DEVICE_ERROR"), 
	_CPTC("Failed to get accuracy - Undeclared axis!"), 
	_CPTC("ControllerInterface::get_accuracy")); 
  }

  return yat::IEEE_NAN; // should not be here, return value for compilation warning
}

// ============================================================================
// ControllerInterface_std::get_backlash
// ============================================================================
double ControllerInterface_std::get_backlash(sgonpmaclib::T_axis_id id)
{
  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  if (this->m_axisMap.count(id) != 0)
  {
    return this->m_axisMap[id].backlash;
  }
  else 
  {
    THROW_DEVFAILED(
	_CPTC("DEVICE_ERROR"), 
	_CPTC("Failed to get backlash - Undeclared axis!"), 
	_CPTC("ControllerInterface::get_backlash")); 
  }

  return yat::IEEE_NAN; // should not be here, return value for compilation warning
}

// ============================================================================
// ControllerInterface_std::get_forward_limit_switch
// ============================================================================
bool ControllerInterface_std::get_forward_limit_switch(sgonpmaclib::T_axis_id id)
{
  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  if (this->m_axisMap.count(id) != 0)
  {
    return this->m_axisMap[id].forward_limit;
  }
  else 
  {
    THROW_DEVFAILED(
	_CPTC("DEVICE_ERROR"), 
	_CPTC("Failed to get forward limit - Undeclared axis!"), 
	_CPTC("ControllerInterface::get_forward_limit_switch")); 
  }

  return false; // should not be here, return value for compilation warning
}

// ============================================================================
// ControllerInterface_std::get_backward_limit_switch
// ============================================================================
bool ControllerInterface_std::get_backward_limit_switch(sgonpmaclib::T_axis_id id)
{
  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  if (this->m_axisMap.count(id) != 0)
  {
    return this->m_axisMap[id].backward_limit;
  }
  else 
  {
    THROW_DEVFAILED(
	_CPTC("DEVICE_ERROR"), 
	_CPTC("Failed to get backward limit - Undeclared axis!"), 
	_CPTC("ControllerInterface::get_backward_limit_switch")); 
  }

  return false; // should not be here, return value for compilation warning
}

// ============================================================================
// ControllerInterface_std::get_max_soft_limit
// ============================================================================
double ControllerInterface_std::get_max_soft_limit(sgonpmaclib::T_axis_id id)
{
  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  if (this->m_axisMap.count(id) != 0)
  {
    return this->m_axisMap[id].max_pos_limit;
  }
  else 
  {
    THROW_DEVFAILED(
	_CPTC("DEVICE_ERROR"), 
	_CPTC("Failed to get max soft limit - Undeclared axis!"), 
	_CPTC("ControllerInterface::get_max_soft_limit")); 
  }

  return yat::IEEE_NAN; // should not be here, return value for compilation warning
}

// ============================================================================
// ControllerInterface_std::get_min_soft_limit
// ============================================================================
double ControllerInterface_std::get_min_soft_limit(sgonpmaclib::T_axis_id id)
{
  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  if (this->m_axisMap.count(id) != 0)
  {
    return this->m_axisMap[id].min_pos_limit;
  }
  else 
  {
    THROW_DEVFAILED(
	_CPTC("DEVICE_ERROR"), 
	_CPTC("Failed to get min soft limit - Undeclared axis!"), 
	_CPTC("ControllerInterface::get_min_soft_limit")); 
  }

  return yat::IEEE_NAN; // should not be here, return value for compilation warning
}

// ============================================================================
// ControllerInterface_std::get_acceleration
// ============================================================================
double ControllerInterface_std::get_acceleration(sgonpmaclib::T_cs_id id)
{
  double accel = yat::IEEE_NAN;

  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  if (this->m_csMap.count(id) != 0)
  {
    accel = this->m_csMap[id].acceleration;
  }
  else 
  {
      THROW_DEVFAILED(
		_CPTC("DEVICE_ERROR"), 
		_CPTC("Failed to get acceleration - Undeclared cs!"), 
		_CPTC("ControllerInterface::get_acceleration")); 
  }

  return accel;
}

// ============================================================================
// ControllerInterface_std::get_deceleration
// ============================================================================
double ControllerInterface_std::get_deceleration(sgonpmaclib::T_cs_id id)
{
  double decel = yat::IEEE_NAN;

  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  if (this->m_csMap.count(id) != 0)
  {
    decel = this->m_csMap[id].deceleration;
  }
  else 
  {
    THROW_DEVFAILED(
		_CPTC("DEVICE_ERROR"), 
		_CPTC("Failed to get deceleration - Undeclared cs!"), 
		_CPTC("ControllerInterface::get_deceleration")); 
  }

  return decel;
}

// ============================================================================
// ControllerInterface_std::get_velocity
// ============================================================================
double ControllerInterface_std::get_velocity(sgonpmaclib::T_cs_id id)
{
  double vel = yat::IEEE_NAN;

  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  if (this->m_csMap.count(id) != 0)
  {
    vel = this->m_csMap[id].velocity;
  }
  else 
  {
    THROW_DEVFAILED(
	_CPTC("DEVICE_ERROR"), 
	_CPTC("Failed to get velocity - Undeclared cs!"), 
	_CPTC("ControllerInterface::get_velocity")); 
  }
  
  return vel;
}

// ============================================================================
// ControllerInterface_std::get_cs_moving_mode
// ============================================================================
sgonpmaclib::E_moving_mode_t ControllerInterface_std::get_cs_moving_mode(sgonpmaclib::T_cs_id id)
{
  sgonpmaclib::E_moving_mode_t mode = sgonpmaclib::CS_MV_MODE_RAPID;

  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  if (this->m_csMap.count(id) != 0)
  {
    mode = this->m_csMap[id].mv_mode;
  }
  else 
  {
    THROW_DEVFAILED(
	_CPTC("DEVICE_ERROR"), 
	_CPTC("Failed to get cs moving mode - Undeclared cs!"), 
	_CPTC("ControllerInterface::get_cs_moving_mode")); 
  }

  return mode;
}
  
// ============================================================================
// ControllerInterface_std::get_cs_moving_time
// ============================================================================
double ControllerInterface_std::get_cs_moving_time(sgonpmaclib::T_cs_id id)
{
  double duration = yat::IEEE_NAN;

  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  if (this->m_csMap.count(id) != 0)
  {
    duration = this->m_csMap[id].mv_time;
  }
  else 
  {
    THROW_DEVFAILED(
	_CPTC("DEVICE_ERROR"), 
	_CPTC("Failed to get cs moving time - Undeclared cs!"), 
	_CPTC("ControllerInterface::get_cs_moving_time")); 
  }

  return duration;
}

// ============================================================================
// ControllerInterface_std::get_ref_status
// ============================================================================
sgonpmaclib::E_cs_referencing_status_t ControllerInterface_std::get_ref_status(sgonpmaclib::T_cs_id id)
{
  sgonpmaclib::E_cs_referencing_status_t ref_status = sgonpmaclib::CS_REF_POS_UNKNOWN;

  yat::AutoMutex<> guard(ControllerInterface::m_lock);
  if (this->m_csMap.count(id) != 0)
  {
    ref_status = this->m_csMap[id].ref_status;
  }
  else 
  {
    THROW_DEVFAILED(
	_CPTC("DEVICE_ERROR"), 
	_CPTC("Failed to get cs referencing status - Undeclared cs!"), 
	_CPTC("ControllerInterface::get_ref_status")); 
  }

  return ref_status;
}

// ============================================================================
// ControllerInterface_std::get_box_status
// ============================================================================
sgonpmaclib::T_ppmac_brick_status ControllerInterface_std::get_box_status()
{
  yat::AutoMutex<> guard(ControllerInterface::m_lock);

  if (m_boxStateError)
  {
    THROW_DEVFAILED(
	_CPTC("DEVICE_ERROR"), 
	_CPTC("Failed to get box state - periodic update error!"), 
	_CPTC("ControllerInterface_std::get_box_status"));    
  }

  return m_boxStatus;
}

} // namespace sgonppmac_ns



//=============================================================================
// ControllerInterface_std.h
//=============================================================================
// class.............Controler Interface for Standard Data (asynchronous)
// original author...F. THIAM
//=============================================================================

#ifndef _CONTROLLER_INTERFACE_STD_H_
#define _CONTROLLER_INTERFACE_STD_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <yat4tango/InnerAppender.h>
#include <yat4tango/DeviceTask.h>
#include "SGonPPMACTypesAndConsts.h"
#include "ControllerInterface.h"


namespace sgonppmac_ns {


// ============================================================================
// class: ControllerInterface
// ============================================================================
class ControllerInterface_std : public ControllerInterface
{

public:

	
  // Constructor.
  ControllerInterface_std(Tango::DeviceImpl * hostDevice);

  // Destructor.
  virtual ~ControllerInterface_std();

	
  //---------------------------------------------------------------------------
  // Controller management
  //---------------------------------------------------------------------------

	// Gets the controller status word.
  // @return PPMAC controller current status (binary word)
	virtual sgonpmaclib::T_ppmac_brick_status get_box_status();
  
	// Gets the CPU temperature.
  // @return CPU temperature in Celsius degrees
  virtual double get_cpu_temp();
	
  // Gets CPU running time from power-on to present, in seconds.
  // @return Running time (s). 
  virtual double get_cpu_running_time();

  // Gets CPU usage to complete tasks in last phase cycle, in �sec.
  // @return Phase usage. 
  virtual double get_cpu_phase_usage();

  // Gets CPU usage to complete tasks in last servo cycle, in �sec.
  // @return So usage. 
  virtual double get_cpu_servo_usage();

  // Gets CPU usage to complete tasks in last RTI cycle, in �sec.
  // @return RTI usage. 
  virtual double get_cpu_rti_usage();

  // Gets CPU usage to complete tasks in last BG cycle, in �sec.
  // @return BG usage. 
  virtual double get_cpu_bg_usage();
  
  // Gets current axis accuracy, in user unit.
  // @param id Axis id (cs, axis name).
  // @return Current axis accuracy, in user unit.
	virtual double get_accuracy(sgonpmaclib::T_axis_id id);

  // Gets current axis backlash correction.
  // @param id Axis id (cs, axis name).
  // @return Current backlash value, in motor unit.
	virtual double get_backlash(sgonpmaclib::T_axis_id id);

	// Gets current motor forward limit switch state.
  // @param id Axis id (cs, axis name).
  // @return Motor forward limit switch is enabled or disabled.
	virtual bool get_forward_limit_switch(sgonpmaclib::T_axis_id id);

	// Gets current motor backward limit switch state.
  // @param id Axis id (cs, axis name).
  // @return Motor backward limit switch is enabled or disabled.
	virtual bool get_backward_limit_switch(sgonpmaclib::T_axis_id id);

	// Gets current axis max software limit, in user unit.
  // @param id Axis id (cs, axis name).
  // @return Current max software limit in user unit.
	virtual double get_max_soft_limit(sgonpmaclib::T_axis_id id);
  
  // Gets current axis min software limit, in user unit.
  // @param id Axis id (cs, axis name).
  // @return Current min software limit in user unit.
	virtual double get_min_soft_limit(sgonpmaclib::T_axis_id id);

  // Gets current axis acceleration, in user unit/s^2.
  // @param id CS id.
  // @return Current axis acceleration, in user unit/s^2.
	virtual double get_acceleration(sgonpmaclib::T_cs_id id);

  // Gets current axis deceleration, in user unit/s^2.
  // @param id CS id.
  // @return Current axis deceleration, in user unit/s^2.
	virtual double get_deceleration(sgonpmaclib::T_cs_id id);

  // Gets current axis velocity, in user unit/s.
  // @param id CS id.
  // @return Current axis velocity, in user unit/s.
	virtual double get_velocity(sgonpmaclib::T_cs_id id);

	// Gets current axis moving mode.
  // @param id CS id.
  // @return Current axis moving mode.
  virtual sgonpmaclib::E_moving_mode_t get_cs_moving_mode(sgonpmaclib::T_cs_id id);

  // Gets current axis moving mode.
  // @param id CS id.
  // @return Current axis moving time, in ms.
  virtual double get_cs_moving_time(sgonpmaclib::T_cs_id id);

  // Gets current referencing status.
  // @param id CS id.
  // @return referencing status.
  virtual sgonpmaclib::E_cs_referencing_status_t get_ref_status(sgonpmaclib::T_cs_id id);

  // Gets validity of compensation tables.
  // @param id CS id.
  // @return Current validity of compensation tables.
  virtual bool get_compens_table_validity(sgonpmaclib::T_cs_id id);

  // Gets current axis compensation: enabled or disabled.
  // @param id Axis id (cs, axis name).
  // @return Current axis compensation state.
  virtual bool get_compensation(sgonpmaclib::T_axis_id id);

protected:

	//- periodic job
  virtual void periodic_job_i ();

  //- box state error flag
  bool m_boxStateError;

  //- current referencing status
  sgonpmaclib::E_cs_referencing_status_t m_referencing_status;

};

} // namespace sgonppmac_ns

#endif // _CONTROLLER_INTERFACE_STD_H_

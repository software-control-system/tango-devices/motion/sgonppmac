//=============================================================================
// DynComposedAxisAttrib.h
//=============================================================================
// class.............DynComposedAxisAttrib
// original author...F. THIAM
//=============================================================================

#ifndef _DYNAMIC_COMPOSED_AXIS_ATTRIB_H_
#define _DYNAMIC_COMPOSED_AXIS_ATTRIB_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================

#include <tango.h>
#include <yat/any/Any.h>

namespace sgonppmac_ns
{
	template <class T>
	class DynComposedAxisAttrib : public Tango::Attr
	{
	public:
		// Typedef for callback read and write
		typedef void (T::*CallbackReadDynAttr_t)(DynComposedAxisAttrib *, Tango::Attribute &);
		typedef void (T::*CallbackWriteDynAttr_t)(DynComposedAxisAttrib *, Tango::WAttribute &);

		// Constructor
		DynComposedAxisAttrib(char const *name, long data_type, Tango::AttrWriteType w_type)
			: Tango::Attr(name, data_type, w_type), m_cbRead(NULL), m_cbWrite(NULL)
		{
		}

		// Destructor
		virtual ~DynComposedAxisAttrib(void) 
		{
		}

		// Read
		virtual void read(Tango::DeviceImpl *dev, Tango::Attribute &att)
		{
			if(m_cbRead) 
			{
				// Call the registred callback read
				((static_cast<T *>(dev))->*(m_cbRead))(this, att);
			}
		}

		// Write
		virtual void write(Tango::DeviceImpl *dev, Tango::WAttribute &att)
		{
			if( m_cbWrite ) 
			{
				// Call the registred callback write
				((static_cast<T *>(dev))->*(m_cbWrite))(this, att);
			}
		}

		// is_allowed
		virtual bool is_allowed(Tango::DeviceImpl *dev,	Tango::AttReqType ty)
		{
			return true;
		}

		// Set the callback read
		void setReadCallback(CallbackReadDynAttr_t cbRead)
		{
			m_cbRead = cbRead;
		}

		// Set the callback write
		void setWriteCallback(CallbackWriteDynAttr_t cbWrite)
		{
			m_cbWrite = cbWrite;
		}

		//! \brief Sets the user data.
		//! \param d Pointer to the user data (generic type \<Tuad\>).
		template <typename Tuad> void set_user_data (Tuad* d);

		//! \brief Sets the user data.
		//! \param d Pointer to the user data (generic type \<Tuad\>).
		template <typename Tuad> void set_user_data (const Tuad & d);

		//! \brief Returns the user data (returns NULL is the user dat is not of type Tuad)
		template <typename Tuad> Tuad* get_user_data ();

		//! \brief Returns the user data.
		template <typename Tuad> void get_user_data (Tuad& ud);

	protected:
		//- callback read pointer registred
		CallbackReadDynAttr_t m_cbRead;

		//- callback write pointer registred
		CallbackWriteDynAttr_t m_cbWrite;
		
		//- user 'abstract' data
		yat::Any uad;
	};

	// ============================================================================
	// DynComposedAxisAttrib::set_user_data
	// ============================================================================
	template <class T>
	template <typename Tuad>
	void DynComposedAxisAttrib<T>::set_user_data (Tuad* d)
	{
		uad = d;
	}

	// ============================================================================
	// DynComposedAxisAttrib::set_user_data
	// ============================================================================
	template <class T>
	template <typename Tuad>
	void DynComposedAxisAttrib<T>::set_user_data (const Tuad & d)
	{
		uad = d;
	}

	// ============================================================================
	// DynComposedAxisAttrib::get_user_data
	// ============================================================================
	template <class T>
	template <typename Tuad>
	Tuad * DynComposedAxisAttrib<T>::get_user_data ()
	{
		try
		{
			return yat::any_cast<Tuad*>(uad);
		}
		catch (const yat::Exception&)
		{
			return 0;
		}
	}

	// ============================================================================
	// DynComposedAxisAttrib::get_user_data
	// ============================================================================
	template <class T>
	template <typename Tuad>
	void DynComposedAxisAttrib<T>::get_user_data (Tuad& ud)
	{
		ud = yat::any_cast<Tuad>(uad);
	}

	

} // namespace sgonppmac_ns

#endif // _DYNAMIC_COMPOSED_AXIS_ATTRIB_H_

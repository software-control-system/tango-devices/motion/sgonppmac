//=============================================================================
// DynamicAttrDeviceInfo.h
//=============================================================================
// class.............DynamicAttrDeviceInfo
// original author...F. THIAM
//=============================================================================

#ifndef _DYNAMIC_ATTR_DEVICE_INFO_H_
#define _DYNAMIC_ATTR_DEVICE_INFO_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================


namespace sgonppmac_ns {


// ============================================================================
// class: DynamicAttrDeviceInfo
// ============================================================================
class DynamicAttrDeviceInfo
{

public:

	// Constructor.
	DynamicAttrDeviceInfo()
	{
	}

	// Destructor.
  virtual ~DynamicAttrDeviceInfo()
	{
	}

	// Init the dynamic params
	virtual void init_dynamic_params() = 0;
	
	// Remove the dynamic params
	virtual void remove_dynamic_params() = 0;

};

} // namespace sgonppmac_ns

#endif // _DYNAMIC_ATTR_DEVICE_INFO_H_

//=============================================================================
// DynamicAttrMgr.cpp
//=============================================================================
// class.............DynamicAttrMgr
// original author...F. THIAM
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <sstream>
#include "DynamicAttrMgr.h"

// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================


namespace sgonppmac_ns {

// ============================================================================
// SINGLETON
// ============================================================================
DynamicAttrMgr * DynamicAttrMgr::singleton = 0;

// ============================================================================
// DynamicAttrMgr::init <STATIC MEMBER>
// ============================================================================
void DynamicAttrMgr::init (size_t nbDevice) 
{
  //- already instanciated?
  if (DynamicAttrMgr::singleton)
    return;

  try
  {
  	//- actual instanciation
    DynamicAttrMgr::singleton = new DynamicAttrMgr();
    if (DynamicAttrMgr::singleton == 0)
      throw std::bad_alloc();
		DynamicAttrMgr::singleton->set_nb_device(nbDevice);
		
  }
  catch (const std::bad_alloc&)
  {
    THROW_DEVFAILED(_CPTC("OUT_OF_MEMORY"),
                    _CPTC("DynamicAttrMgr::singleton instanciation failed"),
                    _CPTC("DynamicAttrMgr::init"));	
  }
  catch (...)
  {
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("unknown exception caught while while trying to instanciate the DynamicAttrMgr::singleton"),
                    _CPTC("DynamicAttrMgr::init"));
  }
}

// ============================================================================
// DynamicAttrMgr::close  <STATIC MEMBER>
// ============================================================================
void DynamicAttrMgr::close ()
{
  if (! DynamicAttrMgr::singleton)
    return;

	if (DynamicAttrMgr::singleton->get_nb_registered_device() > 0)
	{
		// The <DynamicAttrMgr> singleton is not release if there is one or more device registered
		return;
	}

  try
  {
    //- release the <DynamicAttrMgr> singleton
    SAFE_DELETE_PTR(DynamicAttrMgr::singleton);
  }
  catch (Tango::DevFailed& df)
  {
    RETHROW_DEVFAILED(df,
                      _CPTC("SOFTWARE_ERROR"),
                      _CPTC("Tango exception caught while trying to shutdown the DynamicAttrMgr::singleton"),
                      _CPTC("DynamicAttrMgr::close"));
  }
  catch (...)
  {
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
                    _CPTC("unknown exception caught while while trying to shutdown the DynamicAttrMgr::singleton"),
                    _CPTC("DynamicAttrMgr::close"));
  }
}

// ============================================================================
// DynamicAttrMgr::DynamicAttrMgr
// ============================================================================
DynamicAttrMgr::DynamicAttrMgr ()
{
	m_nbDevice = 0;
}

// ============================================================================
// DynamicAttrMgr::~DynamicAttrMgr
// ============================================================================
DynamicAttrMgr::~DynamicAttrMgr (void)
{
	
}

// ============================================================================
// DynamicAttrMgr::set_nb_device
// ============================================================================
void DynamicAttrMgr::set_nb_device(size_t nbDevice)
{
	yat::AutoMutex<> guard(DynamicAttrMgr::m_lock);
	m_nbDevice = nbDevice;
}

// ============================================================================
// DynamicAttrMgr::get_nb_registered_device
// ============================================================================
size_t DynamicAttrMgr::get_nb_registered_device()
{
	yat::AutoMutex<> guard(DynamicAttrMgr::m_lock);
	return m_mapDevice.size();
}

// ============================================================================
// DynamicAttrMgr::add_device
// ============================================================================
void DynamicAttrMgr::add_device(DynamicAttrDeviceInfo *attrDevice)
{
	yat::AutoMutex<> guard(DynamicAttrMgr::m_lock);

	if (m_mapDevice.count(attrDevice) == 0) 
	{
		// Add DynamicAttrDeviceInfo if not exist in map
		m_mapDevice.insert(std::pair<DynamicAttrDeviceInfo *, bool>(attrDevice, false ));
	}

	if (m_mapDevice.size() == m_nbDevice)
	{
		// For each registred DynamicAttrDeviceInfo call the method init_dynamic_params

		DynamicAttrDeviceMap_it_t itDev;
		for (itDev = this->m_mapDevice.begin(); itDev != this->m_mapDevice.end(); ++itDev)
		{
			// Call the init_dynamic_params if the DynamicAttrDeviceInfo is not already enabled
			if(itDev->second == false )
			{
				itDev->first->init_dynamic_params();

				itDev->second = true;
			}
		}
	}
}

// ============================================================================
// DynamicAttrMgr::remove_device
// ============================================================================
void DynamicAttrMgr::remove_device(DynamicAttrDeviceInfo *attrDevice)
{
	yat::AutoMutex<> guard(DynamicAttrMgr::m_lock);

	if (m_mapDevice.count(attrDevice) == 0) 
	{
		// Do not nothing if the map is empty
		return;
	}
	else
	{
		bool bInitDyn = m_mapDevice[attrDevice];
		if( bInitDyn ) 
		{
			// Call the remove_dynamic_params if the DynamicAttrDeviceInfo is not already deactivated
			attrDevice->remove_dynamic_params();

			// Remove the DynamicAttrDeviceInfo from the map
			m_mapDevice[attrDevice] = false;
			m_mapDevice.erase(attrDevice);
		}
	}
}

} // namespace sgonppmac_ns



//=============================================================================
// DynamicAttrMgr.h
//=============================================================================
// class.............DynamicAttrMgr
// original author...F. THIAM
//=============================================================================

#ifndef _DYNAMIC_ATTR_MGR_H_
#define _DYNAMIC_ATTR_MGR_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <sstream>
#include <vector>
#include <string>
#include <map>


/*Use YAT library*/
#include <yat/any/Any.h>
#include <yat4tango/DeviceTask.h>
#include "DynamicAttrDeviceInfo.h"

namespace sgonppmac_ns {

	typedef std::map<DynamicAttrDeviceInfo *, bool> DynamicAttrDeviceMap_t;
	typedef DynamicAttrDeviceMap_t::iterator DynamicAttrDeviceMap_it_t;

// ============================================================================
// class: DynamicAttrMgr
// ============================================================================
class DynamicAttrMgr
{

public:

	//----------------------------------------
  // SINGLETON 
  //----------------------------------------
  static inline DynamicAttrMgr * instance ()
  {
    if (! DynamicAttrMgr::singleton)
        THROW_DEVFAILED(_CPTC("SOFTWARE_ERROR"),
                        _CPTC("unexpected NULL pointer [DynamicAttrMgr singleton not properly initialized]"),
                        _CPTC("DynamicAttrMgr::instance")); 
    return DynamicAttrMgr::singleton;
  }

  //- singleton instanciation --------------
  static void init (size_t nbDevice);

  //- singleton release --------------------
  static void close ();

	//- set the number of DynamicAttrDeviceInfo to be register
	void set_nb_device(size_t nbDevice);

	//- get the number of DynamicAttrDeviceInfo registred
	size_t get_nb_registered_device();

	//- Add a DynamicAttrDeviceInfo in the manager
	void add_device(DynamicAttrDeviceInfo *attrDevice);
	

	//- Remove a DynamicAttrDeviceInfo in the manager
	void remove_device(DynamicAttrDeviceInfo *attrDevice);
	
	
private:
	//- to avoid race condition between ControllerInterface class & periodic_job
  yat::Mutex m_lock;

  //- ctor -------------
  DynamicAttrMgr ();
	
  //- dtor -------------
  virtual ~DynamicAttrMgr ();

  //- the singleton
  static DynamicAttrMgr * singleton;

	//- number of DynamicAttrDeviceInfo to be register
	size_t m_nbDevice;
	
	//- Map of registred DynamicAttrDeviceInfo
	DynamicAttrDeviceMap_t m_mapDevice;

};

} // namespace sgonppmac_ns

#endif // _DYNAMIC_ATTR_MGR_H_

//=============================================================================
// DynamicComposedAxis.cpp
//=============================================================================
// class.............DynamicComposedAxis
// original author...F. THIAM
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <sstream>
#include "DynamicComposedAxis.h"
//#include "ComposedAxis.h"
#include "SGonPPMACComposedAxis.h"

// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================


namespace sgonppmac_ns {

// ============================================================================
// DynamicComposedAxis::DynamicComposedAxis
// ============================================================================
DynamicComposedAxis::DynamicComposedAxis()
	: DynamicAttrDeviceInfo(), m_device(NULL) //m_axis(NULL)
{
}

// ============================================================================
// DynamicComposedAxis::~DynamicComposedAxis
// ============================================================================
DynamicComposedAxis::~DynamicComposedAxis()
{
}

// ============================================================================
// DynamicComposedAxis::setDevice
// ============================================================================
//void DynamicComposedAxis::setComposedAxis(ComposedAxis *axis)
void DynamicComposedAxis::setDevice(SGonPPMACComposedAxis_ns::SGonPPMACComposedAxis *device)
{
	//m_axis = axis;
	m_device = device;
}

// ============================================================================
// DynamicComposedAxis::init_dynamic_params
// ============================================================================
void DynamicComposedAxis::init_dynamic_params()
{
	//if(m_axis)
	//{
//		m_axis->init_dynamic_params();
	//}

	if(m_device)
	{
		m_device->init_dynamic_params();
	}
}	

// ============================================================================
// DynamicComposedAxis::remove_dynamic_params
// ============================================================================
void DynamicComposedAxis::remove_dynamic_params()
{
	/*if(m_axis)
	{
		m_axis->remove_dynamic_params();
	}*/

	if(m_device)
	{
		m_device->remove_dynamic_params();
	}
}



} // namespace sgonppmac_ns



//=============================================================================
// DynamicComposedAxis.h
//=============================================================================
// class.............DynamicComposedAxis
// original author...F. THIAM
//=============================================================================

#ifndef _DYNAMIC_COMPOSED_AXIS_H_
#define _DYNAMIC_COMPOSED_AXIS_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <sstream>
#include <vector>
#include <string>
#include <map>


/*Use YAT library*/
#include <yat/any/Any.h>
#include <yat4tango/DeviceTask.h>
#include "DynamicAttrDeviceInfo.h"

namespace SGonPPMACComposedAxis_ns
{
	class SGonPPMACComposedAxis;
}


namespace sgonppmac_ns {


	class ComposedAxis;

// ============================================================================
// class: DynamicAttrMgr
// ============================================================================
	
class DynamicComposedAxis : public DynamicAttrDeviceInfo
{

public:

	// Constructor.
	DynamicComposedAxis();

	// Destructor.
  virtual ~DynamicComposedAxis();

	//void setComposedAxis(ComposedAxis *axis);
	void setDevice(SGonPPMACComposedAxis_ns::SGonPPMACComposedAxis *device);

	// Init the dynamic params
	virtual void init_dynamic_params();
	
	// Remove the dynamic params
	virtual void remove_dynamic_params();


protected:
	//ComposedAxis *m_axis;
	SGonPPMACComposedAxis_ns::SGonPPMACComposedAxis *m_device;

};

} // namespace sgonppmac_ns

#endif // _DYNAMIC_COMPOSED_AXIS_H_

	
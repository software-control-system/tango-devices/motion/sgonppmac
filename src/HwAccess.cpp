//=============================================================================
// HwAccess.cpp
//=============================================================================
// class.............HwAccess - Hardware access to the sgonpmaclib implementation
// original author...F. THIAM
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <sstream>
#include "HwAccess.h"


namespace sgonppmac_ns {

// ============================================================================
//- Check low level driver pointer:
#define CHECK_PPMAC_ACCESS \
  if (!m_ppmac_access) \
  { \
     THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
                     _CPTC("request aborted - the controller isn't accessible "), \
                     _CPTC("HwAccess::CHECK_PPMAC_ACCESS()")); \
  }
// ============================================================================

// ============================================================================
// HwAccess::HwAccess
// ============================================================================
HwAccess::HwAccess (Tango::DeviceImpl * hostDevice)
:Tango::LogAdapter(hostDevice)
{
  //DEBUG_STREAM << "HwAccess constructor entering..." << std::endl;
  m_ppmac_access = NULL;
}

// ============================================================================
// HwAccess::~HwAccess
// ============================================================================
HwAccess::~HwAccess ()
{
  //DEBUG_STREAM << "HwAccess destructor entering..." << std::endl;

  if (m_ppmac_access)
  {
    // close connection
    try
    {
      m_ppmac_access->close();
    }
    catch(...)
    {
    }

    delete m_ppmac_access;
    m_ppmac_access = NULL;
  }
}

// ============================================================================
// HwAccess::open
// ============================================================================
void HwAccess::open(std::string ipAddress, unsigned int tcpPort)
{
  DEBUG_STREAM << "HwAccess::open() entering..." << std::endl;

  if(!m_ppmac_access) 
  {
	  // create driver
	  m_ppmac_access = new sgonpmaclib::SGonPMAClib();

	  if (!m_ppmac_access)
	  {
			THROW_DEVFAILED(
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("PMAC driver access NOK - Caught DevFailed!"), 
                     _CPTC("HwAccess::open"));
	  }
  }
  
  // open connection
  try
  {
    m_ppmac_access->open(ipAddress, tcpPort);
  }
  catch(sgonpmaclib::Exception& e)
  {
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Cannot open connection to PPMAC driver - Caught DevFailed!"), 
                     _CPTC("HwAccess::open"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Cannot open connection to PPMAC driver - Caught unknown exception!"), 
							_CPTC("HwAccess::open")); 
  }
}

// ============================================================================
// HwAccess::close
// ============================================================================
void HwAccess::close()
{
  DEBUG_STREAM << "HwAccess::close() entering..." << std::endl;

  CHECK_PPMAC_ACCESS;

   // close connection
  try
  {
		m_ppmac_access->close();
	}
	catch(...)
	{
	}

	delete m_ppmac_access;
	m_ppmac_access = NULL;
}

// ============================================================================
// HwAccess::is_connection_opened
// ============================================================================
void HwAccess::get_connection_state(bool &isConnected)
{
  CHECK_PPMAC_ACCESS;

  try 
  {
		isConnected = m_ppmac_access->is_connection_opened();
  }
  catch(sgonpmaclib::Exception& e)
  {
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to get connection state - Caught DevFailed!"), 
                     _CPTC("HwAccess::get_connection_state"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to get connection state - Caught unknown exception!"), 
							_CPTC("HwAccess::get_connection_state")); 
  }	
}

// ============================================================================
// HwAccess::get_raw_status
// ============================================================================
sgonpmaclib::T_ppmac_brick_status HwAccess::get_raw_status()
{
  CHECK_PPMAC_ACCESS;
	sgonpmaclib::T_ppmac_brick_status status;

  try 
  {
		status = m_ppmac_access->get_raw_status();
  }
  catch(sgonpmaclib::Exception& e)
  {
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to get raw status - Caught DevFailed!"), 
                     _CPTC("HwAccess::get_raw_status"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to get raw status - Caught unknown exception!"), 
							_CPTC("HwAccess::get_raw_status")); 
  }

  return status;
}

// ============================================================================
// HwAccess::get_cpu_temperature
// ============================================================================
double HwAccess::get_cpu_temperature()
{
  CHECK_PPMAC_ACCESS;
  double temp = 0.0;

	try 
  {
		temp = m_ppmac_access->get_cpu_temperature();
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to get cpu temperature - Caught DevFailed!"), 
                     _CPTC("HwAccess::get_cpu_temperature"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to get cpu temperature - Caught unknown exception!"), 
							_CPTC("HwAccess::get_cpu_temperature")); 
  }

	return temp;
}
	
// ============================================================================
// HwAccess::get_cpu_running_time
// ============================================================================
double HwAccess::get_cpu_running_time()
{
  CHECK_PPMAC_ACCESS;
  double cpu_time = yat::IEEE_NAN;

  try 
  {
		cpu_time = m_ppmac_access->get_cpu_running_time();
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to get cpu running time - Caught DevFailed!"), 
                     _CPTC("HwAccess::get_cpu_running_time"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to get cpu running time - Caught unknown exception!"), 
							_CPTC("HwAccess::get_cpu_running_time")); 
  }

  return cpu_time;
}

// ============================================================================
// HwAccess::get_cpu_phase_usage
// ============================================================================
double HwAccess::get_cpu_phase_usage()
{
  CHECK_PPMAC_ACCESS;
  double cpu_usage = yat::IEEE_NAN;

  try 
  {
		cpu_usage = m_ppmac_access->get_cpu_phase_usage();
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to get cpu phase usage - Caught DevFailed!"), 
                     _CPTC("HwAccess::get_cpu_phase_usage"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to get cpu phase usage - Caught unknown exception!"), 
							_CPTC("HwAccess::get_cpu_phase_usage")); 
  }

  return cpu_usage;
}

// ============================================================================
// HwAccess::get_cpu_servo_usage
// ============================================================================
double HwAccess::get_cpu_servo_usage()
{
  CHECK_PPMAC_ACCESS;
  double cpu_usage = yat::IEEE_NAN;

  try 
  {
		cpu_usage = m_ppmac_access->get_cpu_servo_usage();
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to get cpu servo usage - Caught DevFailed!"), 
                     _CPTC("HwAccess::get_cpu_servo_usage"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to get cpu servo usage - Caught unknown exception!"), 
							_CPTC("HwAccess::get_cpu_servo_usage")); 
  }

  return cpu_usage;
}

// ============================================================================
// HwAccess::get_cpu_rti_usage
// ============================================================================
double HwAccess::get_cpu_rti_usage()
{
  CHECK_PPMAC_ACCESS;
  double cpu_usage = yat::IEEE_NAN;

  try 
  {
		cpu_usage = m_ppmac_access->get_cpu_rti_usage();
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to get cpu rti usage - Caught DevFailed!"), 
                     _CPTC("HwAccess::get_cpu_rti_usage"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to get cpu rti usage - Caught unknown exception!"), 
							_CPTC("HwAccess::get_cpu_rti_usage")); 
  }

  return cpu_usage;
}

// ============================================================================
// HwAccess::get_cpu_bg_usage
// ============================================================================
double HwAccess::get_cpu_bg_usage()
{
  CHECK_PPMAC_ACCESS;
  double cpu_usage = yat::IEEE_NAN;

  try 
  {
		cpu_usage = m_ppmac_access->get_cpu_bg_usage();
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to get cpu bg usage - Caught DevFailed!"), 
                     _CPTC("HwAccess::get_cpu_bg_usage"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to get cpu bg usage - Caught unknown exception!"), 
							_CPTC("HwAccess::get_cpu_bg_usage")); 
  }

  return cpu_usage;
}

// ============================================================================
// HwAccess::reset
// ============================================================================
void HwAccess::reset()
{
  DEBUG_STREAM << "HwAccess::reset() entering..." << std::endl;

  CHECK_PPMAC_ACCESS;

	try
  {
    m_ppmac_access->reset();
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to reset - Caught DevFailed!"), 
                     _CPTC("HwAccess::reset"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to reset - Caught unknown exception!"), 
							_CPTC("HwAccess::reset")); 
  }
}

// ============================================================================
// HwAccess::reboot_os
// ============================================================================
void HwAccess::reboot()
{
  DEBUG_STREAM << "HwAccess::reboot() entering..." << std::endl;

  CHECK_PPMAC_ACCESS;

	try
  {
    m_ppmac_access->reboot_os();
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to reboot - Caught DevFailed!"), 
                     _CPTC("HwAccess::reboot"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to reboot - Caught unknown exception!"), 
							_CPTC("HwAccess::reboot")); 
  }
}

// ============================================================================
// HwAccess::abort
// ============================================================================
void HwAccess::abort()
{
  DEBUG_STREAM << "HwAccess::abort() entering..." << std::endl;

  CHECK_PPMAC_ACCESS;

	try
  {
    m_ppmac_access->abort();
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to abort - Caught DevFailed!"), 
                     _CPTC("HwAccess::abort"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to abort - Caught unknown exception!"), 
							_CPTC("HwAccess::abort")); 
  }
}

// ============================================================================
// HwAccess::send_low_level_cmd
// ============================================================================
void HwAccess::send_low_level_cmd(std::string cmd, std::string &outParam)
{
  DEBUG_STREAM << "HwAccess::send_low_level_cmd() entering..." << std::endl;
  DEBUG_STREAM << "cmd = " << cmd << std::endl;

  CHECK_PPMAC_ACCESS;
  
	try
  {
    outParam = m_ppmac_access->send_low_level_cmd(cmd);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to send low level cmd - Caught DevFailed!"), 
                     _CPTC("HwAccess::send_low_level_cmd"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to send low level cmd - Caught unknown exception!"), 
							_CPTC("HwAccess::send_low_level_cmd")); 
  }
}

// ============================================================================
// HwAccess::get_firmware_vers
// ============================================================================
std::string HwAccess::get_firmware_vers()
{
  CHECK_PPMAC_ACCESS;
  std::string vers;

  try 
  {
		vers = m_ppmac_access->get_firmware_vers();
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to get firmware version - Caught DevFailed!"), 
                     _CPTC("HwAccess::get_firmware_vers"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to get firmware version - Caught unknown exception!"), 
							_CPTC("HwAccess::get_firmware_vers")); 
  }

  return vers;
}

// ============================================================================
// HwAccess::get_ucode_vers
// ============================================================================
std::string HwAccess::get_ucode_vers()
{
  CHECK_PPMAC_ACCESS;
  std::string vers;

  try 
  {
		vers = m_ppmac_access->get_ucode_vers();
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to get ucode version - Caught DevFailed!"), 
                     _CPTC("HwAccess::get_ucode_vers"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to get ucode version - Caught unknown exception!"), 
							_CPTC("HwAccess::get_ucode_vers")); 
  }

  return vers;
}

// Multi-axes management

// ============================================================================
// HwAccess::get_axes_positions
// ============================================================================
void HwAccess::get_axes_positions(std::vector<sgonpmaclib::T_axis_id> axis_list, std::vector<double> &axes_pos)
{
  CHECK_PPMAC_ACCESS;

	try 
  {
		/*
		for (size_t i = 0; i < axis_list.size(); i++)
		{
			DEBUG_STREAM << "HwAccess::get_axes_positions: i=" << i << " cs=" << axis_list[i].cs << " axis_name=" << axis_list[i].axis_name << std::endl;
		}*/

		axes_pos = m_ppmac_access->get_axes_positions(axis_list);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to get axes positions - Caught DevFailed!"), 
                     _CPTC("HwAccess::get_axes_positions"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to get axes positions - Caught unknown exception!"), 
							_CPTC("HwAccess::get_axes_positions")); 
  }
}

// ============================================================================
// HwAccess::init_axis
// ============================================================================
void HwAccess::init_axis(sgonpmaclib::T_axis_id id)
{
  DEBUG_STREAM << "HwAccess::init_axis() entering..." << std::endl;
  DEBUG_STREAM << "(cs, axis) = (" << id.cs << ", " << id.axis_name << ")" << std::endl;

  CHECK_PPMAC_ACCESS;

	try 
  {
		m_ppmac_access->init_axis(id);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to init axis - Caught DevFailed!"), 
                     _CPTC("HwAccess::init_axis"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to init axis - Caught unknown exception!"), 
							_CPTC("HwAccess::init_axis")); 
  }
}

// ============================================================================
// HwAccess::undeclare_axis
// ============================================================================
void HwAccess::undeclare_axis(sgonpmaclib::T_axis_id id)
{
  DEBUG_STREAM << "HwAccess::undeclare_axis() entering..." << std::endl;
  DEBUG_STREAM << "(cs, axis) = (" << id.cs << ", " << id.axis_name << ")" << std::endl;

  CHECK_PPMAC_ACCESS;

	try 
  {
		m_ppmac_access->undeclare_axis(id);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to undeclare axis - Caught DevFailed!"), 
                     _CPTC("HwAccess::undeclare_axis"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to undeclare axis - Caught unknown exception!"), 
							_CPTC("HwAccess::undeclare_axis")); 
  }
}


// ============================================================================
// HwAccess::get_cs_status
// ============================================================================
sgonpmaclib::T_cs_status HwAccess::get_cs_status(sgonpmaclib::T_cs_id id)
{
  CHECK_PPMAC_ACCESS;
  sgonpmaclib::T_cs_status status;

  try 
  {
		status = m_ppmac_access->get_cs_status(id);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to get cs status - Caught DevFailed!"), 
                     _CPTC("HwAccess::get_cs_status"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to get cs status - Caught unknown exception!"), 
							_CPTC("HwAccess::get_cs_status")); 
  }

  return status;
}


// ============================================================================
// HwAccess::get_cs_motors_status
// ============================================================================
std::string HwAccess::get_cs_motors_status(sgonpmaclib::T_cs_id id)
{
  CHECK_PPMAC_ACCESS;
  std::string status;

  try 
  {
		status = m_ppmac_access->get_cs_motors_status(id);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to get motors status - Caught DevFailed!"), 
                     _CPTC("HwAccess::get_cs_motors_status"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to get motors status - Caught unknown exception!"), 
							_CPTC("HwAccess::get_cs_motors_status")); 
  }

  return status;
}

// ============================================================================
// HwAccess::get_cs_ref_status
// ============================================================================
sgonpmaclib::E_cs_referencing_status_t HwAccess::get_cs_ref_status(sgonpmaclib::T_cs_id id)
{
  CHECK_PPMAC_ACCESS;
  sgonpmaclib::E_cs_referencing_status_t ref_status = sgonpmaclib::CS_REF_POS_UNKNOWN;

  try 
  {
      ref_status = m_ppmac_access->get_cs_ref_status(id);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to get cs status - Caught DevFailed!"), 
                     _CPTC("HwAccess::get_cs_ref_status"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to get cs status - Caught unknown exception!"), 
							_CPTC("HwAccess::get_cs_ref_status")); 
  }

  return ref_status;
}


// ============================================================================
// HwAccess::motor_on
// ============================================================================
void HwAccess::motor_on(sgonpmaclib::T_cs_id id)
{
  DEBUG_STREAM << "HwAccess::motor_on() entering..." << std::endl;
  DEBUG_STREAM << "(cs) = (" << id << ")" << std::endl;

  CHECK_PPMAC_ACCESS;

  try 
  {
		m_ppmac_access->motor_on(id);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to motor on - Caught DevFailed!"), 
                     _CPTC("HwAccess::motor_on"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to motor on - Caught unknown exception!"), 
							_CPTC("HwAccess::motor_on")); 
  }
}

// ============================================================================
// HwAccess::motor_off
// ============================================================================
void HwAccess::motor_off(sgonpmaclib::T_cs_id id)
{
  DEBUG_STREAM << "HwAccess::motor_off() entering..." << std::endl;
  DEBUG_STREAM << "(cs) = (" << id << ")" << std::endl;

  CHECK_PPMAC_ACCESS;

  try 
  {
		m_ppmac_access->motor_off(id);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to motor off - Caught DevFailed!"), 
                     _CPTC("HwAccess::motor_off"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to motor off - Caught unknown exception!"), 
							_CPTC("HwAccess::motor_off")); 
  }
}// ============================================================================
// HwAccess::motor_other_off
// ============================================================================
void HwAccess::motor_other_off(sgonpmaclib::T_cs_id id)
{
  DEBUG_STREAM << "HwAccess::motor_other_off() entering..." << std::endl;
  DEBUG_STREAM << "(cs) = (" << id << ")" << std::endl;

  CHECK_PPMAC_ACCESS;

  try 
  {
		m_ppmac_access->motor_other_off(id);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to motor off - Caught DevFailed!"), 
                     _CPTC("HwAccess::motor_other_off"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to motor off - Caught unknown exception!"), 
							_CPTC("HwAccess::motor_other_off")); 
  }
}// ============================================================================
// HwAccess::motor_omega_off
// ============================================================================
void HwAccess::motor_omega_off(sgonpmaclib::T_cs_id id)
{
  DEBUG_STREAM << "HwAccess::motor_omega_off() entering..." << std::endl;
  DEBUG_STREAM << "(cs) = (" << id << ")" << std::endl;

  CHECK_PPMAC_ACCESS;

  try 
  {
		m_ppmac_access->motor_omega_off(id);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to motor off - Caught DevFailed!"), 
                     _CPTC("HwAccess::motor_omega_off"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to motor off - Caught unknown exception!"), 
							_CPTC("HwAccess::motor_omega_off")); 
  }
}

// ============================================================================
// HwAccess::axis_stop
// ============================================================================
void HwAccess::axis_stop(sgonpmaclib::T_cs_id id)
{
  DEBUG_STREAM << "HwAccess::axis_stop() entering..." << std::endl;
  DEBUG_STREAM << "(cs) = (" << id << ")" << std::endl;

  CHECK_PPMAC_ACCESS;

	try 
  {
		m_ppmac_access->axis_stop(id);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to axis stop - Caught DevFailed!"), 
                     _CPTC("HwAccess::axis_stop"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to axis stop - Caught unknown exception!"), 
							_CPTC("HwAccess::axis_stop")); 
  }
}

// ============================================================================
// HwAccess::axis_init_ref_pos
// ============================================================================
void HwAccess::axis_init_ref_pos(sgonpmaclib::T_cs_id id)
{
  DEBUG_STREAM << "HwAccess::axis_init_ref_pos() entering..." << std::endl;
  DEBUG_STREAM << "(cs) = (" << id << ")" << std::endl;

  CHECK_PPMAC_ACCESS;

	try 
  {
		m_ppmac_access->axis_init_ref_pos(id);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to axis init ref pos - Caught DevFailed!"), 
                     _CPTC("HwAccess::axis_init_ref_pos"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to axis init ref pos - Caught unknown exception!"), 
							_CPTC("HwAccess::axis_init_ref_pos")); 
  }
}

// ============================================================================
// HwAccess::set_axis_position
// ============================================================================
void HwAccess::set_axis_position(sgonpmaclib::T_axis_id id, double position, bool apply)
{
  DEBUG_STREAM << "HwAccess::set_axis_position() entering..." << std::endl;
  DEBUG_STREAM << "(cs, axis) = (" << id.cs << ", " << id.axis_name << ")" << std::endl;
  DEBUG_STREAM << "position = " << position << " - apply = " << apply << std::endl;

  CHECK_PPMAC_ACCESS;

	try 
  {
		m_ppmac_access->set_axis_position(id, position, apply);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to set axis position - Caught DevFailed!"), 
                     _CPTC("HwAccess::set_axis_position"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to set axis posistion - Caught unknown exception!"), 
							_CPTC("HwAccess::set_axis_position")); 
  }
}

// ============================================================================
// HwAccess::apply_axis_position
// ============================================================================
void HwAccess::apply_axis_position(sgonpmaclib::T_cs_id id)
{
  DEBUG_STREAM << "HwAccess::apply_axis_position() entering..." << std::endl;
  DEBUG_STREAM << "(cs) = (" << id << ")" << std::endl;

  CHECK_PPMAC_ACCESS;

	try 
  {
		m_ppmac_access->apply_axis_position(id);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to apply axis position - Caught DevFailed!"), 
                     _CPTC("HwAccess::apply_axis_position"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to apply axis posistion - Caught unknown exception!"), 
							_CPTC("HwAccess::apply_axis_position")); 
  }
}

// ============================================================================
// HwAccess::set_axis_pos_offset
// ============================================================================
void HwAccess::set_axis_pos_offset(sgonpmaclib::T_axis_id id, double offset)
{
  DEBUG_STREAM << "HwAccess::set_axis_pos_offset() entering..." << std::endl;
  DEBUG_STREAM << "(cs, axis) = (" << id.cs << ", " << id.axis_name << ")" << std::endl;
  DEBUG_STREAM << "offset = " << offset << std::endl;

  CHECK_PPMAC_ACCESS;

	try 
  {
		m_ppmac_access->set_axis_pos_offset(id, offset);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to set axis pos offset - Caught DevFailed!"), 
                     _CPTC("HwAccess::set_axis_pos_offset"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to set axis pos offset - Caught unknown exception!"), 
							_CPTC("HwAccess::set_axis_pos_offset")); 
  }
}

// ============================================================================
// HwAccess::get_axis_pos_offset
// ============================================================================
double HwAccess::get_axis_pos_offset(sgonpmaclib::T_axis_id id)
{
  double offset = yat::IEEE_NAN;

  try 
  {
    offset = m_ppmac_access->get_axis_pos_offset(id);
  }
  catch(sgonpmaclib::Exception& e)
  {	
    Tango::DevFailed df = pmacToTangoException(e);
    RETHROW_DEVFAILED(df,
      _CPTC("HARDWARE_FAILURE"),
      _CPTC("Failed to get axis pos offset - Caught DevFailed!"), 
      _CPTC("HwAccess::get_axis_pos_offset"));
  }
  catch(...)
  {
    THROW_DEVFAILED(
      _CPTC("HARDWARE_FAILURE"), 
      _CPTC("Failed to get axis pos offset - Caught unknown exception!"), 
      _CPTC("HwAccess::get_axis_pos_offset")); 
  }

  return offset;
}

// ============================================================================
// HwAccess::set_cs_moving_mode
// ============================================================================
void HwAccess::set_cs_moving_mode(sgonpmaclib::T_cs_id id, sgonpmaclib::E_moving_mode_t mode)
{
  DEBUG_STREAM << "HwAccess::set_cs_moving_mode() entering..." << std::endl;
  DEBUG_STREAM << "(cs) = (" << id << ")" << std::endl;
  DEBUG_STREAM << "mode: " << (unsigned short)mode << std::endl;

  CHECK_PPMAC_ACCESS;

	try
  {
		m_ppmac_access->set_cs_moving_mode(id, mode);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to set cs moving mode - Caught DevFailed!"), 
                     _CPTC("HwAccess::set_cs_moving_mode"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to set cs moving mode - Caught unknown exception!"), 
							_CPTC("HwAccess::set_cs_moving_mode")); 
  }
}
 
// ============================================================================
// HwAccess::get_cs_moving_mode
// ============================================================================
sgonpmaclib::E_moving_mode_t HwAccess::get_cs_moving_mode(sgonpmaclib::T_cs_id id)
{
	CHECK_PPMAC_ACCESS;
	sgonpmaclib::E_moving_mode_t mode = sgonpmaclib::CS_MV_MODE_RAPID;

	try 
  {
		mode = m_ppmac_access->get_cs_moving_mode(id);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to get cs moving mode - Caught DevFailed!"), 
                     _CPTC("HwAccess::get_cs_moving_mode"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to get cs moving mode - Caught unknown exception!"), 
							_CPTC("HwAccess::get_cs_moving_mode")); 
  }

	return mode;
}

// ============================================================================
// HwAccess::set_cs_velocity
// ============================================================================
void HwAccess::set_cs_velocity(sgonpmaclib::T_cs_id id, double velocity)
{
  DEBUG_STREAM << "HwAccess::set_cs_velocity() entering..." << std::endl;
  DEBUG_STREAM << "(cs) = (" << id << ")" << std::endl;
  DEBUG_STREAM << "velocity: " << velocity << std::endl;

  CHECK_PPMAC_ACCESS;

	try 
  {
		m_ppmac_access->set_cs_velocity(id, velocity);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to set cs velocity - Caught DevFailed!"), 
                     _CPTC("HwAccess::set_cs_velocity"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to set cs velocity - Caught unknown exception!"), 
							_CPTC("HwAccess::set_cs_velocity")); 
  }
}

// ============================================================================
// HwAccess::get_cs_velocity
// ============================================================================
double HwAccess::get_cs_velocity(sgonpmaclib::T_cs_id id)
{
  CHECK_PPMAC_ACCESS;
  double vel = yat::IEEE_NAN;

	try 
  {
		vel = m_ppmac_access->get_cs_velocity(id);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to get cs velocity - Caught DevFailed!"), 
                     _CPTC("HwAccess::get_cs_velocity"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to get cs velocity - Caught unknown exception!"), 
							_CPTC("HwAccess::get_cs_velocity")); 
  }

	return vel;
}

// ============================================================================
// HwAccess::set_cs_acceleration
// ============================================================================
void HwAccess::set_cs_acceleration(sgonpmaclib::T_cs_id id, double acceleration)
{
  DEBUG_STREAM << "HwAccess::set_cs_acceleration() entering..." << std::endl;
  DEBUG_STREAM << "(cs) = (" << id << ")" << std::endl;
  DEBUG_STREAM << "acceleration: " << acceleration << std::endl;

  CHECK_PPMAC_ACCESS;

  // Apply ratio to acceleration  
  acceleration = acceleration * kACCELERATION_RATIO;
  
	try 
  {
		m_ppmac_access->set_cs_acceleration_time(id, acceleration); 
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to set cs acceleration - Caught DevFailed!"), 
                     _CPTC("HwAccess::set_cs_acceleration"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to set cs acceleration - Caught unknown exception!"), 
							_CPTC("HwAccess::set_cs_acceleration")); 
  }
}

// ============================================================================
// HwAccess::get_cs_acceleration
// ============================================================================
double HwAccess::get_cs_acceleration(sgonpmaclib::T_cs_id id)
{
  CHECK_PPMAC_ACCESS;
  double accel = yat::IEEE_NAN;

	try 
  {
		accel = m_ppmac_access->get_cs_acceleration_time(id);
    
    // Apply ratio to acceleration
    accel = accel / kACCELERATION_RATIO;
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to get cs acceleration - Caught DevFailed!"), 
                     _CPTC("HwAccess::get_cs_acceleration"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to get cs acceleration - Caught unknown exception!"), 
							_CPTC("HwAccess::get_cs_acceleration")); 
  }

  return accel;
}

// ============================================================================
// HwAccess::set_cs_deceleration
// ============================================================================
void HwAccess::set_cs_deceleration(sgonpmaclib::T_cs_id id, double deceleration)
{
  DEBUG_STREAM << "HwAccess::set_cs_deceleration() entering..." << std::endl;
  DEBUG_STREAM << "(cs) = (" << id << ")" << std::endl;
  DEBUG_STREAM << "deceleration: " << deceleration << std::endl;

  CHECK_PPMAC_ACCESS;
  
  // Apply ratio to deceleration  
  deceleration = deceleration * kDECELERATION_RATIO;

	try 
  {
		m_ppmac_access->set_cs_deceleration_time(id, deceleration); 
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to set cs deceleration - Caught DevFailed!"), 
                     _CPTC("HwAccess::set_cs_deceleration"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to set cs deceleration - Caught unknown exception!"), 
							_CPTC("HwAccess::set_cs_deceleration")); 
  }
}

// ============================================================================
// HwAccess::get_cs_deceleration
// ============================================================================
double HwAccess::get_cs_deceleration(sgonpmaclib::T_cs_id id)
{
  CHECK_PPMAC_ACCESS;
  double decel = yat::IEEE_NAN;

	try 
  {
		decel = m_ppmac_access->get_cs_deceleration_time(id); 
    // Apply ratio to Deceleration
    decel = decel / kDECELERATION_RATIO;
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to get cs deceleration - Caught DevFailed!"), 
                     _CPTC("HwAccess::get_cs_deceleration"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to get cs deceleration - Caught unknown exception!"), 
							_CPTC("HwAccess::get_cs_deceleration")); 
  }

  return decel;
}

// ======================================================================
// SGonPPMACBox::pmacToTangoException
// ======================================================================
Tango::DevFailed HwAccess::pmacToTangoException (const sgonpmaclib::Exception& te)
{
  Tango::DevErrorList error_list(te.errors.size());
  error_list.length(te.errors.size());

  for (unsigned int i = 0; i < te.errors.size(); i++)
  {
	  error_list[i].reason = CORBA::string_dup(te.errors[i].reason.c_str());
	  error_list[i].desc = CORBA::string_dup(te.errors[i].desc.c_str());
	  error_list[i].origin = CORBA::string_dup(te.errors[i].origin.c_str());
	  
	  switch(te.errors[i].severity)
	  {
	  case sgonpmaclib::WARN:
		  error_list[i].severity = Tango::WARN;
		  break;
	  case sgonpmaclib::PANIC:
		  error_list[i].severity = Tango::PANIC;
		  break;
	  case sgonpmaclib::ERR:
	  default:
		  error_list[i].severity = Tango::ERR;
		  break;
	  }
  }

  return Tango::DevFailed(error_list);
}

// ============================================================================
// HwAccess::start_mp
// ============================================================================
void HwAccess::start_mp(sgonpmaclib::T_cs_id id, int mp)
{
  DEBUG_STREAM << "HwAccess::start_mp() entering..." << std::endl;
  DEBUG_STREAM << "(cs) = (" << id << ")" << std::endl;
  DEBUG_STREAM << "MP id = " << mp << std::endl;

  CHECK_PPMAC_ACCESS;

	try 
  {
		m_ppmac_access->start_motion_program(id, mp);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to start MP - Caught DevFailed!"), 
                     _CPTC("HwAccess::start_mp"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to start MP - Caught unknown exception!"), 
							_CPTC("HwAccess::start_mp")); 
  }
}

// ============================================================================
// HwAccess::pause_mp
// ============================================================================
void HwAccess::pause_mp(sgonpmaclib::T_cs_id id)
{
  DEBUG_STREAM << "HwAccess::pause_mp() entering..." << std::endl;
  DEBUG_STREAM << "(cs) = (" << id << ")" << std::endl;

  CHECK_PPMAC_ACCESS;

	try 
  {
		m_ppmac_access->pause_current_motion_program(id);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to pause MP - Caught DevFailed!"), 
                     _CPTC("HwAccess::pause_mp"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to pause MP - Caught unknown exception!"), 
							_CPTC("HwAccess::pause_mp")); 
  }
}

// ============================================================================
// HwAccess::resume_mp
// ============================================================================
void HwAccess::resume_mp(sgonpmaclib::T_cs_id id)
{
  DEBUG_STREAM << "HwAccess::resume_mp() entering..." << std::endl;
  DEBUG_STREAM << "(cs) = (" << id << ")" << std::endl;

  CHECK_PPMAC_ACCESS;

	try 
  {
		m_ppmac_access->resume_current_motion_program(id);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to resume MP - Caught DevFailed!"), 
                     _CPTC("HwAccess::resume_mp"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to resume MP - Caught unknown exception!"), 
							_CPTC("HwAccess::resume_mp")); 
  }
}

// ============================================================================
// HwAccess::get_mp_state
// ============================================================================
sgonpmaclib::E_mp_state_t HwAccess::get_mp_state(sgonpmaclib::T_cs_id id)
{
	CHECK_PPMAC_ACCESS;
	sgonpmaclib::E_mp_state_t st = sgonpmaclib::MP_STATE_UNKNOWN;

	try 
  {
		st = m_ppmac_access->get_motion_program_state(id);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to get MP state - Caught DevFailed!"), 
                     _CPTC("HwAccess::get_mp_state"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to get MP state - Caught unknown exception!"), 
							_CPTC("HwAccess::get_mp_state")); 
  }

	return st;
}

// ============================================================================
// HwAccess::set_axis_compensation
// ============================================================================
void HwAccess::set_axis_compensation(sgonpmaclib::T_axis_id id, bool enabled)
{
  CHECK_PPMAC_ACCESS;

  try 
  {
    m_ppmac_access->set_axis_compensation(id, enabled);
  }
  catch(sgonpmaclib::Exception& e)
  {	
    Tango::DevFailed df = pmacToTangoException(e);
    RETHROW_DEVFAILED(df,
      _CPTC("HARDWARE_FAILURE"),
      _CPTC("Failed set axis compensation - Caught DevFailed!"), 
      _CPTC("HwAccess::set_axis_compensation"));
  }
  catch(...)
  {
    THROW_DEVFAILED(
      _CPTC("HARDWARE_FAILURE"), 
      _CPTC("Failed to set axis compensation - Caught unknown exception!"), 
      _CPTC("HwAccess::set_axis_compensation")); 
  }
}

// ============================================================================
// HwAccess::get_axis_compensation
// ============================================================================
bool HwAccess::get_axis_compensation(sgonpmaclib::T_axis_id id)
{
  CHECK_PPMAC_ACCESS;
  
  bool enabled = false;
  try 
  {
    enabled = m_ppmac_access->get_axis_compensation(id);
  }
  catch(sgonpmaclib::Exception& e)
  {	
    Tango::DevFailed df = pmacToTangoException(e);
    RETHROW_DEVFAILED(df,
      _CPTC("HARDWARE_FAILURE"),
      _CPTC("Failed to get axis compensation - Caught DevFailed!"), 
      _CPTC("HwAccess::get_axis_compensation"));
  }
  catch(...)
  {
    THROW_DEVFAILED(
      _CPTC("HARDWARE_FAILURE"), 
      _CPTC("Failed to get axis compensation - Caught unknown exception!"), 
      _CPTC("HwAccess::get_axis_compensation")); 
  }
  return enabled;
}

// ============================================================================
// HwAccess::get_cs_scurve_time
// ============================================================================
double HwAccess::get_cs_scurve_time(sgonpmaclib::T_cs_id id)
{
  CHECK_PPMAC_ACCESS;
  
  double scurve_time = yat::IEEE_NAN;
	try 
  {
		scurve_time = m_ppmac_access->get_cs_scurve_time(id);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to get scurve - Caught DevFailed!"), 
                     _CPTC("HwAccess::get_cs_scurve_time"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to get scurve - Caught unknown exception!"), 
							_CPTC("HwAccess::get_cs_scurve_time")); 
  }
  return scurve_time;
}
// ============================================================================
// HwAccess::set_cs_scurve_time
// ============================================================================
void HwAccess::set_cs_scurve_time(sgonpmaclib::T_cs_id id, double scurve)
{
  CHECK_PPMAC_ACCESS;

	try 
  {
		m_ppmac_access->set_cs_scurve_time(id, scurve);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to set scurve - Caught DevFailed!"), 
                     _CPTC("HwAccess::set_cs_scurve_time"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to set scurve - Caught unknown exception!"), 
							_CPTC("HwAccess::set_cs_scurve_time")); 
  }
}

// ============================================================================
// HwAccess::get_cs_compens_valid
// ============================================================================
bool HwAccess::get_cs_compens_valid(sgonpmaclib::T_cs_id id)
{
  CHECK_PPMAC_ACCESS;

  bool enabled = false;
	try 
  {
		enabled = m_ppmac_access->get_cs_compensation_validity(id);
  }
  catch(sgonpmaclib::Exception& e)
  {	
		Tango::DevFailed df = pmacToTangoException(e);
		RETHROW_DEVFAILED(df,
										 _CPTC("HARDWARE_FAILURE"),
										 _CPTC("Failed to resume MP - Caught DevFailed!"), 
                     _CPTC("HwAccess::get_cs_compens_valid"));
  }
  catch(...)
  {
		 THROW_DEVFAILED(
							_CPTC("HARDWARE_FAILURE"), 
							_CPTC("Failed to resume MP - Caught unknown exception!"), 
							_CPTC("HwAccess::get_cs_compens_valid")); 
  }
  return enabled;
}

} // namespace sgonppmac_ns


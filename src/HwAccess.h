//=============================================================================
// HwAccess.h
//=============================================================================
// class.............HwAccess - Hardware access to the sgonpmaclib implementation
// original author...F. THIAM
//=============================================================================

#ifndef _HW_ACCESS_H_
#define _HW_ACCESS_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <yat4tango/InnerAppender.h>
#include <SGonPMAC/SGonPMACLib.h>
#include <SGonPPMACTypesAndConsts.h>

// ============================================================================
// API DEFINITION
//
// This API provides access to controller functions and to axes management from
// the sgonpmaclib.
// ============================================================================

namespace sgonppmac_ns {

//***********************************************************************************
// HwAccess API
//***********************************************************************************
class HwAccess : public Tango::LogAdapter
{

public:
  // Constructor.
  HwAccess(Tango::DeviceImpl * hostDevice);

  // Destructor.
  // If a session is opened, it is closed before the destruction
  virtual ~HwAccess();

  //---------------------------------------------------------------------------
  // Controller management
  //---------------------------------------------------------------------------
  
  // Initializes the driver's connection.
  // This function opens a session with the controller.
  void open(std::string ipAddress, unsigned int tcpPort);
    
  // Finalize the driver's connection.
  // This function closes the session with the controller.
  void close();
    
  // Gets the controller's connection state (ok/nok).
  // @param isConnected  (Out) true or false
  void get_connection_state(bool &isConnected);

  // Executes a low level command.
  // @param cmd The low level command
  // @param outParam Command reply
  void send_low_level_cmd(std::string cmd, std::string &outParam);

  // Gets the controller status word.
  // @return PPMAC controller current status (binary word)
	sgonpmaclib::T_ppmac_brick_status get_raw_status();

	// Gets the CPU temperature.
  // @return CPU temperature in Celsius degrees
  double get_cpu_temperature();
	
  // Gets CPU running time from power-on to present, in seconds.
  // @return Running time (s). 
  double get_cpu_running_time();

  // Gets CPU usage to complete tasks in last phase cycle, in �sec.
  // @return Phase usage. 
  double get_cpu_phase_usage();

  // Gets CPU usage to complete tasks in last servo cycle, in �sec.
  // @return So usage. 
  double get_cpu_servo_usage();

  // Gets CPU usage to complete tasks in last RTI cycle, in �sec.
  // @return RTI usage. 
  double get_cpu_rti_usage();

  // Gets CPU usage to complete tasks in last BG cycle, in �sec.
  // @return BG usage. 
  double get_cpu_bg_usage();

  // Resets the controller.
  // Does not break ssh connection.
  void reset();

  // Reboots the controller operating system.
  void reboot();
	
  // Stops all axes movements and plc programs.
  void abort();
  	
  // Gets firmware version.
  // @return Firmware version
  std::string get_firmware_vers();

  // Gets micro code version.
  // @return Micro code version
  std::string get_ucode_vers();

  // Multi-axes management:

  // Gets current position of a list of axes.
  // @param ids Axis id (cs, axis name) list.
  // @param axes_pos Out position list (in axis list order).
	void get_axes_positions(std::vector<sgonpmaclib::T_axis_id> axis_list, std::vector<double> &axes_pos);
  
  //---------------------------------------------------------------------------
  //- Axis initilization
  //---------------------------------------------------------------------------

  // Initializes the axis identified with CS number and axis name in CS.
  // The library asks the associated data index to the controller.
  // @param id Axis id (cs, axis name).
	void init_axis(sgonpmaclib::T_axis_id id);
	
	// Delete axis reference from library internal list.
  // The axis is still declared in controller.
  // @param id Axis id (cs, axis name).
  void undeclare_axis(sgonpmaclib::T_axis_id id);

  // Gets current CS status.
  // @param id CS id.
  // @return CS status.
	sgonpmaclib::T_cs_status get_cs_status(sgonpmaclib::T_cs_id id);

  // Gets current motors status.
  // @param id CS id.
  // @return motors status.
	std::string get_cs_motors_status(sgonpmaclib::T_cs_id id);
  
  // Gets referencing current CS status.
  // @param id CS id.
  // @return CS referencing status.
  sgonpmaclib::E_cs_referencing_status_t get_cs_ref_status(sgonpmaclib::T_cs_id id);

  //---------------------------------------------------------------------------
  // Axis actions
  //---------------------------------------------------------------------------

  // Powers on the underlying motor.
  // @param id CS id.
	void motor_on(sgonpmaclib::T_cs_id id);

  // Powers off the underlying motor.
  // @param id CS id.
	void motor_off(sgonpmaclib::T_cs_id id);
  
  // Powers off the underlying motor.
  // @param id CS id.
	void motor_other_off(sgonpmaclib::T_cs_id id);
  
  // Powers off the underlying motor.
  // @param id CS id.
	void motor_omega_off(sgonpmaclib::T_cs_id id);
  
  // Stop current axis movement.
  // @param id CS id.
	void axis_stop(sgonpmaclib::T_cs_id id);

  // Initializes reference position with current init type 
  // and init position.
  // @param id CS id.
	void axis_init_ref_pos(sgonpmaclib::T_cs_id cs);

  // Starts motion program.
  // @param id CS id.
  // @param mp MP id.
  void start_mp(sgonpmaclib::T_cs_id id, int mp);

  // Pauses current motion program.
  // @param id CS id.
  void pause_mp(sgonpmaclib::T_cs_id id);

  // Resumes current motion program.
  // @param id CS id.
  void resume_mp(sgonpmaclib::T_cs_id id);

  // Gets current MP state.
  // @param id CS id.
  // @return Current MP state.
  sgonpmaclib::E_mp_state_t get_mp_state(sgonpmaclib::T_cs_id id);


  //---------------------------------------------------------------------------
  // Axis attributes
  //---------------------------------------------------------------------------

  // Moves axis to specified absolute position, in user unit.
  // @param id Axis id (cs, axis name).
  // @param position Axis absolute position setpoint in user unit.
  // @param apply True to set and apply position setpoint, false to only set position setpoint.
	void set_axis_position(sgonpmaclib::T_axis_id id, double position, bool apply = true);

  // Apply axis position setpoints in specified CS.
  // @param id CS id.
	void apply_axis_position(sgonpmaclib::T_cs_id id);

  // Sets axis position offset, in user unit.
  // @param id Axis id (cs, axis name).
  // @param offset Axis position offset in user unit.
	void set_axis_pos_offset(sgonpmaclib::T_axis_id id, double offset);

  // Gets current axis position offset, in user unit.
  // @param id Axis id (cs, axis name).
  // @return Current axis position offset in user unit.
	double get_axis_pos_offset(sgonpmaclib::T_axis_id id);

  // Sets axis velocity, in user unit/s.
  // @param id CS id.
  // @param velocity Axis velocity setpoint in user unit/s.
	void set_cs_velocity(sgonpmaclib::T_cs_id id, double velocity);

  // Gets current axis velocity, in user unit/s.
  // @param id CS id.
  // @return Current axis velocity, in user unit/s.
	double get_cs_velocity(sgonpmaclib::T_cs_id id);

	// Sets axis moving mode.
  // @param id CS id.
  // @param mode CS moving mode.
  void set_cs_moving_mode(sgonpmaclib::T_cs_id id, sgonpmaclib::E_moving_mode_t mode);

  // Gets current axis moving mode.
  // @param id CS id.
  // @return Current axis moving mode.
  sgonpmaclib::E_moving_mode_t get_cs_moving_mode(sgonpmaclib::T_cs_id id);

  // Gets current axis acceleration, in user unit/s^2.
  // @param id CS id.
  // @return Current axis acceleration, in user unit/s^2.
	double get_cs_acceleration(sgonpmaclib::T_cs_id id);

  // Sets axis acceleration, in user unit/s^2.
  // @param id CS id.
  // @param acceleration Axis acceleration setpoint in user unit/s^2.
	void set_cs_acceleration(sgonpmaclib::T_cs_id id, double acceleration);

  // Gets current axis deceleration, in user unit/s^2.
  // @param id CS id.
  // @return Current axis deceleration, in user unit/s^2.
	double get_cs_deceleration(sgonpmaclib::T_cs_id id);

  // Sets axis deceleration, in user unit/s^2.
  // @param id CS id.
  // @param deceleration Axis deceleration setpoint in user unit/s^2.
	void set_cs_deceleration(sgonpmaclib::T_cs_id id, double deceleration);
  
  // Sets axis compensation.
  // @param axis id.
  // @param enabled.
  void set_axis_compensation(sgonpmaclib::T_axis_id id, bool enabled);
  
  // Gets axis compensation.
  // @param axis id.
  // @return enabled.
  bool get_axis_compensation(sgonpmaclib::T_axis_id id);
  
  // Gets cs compensation valid state
  // @param axis id.
  // @return valid state.
  bool get_cs_compens_valid(sgonpmaclib::T_cs_id id);  
  
  // Sets scurve
  // @param axis id.
  // @param scurve to set.
  void set_cs_scurve_time(sgonpmaclib::T_cs_id id, double scurve);
  
  // Gets scurve
  // @param axis id.
  // @return Scurve
  double get_cs_scurve_time(sgonpmaclib::T_cs_id id);
  
protected:

  // Low level lib access
  sgonpmaclib::SGonPMAClib * m_ppmac_access;

	Tango::DevFailed pmacToTangoException (const sgonpmaclib::Exception& te);

};

} // namespace sgonppmac_ns

#endif // _HW_ACCESS_H_

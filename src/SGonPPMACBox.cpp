static const char *RcsId = "$Id: DevServ.cpp 13293 2009-04-07 10:53:56Z pascal_verdier $";
//+=============================================================================
//
// file :         SGonPPMACBox.cpp
//
// description :  C++ source for the SGonPPMACBox and its commands. 
//                The class is derived from Device. It represents the
//                CORBA servant object which will be accessed from the
//                network. All commands which can be executed on the
//                SGonPPMACBox are implemented in this file.
//
// project :      TANGO Device Server
//
// $Author: pascal_verdier $
//
// $Revision: 13293 $
//
// $Revision: 13293 $
// $Date: 2009-04-07 12:53:56 +0200 (Tue, 07 Apr 2009) $
//
// SVN only:
// $HeadURL: $
//
// CVS only:
// $Source$
// $Log$
// Revision 3.5  2007/10/24 12:07:35  pascal_verdier
// Another spelling mistake correction
//
// Revision 3.4  2007/10/23 14:04:30  pascal_verdier
// Spelling mistakes correction
//
// Revision 3.3  2005/03/02 14:06:15  pascal_verdier
// namespace is different than class name.
//
// Revision 3.2  2004/11/08 11:33:16  pascal_verdier
// if device property not found in database, it takes class property value if exists.
//
// Revision 3.1  2004/09/06 09:27:05  pascal_verdier
// Modified for Tango 5 compatibility.
//
//
// copyleft :    Synchrotron SOLEIL 
//               L'Orme des merisiers - Saint Aubin
//               BP48 - 91192 Gif sur Yvette
//               FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================



//===================================================================
//
//	The following table gives the correspondence
//	between commands and method name.
//
//  Command name        |  Method name
//	----------------------------------------
//  State               |  dev_state()
//  Status              |  dev_status()
//  Reset               |  reset()
//  Abort               |  abort()
//  ExecLowLevelCmd     |  exec_low_level_cmd()
//  GetFirmwareVersion  |  get_firmware_version()
//  GetUCodeVersion     |  get_ucode_version()
//
//===================================================================



#include <tango.h>
#include <SGonPPMACBox.h>
#include <SGonPPMACBoxClass.h>
#include <BoxInitDetector.h> 

namespace SGonPPMACBox_ns
{

//- check controller factory macro:
#define CHECK_CTRL_FACTORY() \
	do \
	{ \
	if (! m_ctrlFactory) \
	THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
	_CPTC("request aborted - the controller factory isn't accessible "), \
  _CPTC("SGonPPMACBox::check_ctrl_factory")); \
	} while (0)

		//- check controller cmd macro:
#define CHECK_CTRL_CMD() \
	do \
	{ \
	if (! m_cmdInterface) \
	THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
	_CPTC("request aborted - the controller cmd isn't accessible "), \
  _CPTC("SGonPPMACBox::check_ctrl_cmd")); \
	} while (0)

	//- check controller std macro:
#define CHECK_CTRL_STD() \
	do \
	{ \
	if (! m_stdDataInterface) \
	THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
	_CPTC("request aborted - the controller std isn't accessible "), \
  _CPTC("SGonPPMACBox::check_ctrl_std")); \
	} while (0)

	//- check controller ctl macro:
#define CHECK_CTRL_CTL() \
	do \
	{ \
	if (! m_ctlDataInterface) \
	THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
	_CPTC("request aborted - the controller ctl isn't accessible "), \
  _CPTC("SGonPPMACBox::check_ctrl_ctl")); \
	} while (0)

//+----------------------------------------------------------------------------
//
// method : 		SGonPPMACBox::SGonPPMACBox(string &s)
// 
// description : 	constructor for simulated SGonPPMACBox
//
// in : - cl : Pointer to the DeviceClass object
//      - s : Device name 
//
//-----------------------------------------------------------------------------
SGonPPMACBox::SGonPPMACBox(Tango::DeviceClass *cl,string &s)
:Tango::Device_4Impl(cl,s.c_str())
{
	init_device();
}

SGonPPMACBox::SGonPPMACBox(Tango::DeviceClass *cl,const char *s)
:Tango::Device_4Impl(cl,s)
{
	init_device();
}

SGonPPMACBox::SGonPPMACBox(Tango::DeviceClass *cl,const char *s,const char *d)
:Tango::Device_4Impl(cl,s,d)
{
	init_device();
}
//+----------------------------------------------------------------------------
//
// method : 		SGonPPMACBox::delete_device()
// 
// description : 	will be called at device destruction or at init command.
//
//-----------------------------------------------------------------------------
void SGonPPMACBox::delete_device()
{
  // set init box state to true (beginning of re-init sequence)
  sgonppmac_ns::BoxInitDetector::instance()->set_box_init_state(true);

	//	Delete device allocated objects
	if (m_ctrlFactory)
  {
  	// Remove the ctrl factory --------------------------------------------
		try
		{
			sgonppmac_ns::ControllerFactory::close();
		}
		catch (...)
		{
			
		}

		m_ctrlFactory = NULL;
		m_cmdInterface = NULL;
		m_stdDataInterface = NULL;
		m_ctlDataInterface = NULL;
  }

	//- delete logs attr
	yat4tango::InnerAppender::release(this);
}

//+----------------------------------------------------------------------------
//
// method : 		SGonPPMACBox::init_device()
// 
// description : 	will be called at device initialization.
//
//-----------------------------------------------------------------------------
void SGonPPMACBox::init_device()
{
  // set init box state to true (beginning of init sequence)
  sgonppmac_ns::BoxInitDetector::instance()->set_box_init_state(true);

	m_state = Tango::INIT;
	m_initOk = false;

	// Initialise variables to default values
	//--------------------------------------------
	m_cpuTemperature = 0.0;
	m_runningTime = 0.0;
	m_phaseTaskUsage = 0.0;
	m_servoTaskUsage = 0.0;
	m_rtIntTaskUsage = 0.0;
	m_bgTaskUsage = 0.0;
	m_ctrlFactory = NULL;
	m_cmdInterface = NULL;
	m_stdDataInterface = NULL;
	m_ctlDataInterface = NULL;

	//- initialize the inner appender (first thing to do)
	try
	{
		yat4tango::InnerAppender::initialize(this);
	}
	catch( Tango::DevFailed& df )
	{
		ERROR_STREAM << df << std::endl;
		m_state = Tango::FAULT;
		m_currStatus = "initialization failed - could not instanciate the InnerAppender";
		return;
	}

	INFO_STREAM << "SGonPPMACBox::SGonPPMACBox() create device " << device_name << endl;

	// get properties
	//--------------------------------------------

	try
	{
		this->get_device_property();

		DEBUG_STREAM << "SGonPPMACBox: get_device_property OK." << std::endl;
		m_cfgPMAC.dump();
	}
	catch (Tango::DevFailed & df)
	{
		ERROR_STREAM << df << std::endl;
		m_currStatus = "initialization failed - <get_device_property> failed [see device log for details]";
		m_state = Tango::FAULT;
		return;
	}
	catch (...)
	{
		ERROR_STREAM << "initialization failed - unknown <get_device_property> error" << std::endl;
		m_currStatus = "initialization failed [unknown <get_device_property> error]";
		m_state = Tango::FAULT;
		return;
	}

	// construct the ControlerFactory
	//--------------------------------------------
	try
	{
		sgonppmac_ns::ControllerFactory::init(m_cfgPMAC);

		DEBUG_STREAM << "SGonPPMACBox: ControllerFactory init OK." << std::endl;
	}
	catch (...)
	{
		ERROR_STREAM << "initialization failed - failed to init controler factory" << std::endl;
		m_currStatus = "initialization failed [failed to create controler factory]";
		m_state = Tango::FAULT;
		return;
	}

	// Get the instance of the ControlerFactory
	//--------------------------------------------
	try
	{
		m_ctrlFactory = sgonppmac_ns::ControllerFactory::instance();
	}
	catch (...)
	{
		ERROR_STREAM << "initialization failed - failed to get the instance of the controler factory" << std::endl;
		m_currStatus = "initialization failed [failed to get the instance of the controler factory]";
		m_state = Tango::FAULT;
		return;
	}

	// test the ControlerFactory
	if (!m_ctrlFactory)
	{
		ERROR_STREAM << "initialization failed - the controler factory is not created" << std::endl;
		m_currStatus = "initialization failed [the controler factory is not created]";
		m_state = Tango::FAULT;
		return;
	}

	DEBUG_STREAM << "SGonPPMACBox: ControllerFactory instance OK." << std::endl;
	m_cfgPMAC.dump();

	
	// Get the cmd controler interface
	//--------------------------------------------
	try
	{
		m_cmdInterface = m_ctrlFactory->get_cmd_interface();
	}
	catch (...)
	{
		ERROR_STREAM << "Initialization failed - failed to get cmd controler interface" << std::endl;
		m_currStatus = "Device initialization failed [failed to get controller interface]\n";
    m_currStatus += "Check controller connection first! See logs for details.";
		m_state = Tango::FAULT;
		return;
	}

	// check the m_cmdInterface
	if (!m_cmdInterface)
	{
		ERROR_STREAM << "initialization failed - the cmd controler interface is not created" << std::endl;
		m_currStatus = "initialization failed [the cmd controler interface is not created]";
		m_state = Tango::FAULT;
		return;
	}

	// get connection state
    bool cnct_st = false;	
	try
	{
		m_cmdInterface->get_connection_state(cnct_st);
	}
	catch(...)
	{
		ERROR_STREAM << "initialization failed - cannot get CMD connection state!" << std::endl;
		m_currStatus = "initialization failed - cannot get CMD connection state!";
		m_state = Tango::FAULT;
		return;
	}
	DEBUG_STREAM << "SGonPPMACBox: get_cmd_interface OK." << std::endl;

	
	// Get the std controler interface
	//--------------------------------------------
	try
	{
		m_stdDataInterface = m_ctrlFactory->get_std_data_interface();
	}
	catch (...)
	{
		ERROR_STREAM << "initialization failed - failed to get std controler interface" << std::endl;
		m_currStatus = "initialization failed [failed to get std controler interface]";
		m_state = Tango::FAULT;
		return;
	}

	// check the m_stdDataInterface
	if (!m_stdDataInterface)
	{
		ERROR_STREAM << "initialization failed - the std controler interface is not created" << std::endl;
		m_currStatus = "initialization failed [the std controler interface is not created]";
		m_state = Tango::FAULT;
		return;
	}

	// get connection state
	try
	{
		m_stdDataInterface->get_connection_state(cnct_st);
	}
	catch(...)
	{
		ERROR_STREAM << "initialization failed - cannot get STD connection state!" << std::endl;
		m_currStatus = "initialization failed - cannot get STD connection state!";
		m_state = Tango::FAULT;
		return;
	}
	DEBUG_STREAM << "SGonPPMACBox: get_std_data_interface OK." << std::endl;

	
	// Get the ctl controler interface
	//--------------------------------------------
	try
	{
		m_ctlDataInterface = m_ctrlFactory->get_ctl_data_interface();
	}
	catch (...)
	{
		ERROR_STREAM << "initialization failed - failed to get ctl controler interface" << std::endl;
		m_currStatus = "initialization failed [failed to get ctl controler interface]";
		m_state = Tango::FAULT;
		return;
	}

	// check the m_ctlDataInterface
	if (!m_ctlDataInterface)
	{
		ERROR_STREAM << "initialization failed - the ctl controler interface is not created" << std::endl;
		m_currStatus = "initialization failed [the ctl controler interface is not created]";
		m_state = Tango::FAULT;
		return;
	}

	// get connection state
	try
	{
		m_ctlDataInterface->get_connection_state(cnct_st);
	}
	catch(...)
	{
		ERROR_STREAM << "initialization failed - cannot get CTL connection state!" << std::endl;
		m_currStatus = "initialization failed - cannot get CTL connection state!";
		m_state = Tango::FAULT;
		return;
	}	
	DEBUG_STREAM << "SGonPPMACBox: get_ctl_data_interface OK." << std::endl;

	m_state = Tango::OPEN;
    m_currStatus = "Power Brick OK";

	DEBUG_STREAM << "SGonPPMACBox: Initialization OK." << std::endl;

	m_initOk = true;

  // set init box state to false (end of init sequence)
  sgonppmac_ns::BoxInitDetector::instance()->set_box_init_state(false);
}


//+----------------------------------------------------------------------------
//
// method : 		SGonPPMACBox::get_device_property()
// 
// description : 	Read the device properties from database.
//
//-----------------------------------------------------------------------------
void SGonPPMACBox::get_device_property()
{
	//	Initialize your default values here (if not done with  POGO).
	//------------------------------------------------------------------
	
	tCPPort = 22;
	stdDataPollingPeriod = 0;
	criticalDataPollingPeriod = 100;
  nbOfComposedAxes = 1;

	//	Read device properties from database.(Automatic code generation)
	//------------------------------------------------------------------
	Tango::DbData	dev_prop;
	dev_prop.push_back(Tango::DbDatum("IPAddress"));
	dev_prop.push_back(Tango::DbDatum("TCPPort"));
	dev_prop.push_back(Tango::DbDatum("StdDataPollingPeriod"));
	dev_prop.push_back(Tango::DbDatum("CriticalDataPollingPeriod"));
	dev_prop.push_back(Tango::DbDatum("NbOfComposedAxes"));

	//	Call database and extract values
	//--------------------------------------------
	if (Tango::Util::instance()->_UseDb==true)
		get_db_device()->get_property(dev_prop);
	Tango::DbDatum	def_prop, cl_prop;
	SGonPPMACBoxClass	*ds_class =
		(static_cast<SGonPPMACBoxClass *>(get_device_class()));
	int	i = -1;

	//	Try to initialize IPAddress from class property
	cl_prop = ds_class->get_class_property(dev_prop[++i].name);
	if (cl_prop.is_empty()==false)	cl_prop  >>  iPAddress;
	else {
		//	Try to initialize IPAddress from default device value
		def_prop = ds_class->get_default_device_property(dev_prop[i].name);
		if (def_prop.is_empty()==false)	def_prop  >>  iPAddress;
	}
	//	And try to extract IPAddress value from database
	if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  iPAddress;

	//	Try to initialize TCPPort from class property
	cl_prop = ds_class->get_class_property(dev_prop[++i].name);
	if (cl_prop.is_empty()==false)	cl_prop  >>  tCPPort;
	else {
		//	Try to initialize TCPPort from default device value
		def_prop = ds_class->get_default_device_property(dev_prop[i].name);
		if (def_prop.is_empty()==false)	def_prop  >>  tCPPort;
	}
	//	And try to extract TCPPort value from database
	if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  tCPPort;

	//	Try to initialize StdDataPollingPeriod from class property
	cl_prop = ds_class->get_class_property(dev_prop[++i].name);
	if (cl_prop.is_empty()==false)	cl_prop  >>  stdDataPollingPeriod;
	else {
		//	Try to initialize StdDataPollingPeriod from default device value
		def_prop = ds_class->get_default_device_property(dev_prop[i].name);
		if (def_prop.is_empty()==false)	def_prop  >>  stdDataPollingPeriod;
	}
	//	And try to extract StdDataPollingPeriod value from database
	if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  stdDataPollingPeriod;

	//	Try to initialize CriticalDataPollingPeriod from class property
	cl_prop = ds_class->get_class_property(dev_prop[++i].name);
	if (cl_prop.is_empty()==false)	cl_prop  >>  criticalDataPollingPeriod;
	else {
		//	Try to initialize CriticalDataPollingPeriod from default device value
		def_prop = ds_class->get_default_device_property(dev_prop[i].name);
		if (def_prop.is_empty()==false)	def_prop  >>  criticalDataPollingPeriod;
	}
	//	And try to extract CriticalDataPollingPeriod value from database
	if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  criticalDataPollingPeriod;

	//	Try to initialize NbOfComposedAxes from class property
	cl_prop = ds_class->get_class_property(dev_prop[++i].name);
	if (cl_prop.is_empty()==false)	cl_prop  >>  nbOfComposedAxes;
	else {
		//	Try to initialize NbOfComposedAxes from default device value
		def_prop = ds_class->get_default_device_property(dev_prop[i].name);
		if (def_prop.is_empty()==false)	def_prop  >>  nbOfComposedAxes;
	}
	//	And try to extract NbOfComposedAxes value from database
	if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  nbOfComposedAxes;



	//	End of Automatic code generation
	//------------------------------------------------------------------
	//- check the IP address 
	if (iPAddress.empty() == true)
	{
		ERROR_STREAM << "The IP address of the underlying power brick must be filled" << std::endl;
		THROW_DEVFAILED(
      _CPTC("CONFIGURATION_ERROR"),
			_CPTC("The IP address of the underlying power brick  must be filled"),
			_CPTC("SGonPPMACBox::get_device_property "));
	}

	//- check the tCPPort
	if (tCPPort < 0)
	{
		ERROR_STREAM << "The tCPPort must be filled and greater or equal than 0" << std::endl;
			THROW_DEVFAILED(
        _CPTC("CONFIGURATION_ERROR"),
				_CPTC("The tCPPort must be filled and greater than 0"),
				_CPTC("SGonPPMACBox::get_device_property "));
	}

	//- check the stdDataPollingPeriod
	if (stdDataPollingPeriod < 0)
	{
		ERROR_STREAM << "The stdDataPollingPeriod must be filled and greater than 0" << std::endl;
			THROW_DEVFAILED(
        _CPTC("CONFIGURATION_ERROR"),
				_CPTC("The stdDataPollingPeriod must be filled and greater or equal than 0"),
				_CPTC("SGonPPMACBox::get_device_property "));
	}

	//- check the criticalDataPollingPeriod
	if (criticalDataPollingPeriod < 0)
	{
		ERROR_STREAM << "The criticalDataPollingPeriod must be filled and greater than 0" << std::endl;
			THROW_DEVFAILED(
        _CPTC("CONFIGURATION_ERROR"),
				_CPTC("The criticalDataPollingPeriod must be filled and greater or equal than 0"),
				_CPTC("SGonPPMACBox::get_device_property "));
	}

	//- check the nbOfComposedAxes
	try
  {
    // try to get value
		nbOfComposedAxes = this->get_value_as_property<Tango::DevUShort>(this, "NbOfComposedAxes");
  }
  catch(...)
  {
    ERROR_STREAM << "The NbOfComposedAxes must be filled" << std::endl;
			THROW_DEVFAILED(
        _CPTC("CONFIGURATION_ERROR"),
				_CPTC("The NbOfComposedAxes must be filled"),
				_CPTC("SGonPPMACBox::get_device_property "));
  }

	//- check the value of nbOfComposedAxes
	if (nbOfComposedAxes < 0)
	{
		ERROR_STREAM << "The nbOfComposedAxes must be filled and greater or equal than 0" << std::endl;
			THROW_DEVFAILED(
        _CPTC("CONFIGURATION_ERROR"),
				_CPTC("The nbOfComposedAxes must be filled and greater or equal than 0"),
				_CPTC("SGonPPMACBox::get_device_property "));
	}

	m_cfgPMAC.hostDevice = this;
	m_cfgPMAC.iPAddress = iPAddress;
	m_cfgPMAC.tCPPort = tCPPort;
	m_cfgPMAC.stdDataPollingPeriod = stdDataPollingPeriod;
	m_cfgPMAC.criticalDataPollingPeriod = criticalDataPollingPeriod;
	m_cfgPMAC.nbOfComposedAxes = nbOfComposedAxes;
}
//+----------------------------------------------------------------------------
//
// method : 		SGonPPMACBox::always_executed_hook()
// 
// description : 	method always executed before any command is executed
//
//-----------------------------------------------------------------------------
void SGonPPMACBox::always_executed_hook()
{
}
//+----------------------------------------------------------------------------
//
// method : 		SGonPPMACBox::read_attr_hardware
// 
// description : 	Hardware acquisition for attributes.
//
//-----------------------------------------------------------------------------
void SGonPPMACBox::read_attr_hardware(vector<long> &attr_list)
{
	//DEBUG_STREAM << "SGonPPMACBox::read_attr_hardware(vector<long> &attr_list) entering... "<< endl;
	//	Add your own code here
}
//+----------------------------------------------------------------------------
//
// method : 		SGonPPMACBox::read_phaseTaskUsage
// 
// description : 	Extract real attribute values for phaseTaskUsage acquisition result.
//
//-----------------------------------------------------------------------------
void SGonPPMACBox::read_phaseTaskUsage(Tango::Attribute &attr)
{
	//DEBUG_STREAM << "SGonPPMACBox::read_phaseTaskUsage(Tango::Attribute &attr) entering... "<< endl;
	if (this->m_initOk)
	{
		CHECK_CTRL_STD();

		m_phaseTaskUsage = this->m_stdDataInterface->get_cpu_phase_usage();
		
		attr.set_value(&m_phaseTaskUsage); 	
	}
}

//+----------------------------------------------------------------------------
//
// method : 		SGonPPMACBox::read_servoTaskUsage
// 
// description : 	Extract real attribute values for servoTaskUsage acquisition result.
//
//-----------------------------------------------------------------------------
void SGonPPMACBox::read_servoTaskUsage(Tango::Attribute &attr)
{
	//DEBUG_STREAM << "SGonPPMACBox::read_servoTaskUsage(Tango::Attribute &attr) entering... "<< endl;
	if (this->m_initOk)
	{
		CHECK_CTRL_STD();

		m_servoTaskUsage = this->m_stdDataInterface->get_cpu_servo_usage();

		attr.set_value(&m_servoTaskUsage); 
	}
}

//+----------------------------------------------------------------------------
//
// method : 		SGonPPMACBox::read_rtIntTaskUsage
// 
// description : 	Extract real attribute values for rtIntTaskUsage acquisition result.
//
//-----------------------------------------------------------------------------
void SGonPPMACBox::read_rtIntTaskUsage(Tango::Attribute &attr)
{
	//DEBUG_STREAM << "SGonPPMACBox::read_rtIntTaskUsage(Tango::Attribute &attr) entering... "<< endl;
	if (this->m_initOk)
	{
		CHECK_CTRL_STD();

		m_rtIntTaskUsage = this->m_stdDataInterface->get_cpu_rti_usage();
		
		attr.set_value(&m_rtIntTaskUsage); 
	}
}

//+----------------------------------------------------------------------------
//
// method : 		SGonPPMACBox::read_bgTaskUsage
// 
// description : 	Extract real attribute values for bgTaskUsage acquisition result.
//
//-----------------------------------------------------------------------------
void SGonPPMACBox::read_bgTaskUsage(Tango::Attribute &attr)
{
	//DEBUG_STREAM << "SGonPPMACBox::read_bgTaskUsage(Tango::Attribute &attr) entering... "<< endl;
	if (this->m_initOk)
	{
		CHECK_CTRL_STD();

		m_bgTaskUsage = this->m_stdDataInterface->get_cpu_bg_usage();

		attr.set_value(&m_bgTaskUsage); 
	}
}


//+----------------------------------------------------------------------------
//
// method : 		SGonPPMACBox::read_cpuTemperature
// 
// description : 	Extract real attribute values for cpuTemperature acquisition result.
//
//-----------------------------------------------------------------------------
void SGonPPMACBox::read_cpuTemperature(Tango::Attribute &attr)
{
	//DEBUG_STREAM << "SGonPPMACBox::read_cpuTemperature(Tango::Attribute &attr) entering... "<< endl;
	if (this->m_initOk)
	{
		CHECK_CTRL_STD();

		m_cpuTemperature = this->m_stdDataInterface->get_cpu_temp();
		
		attr.set_value(&m_cpuTemperature); 	
	}
}

//+----------------------------------------------------------------------------
//
// method : 		SGonPPMACBox::read_runningTime
// 
// description : 	Extract real attribute values for runningTime acquisition result.
//
//-----------------------------------------------------------------------------
void SGonPPMACBox::read_runningTime(Tango::Attribute &attr)
{
	//DEBUG_STREAM << "SGonPPMACBox::read_runningTime(Tango::Attribute &attr) entering... "<< endl;
	if (this->m_initOk)
	{
		CHECK_CTRL_STD();

		m_runningTime = this->m_stdDataInterface->get_cpu_running_time();
		
		attr.set_value(&m_runningTime); 	
	}
}

//+------------------------------------------------------------------
/**
 *	method:	SGonPPMACBox::reset
 *
 *	description:	method to execute "Reset"
 *	Resets the Power Brick.
 *
 *
 */
//+------------------------------------------------------------------
void SGonPPMACBox::reset()
{
	DEBUG_STREAM << "SGonPPMACBox::reset(): entering... !" << endl;

	//	Add your own code to control device here
	CHECK_CTRL_CMD();

  // reset box
  try
  {
#if defined (_SIMULATION_)
    this->m_stdDataInterface->reset();
#else
    this->m_cmdInterface->reset();
#endif
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "SGonPPMACBox::reset caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to reset box - Caught DevFailed!"), 
                     _CPTC("SGonPPMACBox::reset"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to reset box" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to reset box - Caught unknown exception!"), 
							_CPTC("SGonPPMACBox::reset")); 
  }
}

//+------------------------------------------------------------------
/**
 *	method:	SGonPPMACBox::abort
 *
 *	description:	method to execute "Abort"
 *	Stops all movements and programs
 *
 *
 */
//+------------------------------------------------------------------
void SGonPPMACBox::abort()
{
	DEBUG_STREAM << "SGonPPMACBox::abort(): entering... !" << endl;

	//	Add your own code to control device here
	CHECK_CTRL_CMD();

  // abort box
  try
  {
    this->m_cmdInterface->abort();
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "SGonPPMACBox::abort caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to abort box - Caught DevFailed!"), 
                     _CPTC("SGonPPMACBox::abort"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to abort box" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to abort box - Caught unknown exception!"), 
							_CPTC("SGonPPMACBox::abort")); 
  }
}

//+------------------------------------------------------------------
/**
 *	method:	SGonPPMACBox::exec_low_level_cmd
 *
 *	description:	method to execute "ExecLowLevelCmd"
 *	Execute low level command.
 *
 * @param	argin	command
 * @return	return value if any
 *
 */
//+------------------------------------------------------------------
Tango::DevString SGonPPMACBox::exec_low_level_cmd(Tango::DevString argin)
{
	//	POGO has generated a method core with argout allocation.
	//	If you would like to use a static reference without copying,
	//	See "TANGO Device Server Programmer's Manual"
	//		(chapter : Writing a TANGO DS / Exchanging data)
	//------------------------------------------------------------
	//Tango::DevString	argout  = new char[6];
	//strcpy(argout, "dummy");
	DEBUG_STREAM << "SGonPPMACBox::exec_low_level_cmd(): entering... !" << endl;

	//	Add your own code to control device here
	CHECK_CTRL_CMD();

  std::string argin_str(argin);
  std::string argout_str;

  try
  {
#if defined (_SIMULATION_)
    m_stdDataInterface->exec_low_level_cmd(argin_str, argout_str);
#else
		m_cmdInterface->exec_low_level_cmd(argin_str, argout_str);
#endif
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "Failed to send cmd: " << argin_str << " Caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to send low level cmd - Caught DevFailed!"), 
                     _CPTC("SGonPPMACBox::exec_low_level_cmd"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to send low level cmd: " << argin_str << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to send low level cmd - Caught unknown exception!"), 
							_CPTC("SGonPPMACBox::exec_low_level_cmd")); 
  }

  size_t len = argout_str.size() + 1;
  Tango::DevString argout = new char[len];

  ::memset(argout, 0, len);  
  ::memcpy(argout, argout_str.c_str(), len);
	
	return argout;
}

//+------------------------------------------------------------------
/**
 *	method:	SGonPPMACBox::get_firmware_version
 *
 *	description:	method to execute "GetFirmwareVersion"
 *	Returns current firmware version.
 *
 * @return	firmware version
 *
 */
//+------------------------------------------------------------------
Tango::DevString SGonPPMACBox::get_firmware_version()
{
	//	POGO has generated a method core with argout allocation.
	//	If you would like to use a static reference without copying,
	//	See "TANGO Device Server Programmer's Manual"
	//		(chapter : Writing a TANGO DS / Exchanging data)
	//------------------------------------------------------------
	//Tango::DevString	argout  = new char[6];
	//strcpy(argout, "dummy");
	DEBUG_STREAM << "SGonPPMACBox::get_firmware_version(): entering... !" << endl;

	//	Add your own code to control device here
	CHECK_CTRL_CMD();
  std::string argout_str;

  try
  {
    argout_str = m_cmdInterface->get_firmware_vers();
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "Failed to get firmware version! Caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get firmware vers - Caught DevFailed!"), 
                     _CPTC("SGonPPMACBox::get_firmware_version"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to get firmware version!" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get firmware vers - Caught unknown exception!"), 
							_CPTC("SGonPPMACBox::get_firmware_version")); 
  }

  size_t len = argout_str.size() + 1;
  Tango::DevString argout = new char[len];

  ::memset(argout, 0, len);  
  ::memcpy(argout, argout_str.c_str(), len);

	return argout;
}

//+------------------------------------------------------------------
/**
 *	method:	SGonPPMACBox::get_ucode_version
 *
 *	description:	method to execute "GetUCodeVersion"
 *	Returns current microcode version.
 *
 * @return	microcode version
 *
 *
 */
//+------------------------------------------------------------------
Tango::DevString SGonPPMACBox::get_ucode_version()
{
	//	POGO has generated a method core with argout allocation.
	//	If you would like to use a static reference without copying,
	//	See "TANGO Device Server Programmer's Manual"
	//		(chapter : Writing a TANGO DS / Exchanging data)
	//------------------------------------------------------------
	//Tango::DevString	argout  = new char[6];
	//strcpy(argout, "dummy");
	DEBUG_STREAM << "SGonPPMACBox::get_ucode_version(): entering... !" << endl;

	//	Add your own code to control device here
	CHECK_CTRL_CMD();
  std::string argout_str;

  try
  {
    argout_str = m_cmdInterface->get_ucode_vers();
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "Failed to get ucode version! Caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to get ucode vers - Caught DevFailed!"), 
                     _CPTC("SGonPPMACBox::get_ucode_version"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to get ucode version!" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to get ucode vers - Caught unknown exception!"), 
							_CPTC("SGonPPMACBox::get_ucode_version")); 
  }

  size_t len = argout_str.size() + 1;
  Tango::DevString argout = new char[len];

  ::memset(argout, 0, len);  
  ::memcpy(argout, argout_str.c_str(), len);

	return argout;
}

//+------------------------------------------------------------------
/**
 *	method:	SGonPPMACBox::dev_state
 *
 *	description:	method to execute "State"
 *	This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
 *
 * @return	State Code
 *
 */
//+------------------------------------------------------------------
Tango::DevState SGonPPMACBox::dev_state()
{
	//	Add your own code to control device here
	if (m_initOk == true)
  {
    yat::AutoMutex<> guard(SGonPPMACBox::m_stLock);

		CHECK_CTRL_CTL();

	  // get connection state
    bool cnct_st = false;
		
		try
		{
#if defined (_SIMULATION_)
      m_stdDataInterface->get_connection_state(cnct_st);
#else
			m_ctlDataInterface->get_connection_state(cnct_st);
#endif
		}
		catch(Tango::DevFailed &e)
		{
			ERROR_STREAM << "Failed to get connection state! Caught DevFAILED: \n" << e << std::endl;
			m_state = Tango::FAULT;
			return m_state;
		}
		catch(...)
		{
			ERROR_STREAM << "Failed to get connection state!" << std::endl;
			m_state = Tango::FAULT;
			return m_state;
		}

    if (cnct_st)
    {
      m_state = Tango::OPEN;
    }
    else
    {
      m_state = Tango::CLOSE;
    }

    // get box status
    sgonpmaclib::T_ppmac_brick_status raw_st;
	  
		try
		{
      raw_st = m_stdDataInterface->get_box_status();
		}
		catch(Tango::DevFailed &e)
		{
			ERROR_STREAM << "Failed to get box status! Caught DevFAILED: \n" << e << std::endl;
			m_state = Tango::FAULT;
			return m_state;
		}
		catch(...)
		{
			ERROR_STREAM << "Failed to get box status!" << std::endl;
			m_state = Tango::FAULT;
			return m_state;
		}
		
    m_box_status_word_str.str("");

    // check errors
    if (sgonppmac_ns::box_error_mask(raw_st))
    {
      ERROR_STREAM << "PPMAC Box in FAULT state!" << std::endl;

      // only write faults in status
      if (raw_st.WDTFault0)
			  m_box_status_word_str << "+ Software watchdog fault (background failure)" << std::endl; 
      if (raw_st.WDTFault1)
			  m_box_status_word_str << "+ Software watchdog fault (interrupt failure)" << std::endl;
      if (raw_st.PwrOnFault)
			  m_box_status_word_str << "+ Power-on or reset load fault" << std::endl;
      if (raw_st.ProjectLoadErr)
			  m_box_status_word_str << "+ Project load error" << std::endl;
      if (raw_st.ConfigLoadErr)
			  m_box_status_word_str << "+ Saved configuration load error" << std::endl;
      if (raw_st.HWChangeErr)
			  m_box_status_word_str << "+ Hardware change detected since save" << std::endl;
      if (raw_st.FileConfigErr)
			  m_box_status_word_str << "+ System file configuration error" << std::endl;
      if (raw_st.Default)
			  m_box_status_word_str << "+ Factory default configuration (by cmd or error)" << std::endl;
      if (raw_st.NoClocks)
			  m_box_status_word_str << "+ No system clocks found" << std::endl;
      if (raw_st.AbortAll)
			  m_box_status_word_str << "+ Abort all" << std::endl;
      if (raw_st.BusOverVoltage)
			  m_box_status_word_str << "+ DC bus overvoltage fault" << std::endl;
      if (raw_st.OverTemp)
			  m_box_status_word_str << "+ Power board over temperature" << std::endl;
      if (raw_st.BusUnderVoltage)
			  m_box_status_word_str << "+ DC bus undervoltage warning" << std::endl;
      //if (raw_st.PLCsFault)
			  //m_box_status_word_str << "+ Critical PLCs are not running" << std::endl;
      //if (raw_st.PLCsWarning)
			  //m_box_status_word_str << "+ Some PLCs are not running" << std::endl;
      m_state = Tango::FAULT;
    }
    // check warnings
    else if (sgonppmac_ns::box_warning_mask(raw_st))
    {
			WARN_STREAM << "PPMAC Box in ALARM state!" << std::endl;

      // only write warnings in status
      if (raw_st.WDTFault0)
			  m_box_status_word_str << "+ Software watchdog fault (background failure)" << std::endl; 
      if (raw_st.WDTFault1)
			  m_box_status_word_str << "+ Software watchdog fault (interrupt failure)" << std::endl;
      if (raw_st.PwrOnFault)
			  m_box_status_word_str << "+ Power-on or reset load fault" << std::endl;
      if (raw_st.ProjectLoadErr)
			  m_box_status_word_str << "+ Project load error" << std::endl;
      if (raw_st.ConfigLoadErr)
			  m_box_status_word_str << "+ Saved configuration load error" << std::endl;
      if (raw_st.HWChangeErr)
			  m_box_status_word_str << "+ Hardware change detected since save" << std::endl;
      if (raw_st.FileConfigErr)
			  m_box_status_word_str << "+ System file configuration error" << std::endl;
      if (raw_st.Default)
			  m_box_status_word_str << "+ Factory default configuration (by cmd or error)" << std::endl;
      if (raw_st.NoClocks)
			  m_box_status_word_str << "+ No system clocks found" << std::endl;
      if (raw_st.AbortAll)
			  m_box_status_word_str << "+ Abort all" << std::endl;
      if (raw_st.BusOverVoltage)
			  m_box_status_word_str << "+ DC bus overvoltage fault" << std::endl;
      if (raw_st.OverTemp)
			  m_box_status_word_str << "+ Power board over temperature" << std::endl;
      if (raw_st.BusUnderVoltage)
			  m_box_status_word_str << "+ DC bus undervoltage warning" << std::endl;
//      if (raw_st.PLCsFault)
	//		  m_box_status_word_str << "+ Critical PLCs are not running" << std::endl;
    //  if (raw_st.PLCsWarning)
			//  m_box_status_word_str << "+ Some PLCs are not running" << std::endl;

      m_state = Tango::ALARM;
    }
    else
    {
      // no pb, we keep initial m_status value
      m_box_status_word_str << "" << std::endl;
    }
  }
	  
	return m_state;
}

//+------------------------------------------------------------------
/**
 *	method:	SGonPPMACBox::dev_status
 *
 *	description:	method to execute "Status"
 *	This command gets the device status (stored in its <i>device_status</i> data member) and returns it to the caller.
 *
 * @return	Status description
 *
 */
//+------------------------------------------------------------------
Tango::ConstDevString SGonPPMACBox::dev_status()
{
	//	Add your own code to control device here
	static std::string s = "";
	yat::OSStream oss;

	if (m_initOk == true)
	{
    yat::AutoMutex<> guard(SGonPPMACBox::m_stLock);

    if (m_state == Tango::OPEN)
    {
      oss << "Connection is open." << endl;
    }
    else if (m_state == Tango::CLOSE)
    {
      oss << "Connection is closed." << endl;
    }
    else if (m_state == Tango::FAULT)
    {
      oss << "BOX in error!" << endl << endl;
      oss << "Detailed box status: " << endl;
      oss << m_box_status_word_str.str();
    }
    else if (m_state == Tango::ALARM)
    {
      oss << "BOX in alarm!" << endl << endl;
      oss << "Detailed box status: " << endl;
      oss << m_box_status_word_str.str();
    }
    else 
    {
      oss << "Not controlled state..." << endl;
    }
	}
	else
	{
		oss << m_currStatus.c_str() << std::endl;
	}

	s = oss.str();
	return s.c_str();
}


}	//	namespace

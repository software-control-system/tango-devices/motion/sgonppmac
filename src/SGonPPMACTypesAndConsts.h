//=============================================================================
// SGonPPMACTypesAndConsts.h
//=============================================================================
// abstraction.......PowerPMAC 
// class.............SGonPPMACTypesAndConsts
// original author.... F.Losacco - NEXEYA
//=============================================================================

#ifndef _POWERPMAC_TYPES_AND_CONSTS_H_
#define _POWERPMAC_TYPES_AND_CONSTS_H_

#pragma once

//=============================================================================
// DEPENDENCIES
//=============================================================================
#include "tango.h"
#include <sstream>
#include <vector>
#include <string>
#include <map>


/*Use YAT library*/
#include <yat/any/Any.h>
#include <yat4tango/DynamicInterfaceManager.h>
#include <SGonPMAC/SGonPMACLib.h>

//=============================================================================
// IMPL OPTION
//=============================================================================

namespace sgonppmac_ns
{

	//- split_property_line
  static std::vector<std::string> split_property_line(std::string line)
  {
	  //std::cout << "\n\t\t LINE TO PARSE \"" << line << "\"" << std::endl;
	  //- string iterators
	  std::string::size_type begIdx, endIdx;
	  std::string tmp("");

	  //- result : the splitted line 
	  std::vector<std::string> propertyList;

	  //- search beginning of the first word
#define DELIMS "::"

	  begIdx = line.find_first_not_of(DELIMS);

	  while ( begIdx != std::string::npos )
	  {
		  //- search the end of the actual word
		  endIdx = line.find_first_of(DELIMS, begIdx);
		  if ( endIdx == std::string::npos)
		  {
			  tmp = line.substr(begIdx);
			  //std::cout << "\t\t END LINE TO PARSE \"" << tmp << "\"" << std::endl;
			  propertyList.push_back( tmp );
			  break;
		  }

		  //- store the property found
		  tmp = line.substr(begIdx, (endIdx-begIdx));
		  //std::cout << "\t\t LINE PARSED \"" << tmp << "\"\n" << std::endl;
		  propertyList.push_back( tmp );

		  //- search beginning of the next word
		  begIdx = line.find_first_not_of(DELIMS, endIdx);
	  }
	  return propertyList;
  }

	// ============================================================================
	// PowerPMACConfig:  struct containing the PowerPMAC configuration
	// ============================================================================
	typedef struct PowerPMACConfig
	{
		//- members
		
		//- host device
		Tango::DeviceImpl * hostDevice;
		
		//- IP address of the underlying power brick.
 		string	iPAddress;

		//-	Power brick listening TCP port.
		unsigned long	tCPPort;

		//	Polling period for standard data reading, in ms.
 		double	stdDataPollingPeriod;
		
		//	Polling period for critical data reading, in ms.
 		double	criticalDataPollingPeriod;

		// Number of deployed instances of composed axes
		Tango::DevUShort	nbOfComposedAxes;

		//- default constructor -----------------------
		PowerPMACConfig ()
			: hostDevice(NULL),
			iPAddress(""),
			tCPPort(0),
			stdDataPollingPeriod(0),
			criticalDataPollingPeriod(0),
			nbOfComposedAxes(0)
		{
		}

		//- destructor -----------------------
		~PowerPMACConfig ()
		{
		}

		//- copy constructor ------------------
		PowerPMACConfig (const PowerPMACConfig& src)
		{
			*this = src;
		}

		//- operator= ------------------
		const PowerPMACConfig & operator= (const PowerPMACConfig& src)
		{
			if (this == & src) 
				return *this;

			this->hostDevice = src.hostDevice;
			this->iPAddress = src.iPAddress;
			this->tCPPort = src.tCPPort;
			this->stdDataPollingPeriod = src.stdDataPollingPeriod;
			this->criticalDataPollingPeriod = src.criticalDataPollingPeriod;
			this->nbOfComposedAxes = src.nbOfComposedAxes;

			return *this;
		}

		//- dump -------------------
    void dump () const
    {
      std::cout << "PowerPMACConfig::hostDevice........." 
                << this->hostDevice
                << std::endl; 
			std::cout << "PowerPMACConfig::iPAddress........." 
                << this->iPAddress
                << std::endl; 
      std::cout << "PowerPMACConfig::tCPPort........." 
                << this->tCPPort
                << std::endl; 
      std::cout << "PowerPMACConfig::stdDataPollingPeriod........." 
                << this->stdDataPollingPeriod
                << std::endl; 
      std::cout << "PowerPMACConfig::criticalDataPollingPeriod........." 
                << this->criticalDataPollingPeriod
                << std::endl; 
			std::cout << "PowerPMACConfig::nbOfComposedAxes........." 
                << this->nbOfComposedAxes
                << std::endl; 
    }
	      
	} PowerPMACConfig;

	// ============================================================================
	// PowerPMACConfig:  struct containing the PowerPMAC configuration
	// ============================================================================
	typedef struct AxisInitData
	{
		//- members

		//- Indicate if encoder type is initialized
		bool encTypeIsInitialized;

		//- Axis initialization position
		double initPos;
		
		//- Axis axis velocity
		double initVel;
		
		//- Axis user unit
		double userRatio;

		//- default constructor -----------------------
		AxisInitData ()
			: encTypeIsInitialized(false),
			initPos(-1.0),
			initVel(-1.0),
			userRatio(-1.0)
		{
		}

		//- destructor -----------------------
		~AxisInitData ()
		{
		}

		//- copy constructor ------------------
		AxisInitData (const AxisInitData& src)
		{
			*this = src;
		}

		//- operator= ------------------
		const AxisInitData & operator= (const AxisInitData& src)
		{
			if (this == & src) 
				return *this;
      
			this->encTypeIsInitialized = src.encTypeIsInitialized;
			this->initPos = src.initPos;
			this->initVel = initVel;
			this->userRatio = userRatio;

			return *this;
		}
	      
	} AxisInitData;


	// ============================================================================
	// ParamAxisValueMsg:  struct containing the parameters for ControlerInterace 
	// cmd with axis id param and a value (double)
	// ============================================================================
	typedef struct ParamAxisValueMsg
	{
		//- members
		
		//- Axis id
		sgonpmaclib::T_axis_id idAxis;

		//- value in double
		double valueDouble;
    
    bool valueBool;
		
		//- default constructor -----------------------
		ParamAxisValueMsg ()
			: valueDouble(0.0),
        valueBool(false)
		{
		}

		//- destructor -----------------------
		~ParamAxisValueMsg ()
		{
		}

		//- copy constructor ------------------
		ParamAxisValueMsg (const ParamAxisValueMsg& src)
		{
			*this = src;
		}

		//- operator= ------------------
		const ParamAxisValueMsg & operator= (const ParamAxisValueMsg& src)
		{
			if (this == & src) 
				return *this;

			this->idAxis = src.idAxis;
			this->valueDouble = src.valueDouble;
			this->valueBool = src.valueBool;
			return *this;
		}
	} ParamAxisValueMsg;

	// ============================================================================
	// ParamCsValueMsg:  struct containing the parameters for ControlerInterace 
	// cmd with CS id param and a value (double)
	// ============================================================================
	typedef struct ParamCsValueMsg
	{
		//- members
		
		//- Axis id
		sgonpmaclib::T_cs_id idCs;
		
		//- value in double
		double valueDouble;
		
		//- default constructor -----------------------
		ParamCsValueMsg ()
			: valueDouble(0.0)
		{
		}

		//- destructor -----------------------
		~ParamCsValueMsg ()
		{
		}

		//- copy constructor ------------------
		ParamCsValueMsg (const ParamCsValueMsg& src)
		{
			*this = src;
		}

		//- operator= ------------------
		const ParamCsValueMsg & operator= (const ParamCsValueMsg& src)
		{
			if (this == & src) 
				return *this;

			this->idCs = src.idCs;
			this->valueDouble = src.valueDouble;
			
			return *this;
		}
	} ParamCsValueMsg;

	// ============================================================================
	// ParamCsModeMsg:  struct containing the parameters for ControlerInterace 
	// cmd with CS id param and a moving mode 
	// ============================================================================
	typedef struct ParamCsModeMsg
	{
		//- members
		
		//- Axis id
		sgonpmaclib::T_cs_id idCs;
		
		//- value in E_moving_mode_t
		sgonpmaclib::E_moving_mode_t mv_mode;
		
		//- default constructor -----------------------
		ParamCsModeMsg ()
			: mv_mode(sgonpmaclib::CS_MV_MODE_RAPID)
		{
		}

		//- destructor -----------------------
		~ParamCsModeMsg ()
		{
		}

		//- copy constructor ------------------
		ParamCsModeMsg (const ParamCsModeMsg& src)
		{
			*this = src;
		}

		//- operator= ------------------
		const ParamCsModeMsg & operator= (const ParamCsModeMsg& src)
		{
			if (this == & src) 
				return *this;

			this->idCs = src.idCs;
			this->mv_mode = src.mv_mode;
			
			return *this;
		}
	} ParamCsModeMsg;

	// ============================================================================
	// ParamAxisValueBool:  struct containing the parameters for ControlerInterace 
	// cmd with axis id param, a value (double) and a boolean
	// ============================================================================
	typedef struct ParamAxisValueBool
	{
		//- members
		
		ParamAxisValueMsg axisParams;
		
		//- value in Boolean
		bool bValue;
		
		//- default constructor -----------------------
		ParamAxisValueBool ()
			: bValue(false)
		{
		}

		//- destructor -----------------------
		~ParamAxisValueBool ()
		{
		}

		//- copy constructor ------------------
		ParamAxisValueBool (const ParamAxisValueBool& src)
		{
			*this = src;
		}

		//- operator= ------------------
		const ParamAxisValueBool & operator= (const ParamAxisValueBool& src)
		{
			if (this == & src) 
				return *this;

			this->axisParams = src.axisParams;
			this->bValue = src.bValue;
			
			return *this;
		}
	} ParamAxisValueBool;


	//- Axis parameters
  typedef struct AxisParams
  {
    //- members ------------

		// position
		double position;

		// offset
    double offset;

    // backlash
    double backlash;
    
    // accuracy = deadband in the case of deltaTAU controller
    double accuracy;

		// backward limit switch reached?
    bool backward_limit;

		// forward limit switch reached?
    bool forward_limit;

    // min software limit for position
    double min_pos_limit;

    // max software limit for position
    double max_pos_limit;
    
    // user unit 
    double user_unit;
    
    // axis compensation enabled
    bool compensation;
    
    // user unit name 
    std::string user_unit_name;
    
    //- default constructor --
    AxisParams ()
      : position(0.0),
				offset(0.0),
        backlash(0.0),
				accuracy(0.0),
				backward_limit(false),
				forward_limit(false),
        min_pos_limit(0.0),
        max_pos_limit(0.0),
        user_unit(1), 
        compensation(false),
        user_unit_name("")
    {
    }

    //- destructor -----------
    ~AxisParams ()
    {
    }

    //- copy constructor -----
    AxisParams (const AxisParams& src)
    {
      *this = src;
    }

    //- operator= --------------
    const AxisParams & operator= (const AxisParams& src)
    {
        if (this == & src) 
          return *this;

        this->accuracy = src.accuracy;
        this->backlash = src.backlash;
        this->offset = src.offset;
				this->position = src.position;
        this->min_pos_limit = src.min_pos_limit;
        this->max_pos_limit = src.max_pos_limit;
        this->backward_limit = src.backward_limit;
				this->forward_limit = src.forward_limit;
				this->compensation = src.compensation;
				this->user_unit = src.user_unit;
				this->user_unit_name = src.user_unit_name;
				
        return *this;
    }

    //- dump -------------------
    void dump () const
    {
      std::cout << "AxisParams::position........." 
                << this->position
                << std::endl; 
			std::cout << "AxisParams::offset........." 
                << this->offset
                << std::endl; 
      std::cout << "AxisParams::accuracy........." 
                << this->accuracy
                << std::endl; 
      std::cout << "AxisParams::backlash........." 
                << this->backlash
                << std::endl; 
      std::cout << "AxisParams::min_pos_limit........." 
                << this->min_pos_limit
                << std::endl; 
      std::cout << "AxisParams::max_pos_limit........." 
                << this->max_pos_limit
                << std::endl; 
			std::cout << "AxisParams::backward_limit........." 
                << this->backward_limit
                << std::endl; 
			std::cout << "AxisParams::forward_limit........." 
                << this->forward_limit
                << std::endl; 	
      std::cout << "AxisParams::compensation........." 
                << this->compensation
                << std::endl; 	
      std::cout << "AxisParams::user_unit........." 
                << this->user_unit
                << std::endl; 	
      std::cout << "AxisParams::user_unit_name........." 
                << this->user_unit_name
                << std::endl; 
    }
  } AxisParams;

	//- Cs parameters
  typedef struct CsParams
  {
    //- members ------------
    // Acceleration, in user unit/s^2
    double acceleration;
  
	  // Deceleration, in user unit/s^2
		double deceleration;

		// Velocity, in user unit/s
		double velocity;

		// CS status
		sgonpmaclib::T_cs_status status;

		// Moving mode
		sgonpmaclib::E_moving_mode_t mv_mode;

		// Moving time
		double mv_time;
    
    // Compensation tables validity
    bool comp_valid;

    // referencing status
    sgonpmaclib::E_cs_referencing_status_t ref_status;


		//- default constructor --
    CsParams ()
      : acceleration(0.0),
        deceleration(0.0),
				velocity(0.0),
				mv_mode(sgonpmaclib::CS_MV_MODE_RAPID),
				mv_time(0),
        comp_valid(false),
        ref_status(sgonpmaclib::CS_REF_POS_UNKNOWN)
    {
    }

    //- destructor -----------
    ~CsParams ()
    {
    }

    //- copy constructor -----
    CsParams (const CsParams& src)
    {
      *this = src;
    }

    //- operator= --------------
    const CsParams & operator= (const CsParams& src)
    {
        if (this == & src) 
          return *this;

        this->acceleration = src.acceleration;
        this->deceleration = src.deceleration;
        this->velocity = src.velocity;
        this->status = src.status;
        this->comp_valid = src.comp_valid;
				this->mv_mode = src.mv_mode;
				this->mv_time = src.mv_time;
        this->ref_status = src.ref_status;
        
        return *this;
    }

    //- dump -------------------
    void dump () const
    {
      
			std::cout << "CsParams::acceleration........." 
                << this->acceleration
                << std::endl; 
      std::cout << "CsParams::deceleration........." 
                << this->deceleration
                << std::endl; 
      std::cout << "CsParams::velocity........." 
                << this->velocity
                << std::endl; 
			std::cout << "CsParams::comp_valid........." 
                << this->comp_valid
                << std::endl; 
			std::cout << "CsParams::ref_status........." 
                << this->ref_status
                << std::endl; 
    }

	} CsParams;


	// Other types
	typedef std::map<sgonpmaclib::T_axis_id, AxisParams> AxisParamsMap_t;
  typedef AxisParamsMap_t::iterator AxisParamsMap_it_t;

	typedef std::map<sgonpmaclib::T_cs_id, CsParams> CsParamsMap_t;
  typedef CsParamsMap_t::iterator CdParamsMap_it_t;

	typedef std::vector<sgonpmaclib::T_axis_id> AxisList_t;
	typedef std::vector<sgonpmaclib::T_cs_id> CsList_t;

	typedef std::vector<yat4tango::DynamicAttributeInfo> DynAttrInfoList_t;
	typedef std::vector<yat4tango::DynamicCommandInfo> DynCmdInfo_t;

  // BOX error mask
  static inline bool box_error_mask(sgonpmaclib::T_ppmac_brick_status box_st)
  {
    if ((box_st.WDTFault0) ||
        (box_st.WDTFault1) ||
        (box_st.PwrOnFault) ||
        (box_st.ProjectLoadErr) ||
        (box_st.ConfigLoadErr) ||
        (box_st.HWChangeErr) ||
        (box_st.FileConfigErr) ||
        (box_st.Default) ||
        (box_st.NoClocks) ||
        (box_st.AbortAll) ||
        (box_st.OverTemp)/*||
        (box_st.PLCsFault)*/)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  // BOX warning mask
  static inline bool box_warning_mask(sgonpmaclib::T_ppmac_brick_status box_st)
  {
    if ((box_st.BusUnderVoltage) ||
        (box_st.BusOverVoltage)/* ||
        (box_st.PLCsWarning)*/)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

	// ============================================================================
	// MotionProgramDef:  struct containing the parameters for a motion program
	// ============================================================================
	typedef struct MotionProgramDef
	{
		//- members
		
		//- MP id
		unsigned int id;

		//- value in double
    std::string description;
		
		//- default constructor -----------------------
		MotionProgramDef ()
			: id(0),
      description("")
		{
		}

		//- destructor -----------------------
		~MotionProgramDef ()
		{
		}

		//- copy constructor ------------------
		MotionProgramDef (const MotionProgramDef& src)
		{
			*this = src;
		}

		//- operator= ------------------
		const MotionProgramDef & operator= (const MotionProgramDef& src)
		{
			if (this == & src) 
				return *this;

			this->id = src.id;
      this->description = src.description;
			
			return *this;
		}
 
	} MotionProgramDef;

  // Motion programs states
#define kMP_STATE_STDBY         "STANDBY"
#define kMP_STATE_FAULT         "FAULT"
#define kMP_STATE_RUNNING       "RUNNING"
#define kMP_STATE_STEP_RUNNING  "SINGLE STEP"
#define kMP_STATE_HOLD          "HOLD"
#define kMP_STATE_NULL          "NULL"
#define kMP_STATE_ACTIVE        "ACTIVE"
#define kMP_STATE_UNKNOWN       "UNKNOWN"


	// ============================================================================
	// Disabled commands types
	// ============================================================================
  //- device commands that can be disabled using the DisabledCmds property
#define kCMD_FORWARD       0
#define kCMD_BACKWARD      1
#define kCMD_INIT_REF_POS  2
#define kCMD_DEFINE_POS    3
#define kCMD_ON            4
#define kCMD_OFF           5
#define kCMD_MP_START      6
#define kCMD_DISABLE       7
#define kCMD_LAST          8


  //- name of Device commands that can be disabled using the DisabledCmds property
#define kCMD_FORWARD_NAME       "forward"
#define kCMD_BACKWARD_NAME      "backward"
#define kCMD_INIT_REF_POS_NAME  "initrefpos"
#define kCMD_DEFINE_POS_NAME    "defpos"
#define kCMD_ON_NAME            "on"
#define kCMD_OFF_NAME           "off"
#define kCMD_MAP_START_NAME     "mpstart"
#define kCMD_DISABLE_NAME       "disable"


//- Acceleration ratio
#define kNEGATIVE -1
#define kACCELERATION_RATIO 1000
#define kDECELERATION_RATIO 1000
//- User linear units values - default unit : meters
#define kUNIT_M     1000000
#define kUNIT_MM    1000
#define kUNIT_UM    1
#define kUNIT_NM    0.001
//- name of linear units
#define kUNIT_M_NAME    "m"
#define kUNIT_MM_NAME   "mm"
#define kUNIT_UM_NAME   "um"
#define kUNIT_NM_NAME   "nm"
//- User angular units values - default unit : dregres
#define kUNIT_RAD     57295.779513
#define kUNIT_MRAD    57.295779513
#define kUNIT_DEG     1000
#define kUNIT_MDEG    1
//- name of angular units
#define kUNIT_RAD_NAME    "rad"
#define kUNIT_MRAD_NAME   "mrad"
#define kUNIT_DEG_NAME    "deg"
#define kUNIT_MDEG_NAME   "mdeg"



} // namespace sgonppmac_ns

#endif // _POWERPMAC_TYPES_AND_CONSTS_H_

//=============================================================================
// SimpleAxis.cpp
//=============================================================================
// class.............SimpleAxis
// original author...F. THIAM
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <sstream>
#include "SimpleAxis.h"
#include "BoxInitDetector.h"

// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================


namespace sgonppmac_ns {

	//- check controller cmd macro:
#define CHECK_CTRL_CMD() \
	do \
	{ \
	if (!m_cmdInterface || sgonppmac_ns::BoxInitDetector::instance()->is_init_needed(this)) \
	THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
	_CPTC("request aborted - the command controller isn't accessible "), \
  _CPTC("SimpleAxis::check_ctrl_cmd")); \
	} while (0)

	//- check controller std macro:
#define CHECK_CTRL_STD() \
	do \
	{ \
	if (!m_stdDataInterface || sgonppmac_ns::BoxInitDetector::instance()->is_init_needed(this)) \
	THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
	_CPTC("request aborted - the standard controller isn't accessible "), \
  _CPTC("SimpleAxis::check_ctrl_std")); \
	} while (0)

	//- check controller ctl macro:
#define CHECK_CTRL_CTL() \
	do \
	{ \
	if (!m_ctlDataInterface || sgonppmac_ns::BoxInitDetector::instance()->is_init_needed(this)) \
	THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
	_CPTC("request aborted - the critical controller isn't accessible "), \
  _CPTC("SimpleAxis::check_ctrl_ctl")); \
	} while (0)

// ============================================================================

// ============================================================================
// SimpleAxis::SimpleAxis
// ============================================================================
SimpleAxis::SimpleAxis(Tango::DeviceImpl * hostDevice)
: AxisInterface(hostDevice)
{
	//INFO_STREAM << "SimpleAxis: hostDevice=" << hostDevice 
	//							<< " this=" << this << std::endl;
}

// ============================================================================
// SimpleAxis::~SimpleAxis
// ============================================================================
SimpleAxis::~SimpleAxis ()
{
	
}

// ============================================================================
// SimpleAxis::init_axis
// ============================================================================
void SimpleAxis::init_axis( vector<string>	axisDefinition)
{
	try 
	{
		this->init_controler_interface();
	}
	catch(...)
	{
		throw;
	}

	std::vector<sgonpmaclib::T_axis_id> axisList;

	// extract axis definition
	for (vector<string>::iterator i = axisDefinition.begin(); i != axisDefinition.end(); ++i)
	{
    // Each line of the axisDefinition property contains: "<CS>::<axis>"
    // in case of a simple axis, there is only one line
    std::string axis_id;
    axis_id = *i; 
    std::vector<std::string> l_vect = split_property_line(axis_id);

    // Extract CS number
    std::string cs_str = l_vect.at(0);
		m_local_axis.cs = atoi(cs_str.c_str());
		m_cs_id = m_local_axis.cs;

    // Extract axis name
    m_local_axis.axis_name = l_vect.at(1);

		axisList.push_back(m_local_axis);
  }

  try
  {
		// Declare the axis 
    this->declare_axis(axisList);
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "SimpleAxis::init_axis Axis declaration NOK: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to init axis - Caught DevFailed!"), 
                     _CPTC("SimpleAxis::init_axis"));
  }
  catch(...)
  {
		ERROR_STREAM << "SimpleAxis::init_axis Axis declaration NOK" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to init axis - Caught unknown exception!"), 
							_CPTC("SimpleAxis::init_axis")); 
  }
}

// ============================================================================
// SimpleAxis::undeclare_axes
// ============================================================================
void SimpleAxis::undeclare_axes( )
{
	try
  {
		std::vector<sgonpmaclib::T_axis_id> axisList;
		axisList.push_back(m_local_axis);

		// Undeclare the axis 
    this->undeclare_axis(axisList);
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "SimpleAxis::undeclare_axes Axis undeclaration NOK: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to undeclare axis - Caught DevFailed!"), 
                     _CPTC("SimpleAxis::undeclare_axes"));
  }
  catch(...)
  {
		ERROR_STREAM << "SimpleAxis::undeclare_axes Axis undeclaration NOK" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to undeclare axis - Caught unknown exception!"), 
							_CPTC("SimpleAxis::undeclare_axes")); 
  }
}

// ============================================================================
// SimpleAxis::set_axis_init_data
// ============================================================================
void SimpleAxis::set_axis_init_data(AxisInitData &initData )
{
	CHECK_CTRL_CMD();

	try
  {
		m_cmdInterface->set_axis_init_data(m_local_axis, initData);
		INFO_STREAM << "Axis properly defined" << endl;
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "SimpleAxis::set_axis_init_data caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to set axis init data - Caught DevFailed!"), 
                     _CPTC("SimpleAxis::set_axis_init_data"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to set init data" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to set axis init data - Caught unknown exception!"), 
							_CPTC("SimpleAxis::set_axis_init_data")); 
  }
}


// ============================================================================
// SimpleAxis::set_position
// ============================================================================
void SimpleAxis::set_position(double value)
{
	DEBUG_STREAM << "SimpleAxis::set_position entering... "<< endl;

	CHECK_CTRL_CMD();
	
#if defined (_SIMULATION_)
	CHECK_CTRL_CTL();
#endif

	try
  {
		this->m_cmdInterface->set_position(m_local_axis, value);
    // if set position is OK, set "move in progress" flag to true (for scan needs)
    m_isWritePosInProgress = true;

		#if defined (_SIMULATION_)
			 this->m_ctlDataInterface->set_position(m_local_axis, value);
		#endif
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "SimpleAxis::set_position caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to set position - Caught DevFailed!"), 
                     _CPTC("SimpleAxis::set_position"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to set position" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to set position - Caught unknown exception!"), 
							_CPTC("SimpleAxis::set_position")); 
  }
}

// ============================================================================
// SimpleAxis::get_offset
// ============================================================================
double SimpleAxis::get_offset()
{
	//DEBUG_STREAM << "SimpleAxis::get_offset entering... "<< endl;

	CHECK_CTRL_STD();

	return this->m_stdDataInterface->get_offset(m_local_axis);
}

// ============================================================================
// SimpleAxis::set_offset
// ============================================================================
void SimpleAxis::set_offset(double value)
{
	DEBUG_STREAM << "SimpleAxis::set_offset entering... "<< endl;

	CHECK_CTRL_CMD();
	
#if defined (_SIMULATION_)
	CHECK_CTRL_STD();
#endif

	try
  {
		this->m_cmdInterface->set_offset(m_local_axis, value);

#if defined (_SIMULATION_)
		this->m_stdDataInterface->set_offset(m_local_axis, value);
#endif
		
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "SimpleAxis::set_offset caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to set offset - Caught DevFailed!"), 
                     _CPTC("SimpleAxis::set_offset"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to set offset" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to set offset - Caught unknown exception!"), 
							_CPTC("SimpleAxis::set_offset")); 
  }
}


// ============================================================================
// SimpleAxis::get_backlash
// ============================================================================
double SimpleAxis::get_backlash()
{
  //- Function not available in Smargon case
  return yat::IEEE_NAN;
}

// ============================================================================
// SimpleAxis::set_backlash
// ============================================================================
void SimpleAxis::set_backlash(double value)
{
  //- Function not available in Smargon case
}

// ============================================================================
// SimpleAxis::get_accuracy
// ============================================================================
double SimpleAxis::get_accuracy()
{
  //- Function not available in Smargon case
  return yat::IEEE_NAN;
}

// ============================================================================
// SimpleAxis::set_accuracy
// ============================================================================
void SimpleAxis::set_accuracy(double value)
{
  //- Function not available in Smargon case  
}

// ============================================================================
// SimpleAxis::get_forwardLimitSwitch
// ============================================================================
bool SimpleAxis::get_forwardLimitSwitch()
{
  //- Function not available in Smargon case
  return yat::IEEE_NAN;
}

// ============================================================================
// SimpleAxis::get_backwardLimitSwitch
// ============================================================================
bool SimpleAxis::get_backwardLimitSwitch()
{
  //- Function not available in Smargon case
  return yat::IEEE_NAN;
}

// ============================================================================
// SimpleAxis::get_maxPositionSoftwareLimit
// ============================================================================
double SimpleAxis::get_maxPositionSoftwareLimit()
{
  //- Function not available in Smargon case
  return yat::IEEE_NAN;
}

// ============================================================================
// SimpleAxis::set_maxPositionSoftwareLimit
// ============================================================================
void SimpleAxis::set_maxPositionSoftwareLimit(double value)
{
  //- Function not available in Smargon case
}

// ============================================================================
// SimpleAxis::get_minPositionSoftwareLimit
// ============================================================================
double SimpleAxis::get_minPositionSoftwareLimit()
{
  //- Function not available in Smargon case
  return yat::IEEE_NAN;
}

// ============================================================================
// SimpleAxis::set_minPositionSoftwareLimit
// ============================================================================
void SimpleAxis::set_minPositionSoftwareLimit(double value)
{
  //- Function not available in Smargon case
}

// ============================================================================
// SimpleAxis::axis_forward
// ============================================================================
void SimpleAxis::axis_forward()
{
  //- Function not available in Smargon case
}

// ============================================================================
// SimpleAxis::axis_backward
// ============================================================================
void SimpleAxis::axis_backward()
{
  //- Function not available in Smargon case
}

// ============================================================================
// SimpleAxis::init_axis_ref_pos
// ============================================================================
void SimpleAxis::init_axis_ref_pos()
{
	DEBUG_STREAM << "SimpleAxis::init_axis_ref_pos entering... "<< endl;

	if (this->get_state() == Tango::MOVING)
	{
		THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
			_CPTC("Start position initialization process is forbidden when the state is MOVING !"), 
			_CPTC("SimpleAxis::init_axis_ref_pos")); 
	}

	CHECK_CTRL_CMD();

#if defined (_SIMULATION_)
	CHECK_CTRL_CTL();
#endif

	try
  {
		this->m_cmdInterface->axis_init_ref_pos(m_cs_id);

#if defined (_SIMULATION_)
		this->m_ctlDataInterface->axis_init_ref_pos(m_cs_id);
#endif
  }
	catch(Tango::DevFailed &e)
  {
		ERROR_STREAM << "SimpleAxis::init_axis_ref_pos caught DevFAILED: \n" << e << std::endl;
		RETHROW_DEVFAILED(e,
										 _CPTC("DEVICE_ERROR"),
										 _CPTC("Failed to axis init ref pos - Caught DevFailed!"), 
                     _CPTC("SimpleAxis::init_axis_ref_pos"));
  }
  catch(...)
  {
		ERROR_STREAM << "Failed to axis init ref pos" << std::endl;
		THROW_DEVFAILED(
							_CPTC("DEVICE_ERROR"), 
							_CPTC("Failed to axis init ref pos - Caught unknown exception!"), 
							_CPTC("SimpleAxis::init_axis_ref_pos")); 
  }
}

// ============================================================================
// SimpleAxis::define_axis_posistion
// ============================================================================
void SimpleAxis::define_axis_position(double initPosition)
{
  //- Function not available in Smargon case
}

// ============================================================================
// SimpleAxis::get_advanced_axis_params
// ============================================================================
string SimpleAxis::get_advanced_axis_params()
{
  //- Function not available in Smargon case
  return "";
}

// ============================================================================
// SimpleAxis::compensation_enabled
// ============================================================================
bool SimpleAxis::compensation_enabled()
{
  return (this->m_stdDataInterface->get_compensation(m_local_axis));
}

} // namespace sgonppmac_ns



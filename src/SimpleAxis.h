//=============================================================================
// SimpleAxis.h
//=============================================================================
// class.............SimpleAxis
// original author...F. THIAM
//=============================================================================

#ifndef _SIMPLE_AXIS_H_
#define _SIMPLE_AXIS_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>

#include "AxisInterface.h"



namespace sgonppmac_ns {



// ============================================================================
// class: SimpleAxis
// ============================================================================
class SimpleAxis : public AxisInterface
{

public:

	
  // Constructor.
  SimpleAxis(Tango::DeviceImpl * hostDevice);

  // Destructor.
  virtual ~SimpleAxis();

	
  //---------------------------------------------------------------------------
  // Controller management
  //---------------------------------------------------------------------------

	// Set the axis initialization data
	virtual void set_axis_init_data(AxisInitData &initData );

	// Declare axis list
	// @param axisDefinition   (depend of type axis) list of Cs:Axis or Cs:Axis:Label.
	virtual void init_axis( vector<string>	axisDefinition);

	// Undeclare axis list
	virtual void undeclare_axes();


	// Attribute position
	//virtual double get_position();
  void set_position(double value);

	// Attribute offset
	virtual double get_offset();
  void set_offset(double value);

	// Attribute backlash
	virtual double get_backlash();
  void set_backlash(double value);
	
	// Attribute accuracy = deadband in the case of deltaTAU controller
	virtual double get_accuracy();
  void set_accuracy(double value);
	
	// Attribute forwardLimitSwitch
	virtual bool get_forwardLimitSwitch();

	// Attribute backwardLimitSwitch
	virtual bool get_backwardLimitSwitch();
	
	// Attribute maxPositionSoftwareLimit
	virtual double get_maxPositionSoftwareLimit();
  void set_maxPositionSoftwareLimit(double value);
	
	// Attribute minPositionSoftwareLimit
	virtual double get_minPositionSoftwareLimit();
  
  // set min position Software Limit
  void set_minPositionSoftwareLimit(double value);

	//- Command Forward
	void axis_forward();

	//- Command Backward
	void axis_backward();

	//- Command InitializeReferencePosition
	void init_axis_ref_pos();

	//- Command DefinePosition
	void define_axis_position(double initPosition);

	//- Command GetAdvancedAxisParams
	string get_advanced_axis_params();
  
  // Checks if at least one compensation table is enabled.
  // @return True = at least ne compensation table enabled, false otherwise.
  bool compensation_enabled();

protected:
	
	// Definition of local axis
	sgonpmaclib::T_axis_id m_local_axis;
	
};

} // namespace sgonppmac_ns

#endif // _SIMPLE_AXIS_H_
